<?php

namespace App\Exports;

use App\Models\Brand;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class BrandExport implements FromCollection, WithHeadings, WithMapping, WithStyles, WithEvents {

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection() {
        return Brand::all();
    }

    public function headings(): array {
        return [
            ucfirst(Brand::NAME),
            ucfirst(Brand::DESCRIPTION),
            ucfirst(Brand::IS_VALIDATE),
            ucfirst(Brand::LOGO),
            ucfirst(Brand::IMAGE),
            ucfirst(Brand::CREATED_AT),
            ucfirst(Brand::UPDATED_AT),
        ];
    }

    public function map($brand): array {
        return [
            $brand->name,
            $brand->description,
            $brand->is_validate,
            '  ',
            '  ',
            $brand->created_at,
            $brand->updated_at,
        ];
    }
    
    function styles(Worksheet $sheet) {
        $sheet->getStyle('1')->getFont()->setBold(true);
        $sheet->getStyle('1')->getFont()->setSize(12);
        $sheet->getDefaultRowDimension()->setRowHeight(60);
        // $sheet->getStyle('1')->get->setHeight(1, 50);
        // $sheet->getDefaultColumnDimension()->setA1l1
        $sheet->getStyle('A1:'.$sheet->getHighestColumn().$sheet->getHighestRow())->getAlignment()->setVertical('center');
        // $sheet->getStyle('B')->getAlignment()->setVertical('center');
        // $sheet->getStyle('C')->getAlignment()->setVertical('center');
        // $sheet->getStyle('D')->getAlignment()->setVertical('center');
        // $sheet->getStyle('E')->getAlignment()->setVertical('center');
        // $sheet->getStyle('F')->getAlignment()->setVertical('center');
        // $sheet->getStyle('G')->getAlignment()->setVertical('center');
        $sheet->getStyle('C')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('D')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('E')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('F')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('G')->getAlignment()->setHorizontal('center');
        $sheet->getColumnDimension('A')->setWidth(26);
        $sheet->getColumnDimension('B')->setWidth(60);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setWidth(18);
        $sheet->getColumnDimension('E')->setWidth(24);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
    }
    
    /**
     * @return array
     */
    public function registerEvents(): array {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->setAutoFilter('A1:'.$event->sheet->getDelegate()->getHighestColumn().'1');

                $brands = Brand::get();

                $loop = 2;
                foreach ($brands as $brand) {
                    
                    $logo = explode('/', $brand->logo);
                    $name_logo = end($logo);
                    
                    $drawing_logo = new Drawing();
                    $drawing_logo->setPath(public_path('storage/'.Brand::LOGO_PATH.$name_logo));
                    $drawing_logo->setHeight(65);
                    $drawing_logo->setOffsetX(30);
                    $drawing_logo->setOffsetY(10);
                    $drawing_logo->setCoordinates('D' . $loop);
                    $drawing_logo->setWorksheet($event->sheet->getDelegate());

                    $image = explode('/', $brand->image);
                    $name_image = end($image);

                    $drawing_image = new Drawing();
                    $drawing_image->setPath(public_path('storage/'.Brand::IMAGE_PATH.$name_image));
                    $drawing_image->setWidthAndHeight(160, 70);
                    $drawing_image->setOffsetX(5);
                    $drawing_image->setOffsetY(5);
                    $drawing_image->setCoordinates('E' . $loop);
                    $drawing_image->setWorksheet($event->sheet->getDelegate());

                    $loop++;
                }
            },
        ];
    }

}
