<?php

namespace App\Exports;

use App\Models\Brand;
use App\Models\ImageItem;
use App\Models\Item;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class ProductExport implements FromCollection, WithHeadings, WithMapping, WithStyles, WithEvents {

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection() {
        return Item::all();
    }

    public function headings(): array {
        return [
            ucfirst(Item::NAME), 
            'Brand',
            ucfirst(Item::DESCRIPTION),
            ucfirst(Item::PRICE),
            ucfirst(Item::VARIANT),
            ucfirst(Item::INVENTORY),
            ucfirst(Item::WEIGHT),
            'Image',
            ucfirst(Item::CREATED_AT),
            ucfirst(Item::UPDATED_AT)
        ];
    }

    public function map($item): array {
        return [
            $item->name,
            Brand::find($item->brand_id)->name ?? 'Endora',
            $item->description,
            $item->price,
            $item->variant,
            $item->inventory,
            $item->weight,
            ' ',
            $item->created_at,
            $item->updated_at,
        ];
    }
    
    function styles(Worksheet $sheet) {
        $sheet->getStyle('1')->getFont()->setBold(true);
        $sheet->getStyle('1')->getFont()->setSize(12);
        $sheet->getDefaultRowDimension()->setRowHeight(60);
        // $sheet->getStyle('1')->getBorders()->set;
        $sheet->getStyle('A1:'.$sheet->getHighestColumn().$sheet->getHighestRow())->getAlignment()->setVertical('center');
        $sheet->getStyle('B')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('D')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('F')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('G')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('H')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('I')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('J')->getAlignment()->setHorizontal('center');
        $sheet->getColumnDimension('A')->setWidth(36);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setWidth(36);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setWidth(20);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);
    }

    /**
     * @return array
     */
    public function registerEvents(): array {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->setAutoFilter('A1:'.$event->sheet->getDelegate()->getHighestColumn().'1');

                $items = Item::with('images')->get()->toArray();

                $loop = 2;
                foreach ($items as $item) {
                    if (count($item['images']) > 0) {
                        $image = explode('/', $item['images'][0]['name']);
                        $name_image = end($image);
                        
                        $drawing_image = new Drawing();
                        $drawing_image->setPath(public_path('storage/'.ImageItem::IMAGE_PATH.$name_image));
                        $drawing_image->setHeight(65);
                        $drawing_image->setOffsetX(30);
                        $drawing_image->setOffsetY(10);
                        $drawing_image->setCoordinates('H' . $loop);
                        $drawing_image->setWorksheet($event->sheet->getDelegate());
                    }
                    $loop++;
                }
            },
        ];
    }
}
