<?php

namespace App\Exports\Report;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use PhpOffice\PhpSpreadsheet\Style\Border;

class BalanceReport implements FromCollection, WithHeadings, WithCustomStartCell, WithMapping, ShouldAutoSize, WithStyles, WithColumnFormatting {

    protected $balance;
    private $count = 0;

    public function __construct(array $balance) {
        $this->balance = $balance;
    }

    public function collection() {
        return new Collection($this->balance);
    }

    public function startCell(): string {
        return 'B5';
    }

    public function headings(): array {
        return [
            'No.',
            'ID Pembayaran',
            'Email',
            'Tipe Pembayaran',
            'Pemasukan',
            'Pengeluaran',
            'Tanggal',
        ];
    }

    public function map($balance): array {
        return [
            ++$this->count,
            $balance['payment_id'],
            $balance['email'],
            $balance['type'],
            $balance['income'],
            $balance['outcome'],
            $balance['date']
        ];
    }

    public function columnFormats(): array {
        return [
            'F' => '"Rp"_-#,###',
            'G' => '"Rp"_-#,###',
        ];
    }

    function styles(Worksheet $sheet) {
        $sheet->getStyle('5')->getFont()->setBold(true);
        $sheet->getStyle('5')->getFont()->setSize(12);
        $sheet->getStyle('5')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('B5:H5')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEDE');
        $sheet->getStyle('B5:H5')->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN)->setColor(new Color('808080'));
        // $sheet->getStyle('A1:F1')->getFont()->getColor()->setRGB('0000ff');
        $sheet->getDefaultRowDimension()->setRowHeight(20);
        $sheet->getStyle('B')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('E')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('F')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('G')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('H')->getAlignment()->setHorizontal('center');

        $sheet->getColumnDimension('A')->setAutoSize(false);
        $sheet->getColumnDimension('A')->setWidth(2);

        $sheet->mergeCells('B2:H2')->setCellValue('B2', 'Laporan Keuangan PT. Endora Care');
        $sheet->getStyle('B2')->getFont()->setSize(14);
        $sheet->getStyle('B2')->getFont()->setBold(true);
        $sheet->getStyle('B2')->getAlignment()->setHorizontal('center');
        
        $sheet->mergeCells('B3:H3')->setCellValue('B3', now()->toDateTimeString());
        $sheet->getStyle('B3')->getAlignment()->setHorizontal('center');
    }
    
    // /**
    //  * @return array
    //  */
    // public function registerEvents(): array {
    //     return [
    //         AfterSheet::class => function(AfterSheet $event) {
    //             $event->sheet->getDelegate()->setCellValueByColumnAndRow(2, 2, 'test kabar');
    //         },
    //     ];
    // }
}
