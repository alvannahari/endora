<?php

namespace App\Exports\Report;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class PaymentReport implements FromCollection, WithHeadings, WithCustomStartCell, WithMapping, WithStyles, WithColumnFormatting, ShouldAutoSize, WithEvents {

    protected $payment;
    private $count = 0;

    public function __construct(array $payment) {
        $this->payment = $payment;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection() {
        return new Collection($this->payment);
    }

    
    public function startCell(): string {
        return 'B5';
    }

    public function headings(): array {
        return [
            'No.',
            'ID Pembayaran',
            'Username',
            'Email',
            'Alamat',
            'Status Pesanan',
            'Status Pembayaran',
            'Tipe Pembayaran',
            'Total',
            'Nomor Tagihan',
            'Status Pengiriman',
            'Pengirim',
            'Tanggal Pemesanan',
            'Tanggal Pembaruan',
        ];
    }

    public function map($balance): array {
        $status = 'Menunggu Pembayaran';
        $status_payment = 'Belum Dibayar';
        $status_shipment = $status;

        if ($balance['status'] == '1') 
            $status = 'Pesanan Diterima';
        else if ($balance['status'] == '97') 
            $status = 'Komplain';
        else if ($balance['status'] == '98') 
            $status = 'Pengajuan Pembatalan';
        else if ($balance['status'] == '99') 
            $status = 'Dibatalkan';
        
        if ($balance['status_payment'] == '1')
            $status_payment = 'Sudah Dibayar';

        if ($balance['status_shipment'] == '1')
            $status_shipment = 'Diambil Kurir';
        else if ($balance['status_shipment'] == '2')
            $status_shipment = 'Sedang Dikirim';
        else if ($balance['status_shipment'] == '3')
            $status_shipment = 'Pesanan Diterima';
        else if ($balance['status_shipment'] == '4')
            $status_shipment = 'Barang Dikonfirmasi';
        else if ($balance['status_payment'] == '1')
            $status_shipment = 'Pesanan Dikemas';

        return [
            ++$this->count,
            $balance['payment_id'],
            $balance['username'],
            $balance['email'],
            $balance['address'],
            $status,
            $status_payment,
            $balance['type'],
            $balance['total'],
            $balance['invoice_number'],
            $status_shipment,
            $balance['shipment'],
            $balance['created_at'],
            $balance['updated_at']
        ];
    }

    public function columnFormats(): array {
        return [
            'J' => '"Rp"_-#,###',
            'K' => '#',
        ];
    }

    function styles(Worksheet $sheet) {
        $sheet->getStyle('5')->getFont()->setBold(true);
        $sheet->getStyle('5')->getFont()->setSize(12);
        $sheet->getStyle('5')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('B5:O5')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('DEDEDE');
        $sheet->getStyle('B5:O5')->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN)->setColor(new Color('808080'));
        // $sheet->getStyle('A1:F1')->getFont()->getColor()->setRGB('0000ff');
        $sheet->getDefaultRowDimension()->setRowHeight(20);
        $sheet->getStyle('B')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('G')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('H')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('I')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('J')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('K')->getAlignment()->setHorizontal('left');
        $sheet->getStyle('L')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('N')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('O')->getAlignment()->setHorizontal('center');

        $sheet->getColumnDimension('F')->setAutoSize(false);
        $sheet->getColumnDimension('F')->setWidth(30);

        $sheet->getColumnDimension('A')->setAutoSize(false);
        $sheet->getColumnDimension('A')->setWidth(2);

        $sheet->mergeCells('B2:O2')->setCellValue('B2', 'Laporan Pemesanan Product PT. Endora Care');
        $sheet->getStyle('B2')->getFont()->setSize(14);
        $sheet->getStyle('B2')->getFont()->setBold(true);
        $sheet->getStyle('B2')->getAlignment()->setHorizontal('center');
        
        $sheet->mergeCells('B3:O3')->setCellValue('B3', now()->toDateTimeString());
        $sheet->getStyle('B3')->getAlignment()->setHorizontal('center');
    }

    /**
     * @return array
     */
    public function registerEvents(): array {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->setAutoFilter('C5:'.$event->sheet->getDelegate()->getHighestColumn().'5');
            },
        ];
    }

}
