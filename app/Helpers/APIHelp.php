<?php

if (!function_exists('APIresponse')) {
    function APIresponse($status, $message, $data, $code = 200) {
        return response([
            'status'      => $status,
            'message'     => $message,
            'data'        => $data
        ], $code);
    }
}

?>