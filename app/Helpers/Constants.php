<?php

namespace App\Helpers;

class Constants{
    const BCA_VA_ALIAS_PREFIX = "Endora";
    const BCA_VA_COMPANY_CODE = "13291";
    const BCA_VA_TRANS_DATE_FMT = "/[0-9]{2}\/[0-9]{2}\/[0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2}/";
    
    static $BCA_PAYMENT_DATA = [
        "id" => "bca-va",
        "available_banks" => [
            [
                'bank_account_number' => "0"
            ]
        ]
    ];
    
    static function bcaPad($number = 0){
        return self::pad($number, 12);
    }

    static function pad($number = 0, $n = 18){
        return str_pad($number, $n, '0', STR_PAD_LEFT);
    }

    static function wrapBCA($number = 0){
        return self::wrapVA(Constants::BCA_VA_COMPANY_CODE, $number);
    }

    static function wrapVA($prefix = null, $number = 0){
        $ret = "";
        if($prefix){
            $ret = $prefix.self::pad($number, 12);
        }else{
            $ret = self::BCA_VA_COMPANY_CODE.self::bcaPad($number);
        }

        return $ret;
    }

    static function paymentCode(){
        return substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,21);
    }

    static function vaAliasPrefix($name = ""){
        return self::BCA_VA_ALIAS_PREFIX.preg_replace('/\s+/', '', ucwords($name));
    }
}