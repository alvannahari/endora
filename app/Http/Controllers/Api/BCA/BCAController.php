<?php

namespace App\Http\Controllers\Api\BCA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Services\BCA\BCAServices;

class BCAController extends Controller {
    
    protected $bcaService;

    function __construct(BCAServices $bcaSvc) {
        $this->bcaService = $bcaSvc;
    }

    // va/bills
    function billInquiry(Request $request) {
        // $payload = $request->toArray();
        try{
            $payload = $request->getContent();
            $json = $this->bcaService->handleInquiry($payload);
    
            return response()->json($json);
        }catch(\Exception $e){
            return response()->json(null, 500);
        }
    }

    // va/payments
    function flagCallback(Request $request) {
        $payload = $request->getContent();
        $json = $this->bcaService->handleFlagging($payload);

        return response()->json($json);
    }
}
