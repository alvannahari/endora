<?php

namespace App\Http\Controllers\Api\BCA;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\TokenRepository;
use Lcobucci\JWT\Parser as JwtParser;
use League\OAuth2\Server\AuthorizationServer;
use Nyholm\Psr7\Response as Psr7Response;
use Psr\Http\Message\ServerRequestInterface;
use Laravel\Passport\Http\Controllers\AccessTokenController;

// extends passport
class BCAOauthController extends AccessTokenController {
    public function parseB64Basic($plainAuth) {
        if(strpos($plainAuth, "Basic") !== 0){
            throw new \Exception("Invalid basic auth");
        }

        $plainAuth = str_replace("Basic ", "",$plainAuth);
        $decoded = base64_decode($plainAuth);
        $splitted = explode(":", $decoded);

        if( sizeof($splitted) != 2 ){
            throw new \Exception("Invalid basic auth");
        }

        return $splitted;
    }

    // custom issue token by 0x4164 / adib-enc
    public function issueTokenByBasic(ServerRequestInterface $req, Request $req2) {
        try{
            $plainAuth = $req2->header('Authorization');
            $parsed = $this->parseB64Basic($plainAuth);

            $req = $req->withParsedBody([
                "grant_type" => $req2->grant_type,
                "client_id" => $parsed[0],
                "client_secret" => $parsed[1],
                "scope" => null,
            ]);

            return $this->withErrorHandling(function () use ($req) {
                return $this->convertResponse(
                    $this->server->respondToAccessTokenRequest($req, new Psr7Response)
                );
            });
        }catch(\Exception $e){
            return response()->json([
                "message" => $e->getMessage()
            ]);
        }
    }
}
