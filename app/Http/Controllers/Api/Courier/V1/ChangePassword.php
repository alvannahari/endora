<?php

namespace App\Http\Controllers\Api\Courier\V1;

use App\Http\Controllers\Controller;
use App\Models\Courier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ChangePassword extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            // Courier::PASSWORD       => 'required',
            'new_password'          => 'confirmed|min:8|different:password',
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 400);
        
        $user = $request->user();

        $user->update([
            Courier::PASSWORD => bcrypt($request->new_password)
        ]);
        return APIresponse(true, 'Password User Updated Successfully.', null);
        // if(password_verify($request->password, $user->password)){
        // }

        // return APIresponse(false, 'Password User Failed to Update!', null);
    }
}
