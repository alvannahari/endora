<?php

namespace App\Http\Controllers\Api\Courier\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use App\Models\Courier;
use Illuminate\Support\Facades\Storage;

class ChangePhoto extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Courier::IMAGE      => 'required|image|mimes:png,jpg,jpeg|max:8096',
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 400);

        $user = $request->user();

        $arr_image = explode("/",$user->image);
        $image = end($arr_image);

        if ($image != 'default.png') {
            // $filepath = public_path(UPLOAD_IMAGE_COURIER).'/'.$image;
            $delete = Storage::disk('public')->delete(Courier::IMAGE_PATH, $image);
            if (!$delete)
                return APIresponse(false, 'Current Photo Profile Failed to Delete!', null);
        }

        $photo = Storage::disk('public')->put(Courier::IMAGE_PATH, $request->file(Courier::IMAGE));

        if ($user->update([Courier::IMAGE => basename($photo)]))
            return APIresponse(true, 'Photo Profile Updated Successfully', $user);

        return APIresponse(false, 'Photo Profile Failed to Update!', null);
    }
}
