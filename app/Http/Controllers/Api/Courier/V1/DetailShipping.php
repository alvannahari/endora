<?php

namespace App\Http\Controllers\Api\Courier\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Payment;
use Illuminate\Support\Facades\Validator;
use App\Models\ShopDetail;

class DetailShipping extends Controller {

    function __invoke($notifId = null, Request $request) {
        $validator = Validator::make($request->all(), [
            Payment::ID     => 'required'
        ]);
        
        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        if ($notifId != null) {
            $notif = $request->user()->unreadNotifications()->where('id', $notifId)->first();
            if (!empty($notif)) 
                $notif->markAsRead();
        }

        $payment_id = $request->only(Payment::ID);

        $data = Payment::with(['user' => function ($q) {
            $q->withTrashed();
        }, 'location' => function ($q) {
            $q->withTrashed();
        }, 'shipmentable', 'order', 'histories.status'])->find($payment_id)->toArray();
        $data[0]['shop_info'] = ShopDetail::first();

        return APIresponse(true, 'Data Berhasil Di dapatkan', $data);
    }
}
