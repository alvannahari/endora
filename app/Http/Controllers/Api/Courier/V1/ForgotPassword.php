<?php

namespace App\Http\Controllers\Api\Courier\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Courier;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgotPasswordEmail;

class ForgotPassword extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Courier::EMAIL     => 'required|email'
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 400);
        
        $courier = Courier::where(Courier::EMAIL, $request->email)->first();

        if (empty($courier)) {
            return APIresponse(true, 'sorry email has not been registered ', null, 404);
        }

        $new_password = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,10);

        $courier->update([
            Courier::PASSWORD => bcrypt($new_password)
        ]);

        $courier->toArray();
        $courier[Courier::PASSWORD] = $new_password;

        Mail::to($request->email)->send(new ForgotPasswordEmail($courier));

        return APIresponse(true, 'Password data has been successfully sent to your email', null);
    }
}
