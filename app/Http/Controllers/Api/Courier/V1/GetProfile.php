<?php

namespace App\Http\Controllers\Api\Courier\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GetProfile extends Controller {

    function __invoke(Request $request) {
        $user = $request->user();

        return APIresponse(true, 'Data Berhasil Didapatkan!', $user->toArray());
    }
}
