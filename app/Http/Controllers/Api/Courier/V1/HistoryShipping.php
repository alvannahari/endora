<?php

namespace App\Http\Controllers\Api\Courier\V1;

use App\Http\Controllers\Controller;
use App\Models\ShippingHistory;
use App\Models\Courier;
use Illuminate\Http\Request;

class HistoryShipping extends Controller {

    function __invoke(Request $request) {
        $user = $request->user();
        $result = $user->payment->load(['user' => function ($q) {
            $q->withTrashed();
        }, 'location' => function ($q) {
            $q->withTrashed();
        }, 'histories.status'])->toArray();
        // $courier = Courier::with(['payment.user', 'payment.location', 'payment.histories'])->find($user_id);

        $data['aktif'] = null;
        $data['selesai'] = null;

        foreach ($result as $key => $value) {
            $history = end($value['histories']);

            if ($history[ShippingHistory::STATUS] == '1' || $history[ShippingHistory::STATUS] == '2') 
                $data['aktif'][] = $result[$key];
            else if ($history[ShippingHistory::STATUS] == '3' || $history[ShippingHistory::STATUS] == '4') 
                $data['selesai'][] = $result[$key];
        }

        return APIresponse(true, 'Data Berhasil Di dapatkan', $data);
    }
}
