<?php

namespace App\Http\Controllers\Api\Courier\V1;

use App\Http\Controllers\Controller;
use App\Models\Courier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class Login extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Courier::EMAIL          => 'required|string',
            Courier::PASSWORD       => 'required|string',
            Courier::DEVICE_ID     => 'required|string',
            Courier::TOKEN_FCM     => 'required|string',
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $credentials = $request->only(Courier::EMAIL, Courier::PASSWORD);
        $update[Courier::DEVICE_ID] = $request->device_id;
        $update[Courier::TOKEN_FCM] = $request->token_fcm;

        $courier = Courier::where(Courier::EMAIL, $credentials[Courier::EMAIL])->first();

        if (empty($courier)) 
            return APIresponse(false, 'Akun Username Belum Terdaftar!', null, 404);

        if ($courier->is_active == 0) 
            return APIresponse(false, 'Akun Masih Menunggu Verifikasi dari Admin!!!', 202);

        if(password_verify($credentials[Courier::PASSWORD], $courier->password)){
            $courier->update($update);
            $courier['token'] =  $courier->createToken('Login',['courier'])->accessToken; 
            return APIresponse(true, 'Login Berhasil!', $courier);
        }
        return APIresponse(false, 'Akun Username Belum Terdaftar!', null, 404);
    }
}
