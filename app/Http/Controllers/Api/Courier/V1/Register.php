<?php

namespace App\Http\Controllers\Api\Courier\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Courier;
use Illuminate\Support\Facades\Validator;

class Register extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Courier::EMAIL          => 'required|string|email|unique:couriers',
            Courier::FULLNAME       => 'required|string',
            Courier::GENDER         => 'required|string',
            Courier::PASSWORD       => 'required|string',
            Courier::NUMBER_PLATE   => 'required|string',
            Courier::DATE_BIRTH     => 'required|string',
            Courier::TOKEN_FCM      => 'required|string',
            Courier::DEVICE_ID      => 'required|string'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $credentials = $request->only([
            Courier::EMAIL,
            Courier::FULLNAME,
            Courier::GENDER,
            Courier::PASSWORD,
            Courier::NUMBER_PLATE,
            Courier::DATE_BIRTH,
            Courier::TOKEN_FCM,
            Courier::DEVICE_ID,
        ]);

        $credentials[Courier::PASSWORD] = bcrypt($credentials[Courier::PASSWORD]);
        $credentials[Courier::IS_ACTIVE] = '0';
        $courier = Courier::create($credentials);
        // $courier['token'] =  $courier->createToken('Register',['courier'])->accessToken; 

        return APIresponse(true, 'Registrasi Akun Kurir Berhasil. Silahkan Menunggu untuk konfirmasi dari Admin!', $courier);
    }
}
