<?php

namespace App\Http\Controllers\Api\Courier\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Models\Courier;
use Illuminate\Support\Facades\Storage;

class RemovePhoto extends Controller {

    function __invoke(Request $request) {
        $user = $request->user();

        $arr_image = explode("/",$user->image);
        $image = end($arr_image);
        if ($image == 'default.png') 
            return APIresponse(false, 'Photo Profile has been Empty', null);

        $delete = Storage::disk('public')->delete(Courier::IMAGE_PATH, $image);
        if ($delete){
            $user->update([Courier::IMAGE => null]);
            return APIresponse(true, 'Photo Profile Has Been Successfully Deleted', $user->image);
        }

        return APIresponse(false, 'Photo Profile Failed to Deleted', null);
    }
}
