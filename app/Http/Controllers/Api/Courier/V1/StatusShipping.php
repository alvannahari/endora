<?php

namespace App\Http\Controllers\Api\Courier\V1;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\ShippingHistory;
use App\Models\ShopDetail;
use App\Models\User;
use App\Notifications\Customer\NewStatusShipping;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class StatusShipping extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            ShippingHistory::PAYMENT_ID => 'required',
            ShippingHistory::STATUS     => 'required|numeric',
            ShippingHistory::LATITUDE   => 'required|string',
            ShippingHistory::LONGITUDE  => 'required|string',
            ShippingHistory::IMAGE      => 'image|mimes:png,jpg,jpeg'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $credentials = $request->only(ShippingHistory::PAYMENT_ID, ShippingHistory::STATUS, ShippingHistory::LATITUDE, ShippingHistory::LONGITUDE);

        if ($request->hasfile(ShippingHistory::IMAGE)) {
            $image = Storage::disk('public')->put(ShippingHistory::IMAGE_PATH, $request->file(ShippingHistory::IMAGE));
            $credentials[ShippingHistory::IMAGE] = basename($image);
        }

        DB::beginTransaction();

        try {
            ShippingHistory::create($credentials);
            $credentials['shop_info'] = ShopDetail::first();

            $payment = Payment::find($request->payment_id);
            $customer = $payment->user;
            $dataPayment = $payment->load(['histories' => function ($q) {
                $q->latest()->with('status');
            },'shipmentable','user' => function ($q) {
                $q->withTrashed();
            }]);
            $customer->notify(new NewStatusShipping($dataPayment));

            DB::commit();
            return APIresponse(true, 'Data Pengiriman Berhasil Diperbarui!', $credentials);
        } catch (\Throwable $th) {
            DB::rollBack();
            return APIresponse(false, $th->getMessage(), null, 202);
        }
    }
}
