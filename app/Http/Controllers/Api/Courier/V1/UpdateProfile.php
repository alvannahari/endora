<?php

namespace App\Http\Controllers\Api\Courier\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Courier;

class UpdateProfile extends Controller {

    function __invoke(Request $request) {
        $user = $request->user();
        $validator = Validator::make($request->all(), [
            Courier::EMAIL          => 'required|email|unique:couriers,email,'.$user->id.',id',
            Courier::FULLNAME       => 'required|string',
            Courier::GENDER         => 'required|string',
            Courier::DATE_BIRTH     => 'required|string',
            Courier::NUMBER_PLATE   => 'required|string',
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $credentials = $request->only(Courier::EMAIL, Courier::FULLNAME, Courier::GENDER, Courier::DATE_BIRTH, Courier::NUMBER_PLATE);

        if ($user->update($credentials))
            return APIresponse(true, 'Data Courier Updated Successfully.', $user);

        return APIresponse(false, 'Data Courier Failed to Update!', null);
    }
}
