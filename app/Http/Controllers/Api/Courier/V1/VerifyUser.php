<?php

namespace App\Http\Controllers\Api\Courier\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Courier;
use Illuminate\Support\Facades\Validator;

class VerifyUser extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Courier::PASSWORD       => 'required|string',
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $courier = $request->user();

        if(password_verify($request->password, $courier->password)){
            // $courier->update([
            //     Courier::PASSWORD => bcrypt($request->new_password)
            // ]);
            return APIresponse(true, 'Password Berhasil Terverifikasi!', null);
        }
        return APIresponse(false, 'Password Gagal Terverifikasi', null, 202);
    }
}
