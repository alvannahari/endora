<?php

namespace App\Http\Controllers\Api\Customer\V1\Billboard;

use App\Http\Controllers\Controller;
use App\Models\Billboard;
use App\Models\Offer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Item;

class Detail extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Billboard::ID       => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $data = Billboard::with(['offers' => function ($q) {
            $q->where(Offer::IS_ACTIVE, '1');
        },'categories' => function ($q) {
            $q->with(['subCategories.offer' => function ($q) {
                $q->where(Offer::IS_ACTIVE, '1');
            }, 'items' => function($q) {
                $q->select('items.id', Item::BRAND_ID, Item::NAME, Item::PRICE, Item::WEIGHT)->with(['promo', 'images']);
            }]);
        },'brands' => function ($q) {
            $q->with(['offer' => function ($q) {
                $q->where(Offer::IS_ACTIVE, '1');
            }, 'items' => function($q) {
                $q->select(Item::ID, Item::BRAND_ID, Item::NAME, Item::PRICE, Item::WEIGHT)->has('categories')->with(['promo', 'images']);
            }]);
        }])->find($request->id)->toArray();

        $result = $data;
        unset($result['offers']);
        unset($result['categories']);
        unset($result['items']);

        $arr_ids_offers = [];
        $arr_ids_categories = [];
        $arr_ids_items = [];

        $result['offers'] = [];
        $result['categories'] = [];
        $result['items'] = [];
        
        foreach ($data['offers'] as $key => $value) {
            if (!in_array($value['id'], $arr_ids_offers)) {
                $result['offers'][] = $data['offers'][$key];
                array_push($arr_ids_offers, $value['id']);
            }
        }
        foreach ($data['categories'] as $key => $value) {
            if (!empty($value['sub_categories']['offer']) && count($value['sub_categories']['offer']) > 0) {
                foreach ($value['sub_categories']['offer'] as $keyOffer => $valueOffer) {
                    if (!in_array($valueOffer['id'], $arr_ids_offers)) {
                        $result['offers'][] = $valueOffer;
                        array_push($arr_ids_offers, $valueOffer['id']);
                    }
                }
            }
            if (!empty($value['items']) && count($value['items']) > 0) {
                foreach ($value['items'] as $keyitem => $valueItem) {
                    if (!in_array($valueItem['id'], $arr_ids_items)) {
                        $result['items'][] = $valueItem;
                        array_push($arr_ids_items, $valueItem['id']);
                    }
                }
            }
            if (!in_array($value['id'], $arr_ids_categories)) {
                unset($value['sub_categories']);
                unset($value['items']);
                $result['categories'][] = $value;
                array_push($arr_ids_categories, $value['id']);
            }
        }

        return APIresponse(true, 'Data Billboard Berhasil Didapatkan!!', $result);
    }
}
