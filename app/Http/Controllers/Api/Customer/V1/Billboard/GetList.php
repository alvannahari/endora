<?php

namespace App\Http\Controllers\Api\Customer\V1\Billboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Billboard;

class GetList extends Controller {
    
    function __invoke(Request $request) {

        $data = Billboard::where(Billboard::IS_ACTIVE, '1')->get()->toArray();

        return APIresponse(true, 'Daftar Menu Billboard Berhasil', $data);
    }
}
