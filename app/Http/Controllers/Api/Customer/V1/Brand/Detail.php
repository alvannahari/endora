<?php

namespace App\Http\Controllers\Api\Customer\V1\Brand;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Offer;

class Detail extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Brand::ID       => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $data = Brand::with(['offer' => function ($q) {
            $q->where(Offer::IS_ACTIVE, '1');
        }, 'items' => function($q) {
            $q->select(Item::ID, Item::BRAND_ID, Item::NAME, Item::PRICE, Item::WEIGHT)->has('categories')->with(['promo','images']);
        }])->find($request->id)->toArray();

        return APIresponse(true, 'Data Brand Berhasil Didapatkan !!!', $data);
    }
}
