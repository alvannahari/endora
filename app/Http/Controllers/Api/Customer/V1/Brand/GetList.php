<?php

namespace App\Http\Controllers\Api\Customer\V1\Brand;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;

class GetList extends Controller {

    function __invoke(Request $request) {
        $data = Brand::get();

        return APIresponse(true, 'Data Brand Berhasil Didapatkan !!!', $data);
    }
}
