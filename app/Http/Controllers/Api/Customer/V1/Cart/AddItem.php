<?php

namespace App\Http\Controllers\Api\Customer\V1\Cart;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AddItem extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Cart::ITEM_ID       => 'required|numeric',
            Cart::QTY           => 'required',
            Cart::VARIANT       => 'required',
        ]);

        if ($validator->fails())
            return APIresponse(false, $validator->errors(), null, 400);

        $user = $request->user();
        $item_id = $request->input(Cart::ITEM_ID);
        $variant = $request->input(Cart::VARIANT);
        $qty = $request->input(Cart::QTY);

        $cart = Cart::where([
            Cart::USER_ID => $user->id,
            Cart::ITEM_ID => $item_id,
            Cart::VARIANT => $variant,
        ])->first();

        if (!empty($cart)) {
            $qty_old = $cart->qty;
            $qty_now = (int) $qty_old + (int) $qty;
            $cart->update([Cart::QTY => $qty_now]);
            return APIresponse(true, 'item in cart has been updated', null);
        } 

        $credentials = $request->only(Cart::ITEM_ID, Cart::QTY, Cart::VARIANT);
        $credentials[Cart::USER_ID] = $user->id;
        Cart::create($credentials);
        return APIresponse(true, 'Item has been successfully added to cart.', null);
        
    }
}
