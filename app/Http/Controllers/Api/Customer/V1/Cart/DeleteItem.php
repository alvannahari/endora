<?php

namespace App\Http\Controllers\Api\Customer\V1\Cart;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DeleteItem extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Cart::ID        => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        Cart::find($request->id)->delete();

        return APIresponse(true, 'Item Dikeranjang Telah Dihapus!', null);
    }
}
