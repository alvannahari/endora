<?php

namespace App\Http\Controllers\Api\Customer\V1\Cart;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Offer;

class GetList extends Controller {
    
    function __invoke(Request $request) {
        $user = $request->user();

        // $data = $user->carts->load(['item' => function ($q) {
        //     $q->with(['images', 'promo']);
        // }]);

        $data = Cart::where(Cart::USER_ID, $user->id)->with(['item' => function ($q) {
            $q->with(['images', 'promo']);
        }])->whereHas('item', function ($q) {
            $q->has('categories');
        })->get();

        return APIresponse(true, 'Data Berhasil Didapatkan !!', $data);
    }
}
