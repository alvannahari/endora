<?php

namespace App\Http\Controllers\Api\Customer\V1\Cart;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UpdateQty extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Cart::ID        => 'required|numeric',
            Cart::ITEM_ID   => 'required|numeric',
            Cart::QTY       => 'required|numeric',
        ]);

        
        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 400);
        
        $item = Item::find($request->item_id)->toArray();
        if ($request->qty >= $item[Item::INVENTORY] ) 
            return APIresponse(true, 'jumlah permintaan melebihi batas inventory produk', null, 202);

        $cart = Cart::find($request->id);

        if ($cart->update([Cart::QTY => $request->qty]))
            return APIresponse(true, 'Jumlah Item Berhasil Diperbarui.', null);

        return APIresponse(false, 'Jumlah Item Gagal Diperbarui!', null, 202);
    }
}
