<?php

namespace App\Http\Controllers\Api\Customer\V1\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use App\Models\Offer;

class Detail extends Controller
{
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Category::ID        => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $category  = Category::find($request->id);

        $arr_ids_offers = [];
        $arr_ids_items = [];

        $result['categories'] = [];
        $result['offers'] = [];
        $result['items'] = [];

        if ($category->subCategories()->exists()) {
            $data = $category->subCategories->load(['offer' => function ($q) {
                $q->where(Offer::IS_ACTIVE, '1');
            }, 'items' => function ($q) {
                $q->has('categories')->with('promo', 'images');
            }])->toArray();

            foreach ($data as $key => $value) {
                foreach ($value['offer'] as $keyOffer => $valueOffer) {
                    if (!in_array($valueOffer['id'], $arr_ids_offers)) {
                        $result['offers'][] = $valueOffer;
                        array_push($arr_ids_offers, $valueOffer['id']);
                    }
                }
                foreach ($value['items'] as $keyItem => $valueItem) {
                    if (!in_array($valueItem['id'], $arr_ids_items)) {
                        $result['items'][] = $valueItem;
                        array_push($arr_ids_items, $valueItem['id']);
                    }
                }
                unset($value['offer']);
                unset($value['items']);
                $result['categories'][] = $value;
            }
        } else {
            $result['items'] = $category->items->toArray();
        }

        return APIresponse(true, 'Data Berhasil Didapatkan !!!', $result);

    }
}
