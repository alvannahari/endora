<?php

namespace App\Http\Controllers\Api\Customer\V1\Category;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class GetList extends Controller {

    function __invoke(Request $request) {
        $categories = Category::with('subCategories.subCategories')->doesnthave('categories')->get()->toArray();

        return APIresponse(true, 'Category Data Successfully Retrieved', $categories);
    }
}
