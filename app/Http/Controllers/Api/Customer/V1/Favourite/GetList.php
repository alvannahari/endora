<?php

namespace App\Http\Controllers\Api\Customer\V1\Favourite;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Favourite;

class GetList extends Controller {
    
    function __invoke(Request $request) {
        $user = $request->user();

        // $data = $user->favourites->load(['item.images', 'item.promo']);

        $data = Favourite::where(Favourite::USER_ID, $user->id)->with(['item' => function ($q) {
            $q->with(['images', 'promo']);
        }])->whereHas('item', function ($q) {
            $q->has('categories');
        })->get();

        return APIresponse(true, 'Data Berhasil Didapatkan !!', $data);
    }
}
