<?php

namespace App\Http\Controllers\Api\Customer\V1\Favourite;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Favourite;
use App\Models\Item;

class StateFavourite extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Favourite::ITEM_ID  => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $user = $request->user();
        $item_id = $request->item_id;

        if ($user->is_favourite($item_id)) {
            Favourite::where(Favourite::USER_ID, $user->id)->where(Favourite::ITEM_ID, $item_id)->delete();
            return APIresponse(true, 'Item Favorit Berhasil Dihapus!!', null);
        }

        $credentials = $request->only(Favourite::ITEM_ID);
        $credentials[Favourite::USER_ID] = $user->id;
        Favourite::create($credentials);

        return APIresponse(true, 'Item Favorit Berhasil Ditambahkan!!', null);
    }
}
