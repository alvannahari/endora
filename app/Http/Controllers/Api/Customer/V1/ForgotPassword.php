<?php

namespace App\Http\Controllers\Api\Customer\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgotPasswordEmail;
use Illuminate\Support\Facades\DB;

class ForgotPassword extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            User::EMAIL     => 'required|email'
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 400);
        
        $user = User::where(User::EMAIL, $request->input(User::EMAIL))->first();

        if (empty($user)) 
            return APIresponse(false, 'User not found', null);

        $new_password = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxz'),1,8);

        DB::beginTransaction();
        
        try {
            $user->update([
                User::PASSWORD => bcrypt($new_password)
            ]);
    
            $user->toArray();
            $user['new_password'] = $new_password;
    
            Mail::to($user->email)->send(new ForgotPasswordEmail($user));

            DB::commit();
            
            return APIresponse(true, 'Please check your email you will receive an email from us.', null);
        } catch (\Throwable $th) {
            DB::rollBack();
            return APIresponse(false, $th->getMessage(), null);
        }

    }
}
