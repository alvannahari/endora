<?php

namespace App\Http\Controllers\Api\Customer\V1;

use App\Http\Controllers\Controller;
use App\Models\Billboard;
use App\Models\Brand;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Offer;
use App\Models\Item;

class Home extends Controller {

    function __invoke(Request $request) {
        $page = $request->page ?? 0;
        $start_bill = $page*2;
        $start_offer = $page*4;
        $start_category = $page*2;
        // $start_brand = $page*2;

        $data['billboard'] = Billboard::where(Billboard::IS_IMPORTANT, '1')->where(Billboard::IS_ACTIVE, '1')->skip($start_bill)->limit(2)->get();

        $data['offer_med'] = Offer::where(Offer::TYPE, 'med')->where(Offer::IS_ACTIVE, '1')->skip($start_offer)->limit(4)->get();
        $data['offer_high'] = Offer::where(Offer::TYPE, 'high')->where(Offer::IS_ACTIVE, '1')->skip($start_offer)->limit(4)->get();

        $data['category'] = Category::with('categories')->skip($start_category)->limit(2)->get();

        $data['item'] = Item::with(['categories' => function ($q) {
            $q->where(Category::TYPE, 'promo');
        }, 'images'])->has('categories')->inRandomOrder()->limit(2)->get();

        return APIresponse(true, 'Data Berhasil Didapatkan!', $data);
    }
}
