<?php

namespace App\Http\Controllers\Api\Customer\V1\Item;

use App\Http\Controllers\Controller;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Detail extends Controller { 

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Item::ID    => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $item = Item::find($request->input(Item::ID));
        if (empty($item) || !$item->categories()->exists()) 
            return APIresponse(false, 'Item Not Available', null, 400);

        // $item = Item::with('images', 'categories')->find($request->id);
        $item->load('images', 'categories', 'promo');
        $data['item'] = $item->toArray();
        $data['s_item'] = [];

        $item_ids = [$request->id];
        foreach ($item->categories as $keyCategory => $valCategory) {
            foreach ($valCategory->items as $key => $value) {
                $item_id = $value->id;
                if (!in_array($item_id, $item_ids)) {
                    $data['s_item'][] = $value->load(['images','promo'])->toArray();
                    array_push($item_ids, $item_id);
                }
                if (count($data['s_item']) == 6) break;
            }
        }
        // $s_item = $categories->items;
        // $data['s_item'] = $s_item;

        return APIresponse(true, 'Data Item Berhasil Didapatkan!!', $data);
    }
}
