<?php

namespace App\Http\Controllers\Api\Customer\V1\Item;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GetListPromo extends Controller {
    
    function __invoke(Request $request) {
        $data = Item::inRandomOrder()->with('promo')->whereHas('promo')->get()->toArray();

        return APIresponse(true, 'Data Item Promo Berhasil Didapatkan!', $data);
    }
}
