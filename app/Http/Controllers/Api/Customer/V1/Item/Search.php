<?php

namespace App\Http\Controllers\Api\Customer\V1\Item;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\SearchHistory;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;

class Search extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            'search'    => 'required|string'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $search = $request->search;
        $order = $request->order ?? null;
        $filter_price_min = $request->filter_price_min ?? null;
        $filter_price_max = $request->filter_price_max ?? null;
        $results = Item::where(Item::NAME, 'like', '%' .$search. '%')->with(['promo', 'images'])->has('categories');

        if ($filter_price_min != null)
            $results->whereBetween(Item::PRICE, [$filter_price_min, $filter_price_max]);

        if ($order == 'lowest')
            $results->orderBy(Item::PRICE);
        else if ($order == 'highest')
            $results->orderBy(Item::PRICE, 'DESC');
        else if ($order == 'asc')
            $results->orderBy(Item::NAME);
        else if ($order == 'desc')
            $results->orderBy(Item::NAME, 'DESC');

        $data['search'] = $search;
        $data['order'] = $order;
        $data['filter_price_min'] = $filter_price_min;
        $data['filter_price_max'] = $filter_price_max;
        $data['results'] = $results->get()->toArray();

        $credentials[SearchHistory::USER_ID] = $request->user()->id;
        $credentials[SearchHistory::SEARCH] = $search;

        SearchHistory::create($credentials);

        return APIresponse(true, 'Search was successful', $data);
    }
}
