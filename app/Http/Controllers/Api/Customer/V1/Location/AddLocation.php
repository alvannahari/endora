<?php

namespace App\Http\Controllers\Api\Customer\V1\Location;

use App\Http\Controllers\Controller;
use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AddLocation extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Location::CATEGORY          => 'required|string',
            Location::RECEIVER          => 'required|string',
            Location::PHONE             => 'required|string',
            Location::ADDRESS           => 'required|string',
            Location::SUBDISTRICT_ID    => 'required|numeric',
            Location::POSTAL_CODE       => 'required|numeric',
            // Location::LATITUDE          => 'required|regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/',
            // Location::LONGITUDE         => 'required|regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/',
            Location::LATITUDE          => 'required|string',
            Location::LONGITUDE         => 'required|string',
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };
        
        $user = $request->user();
        $credentials = $request->only([
            Location::CATEGORY,
            Location::RECEIVER,
            Location::PHONE,
            Location::ADDRESS,
            Location::SUBDISTRICT_ID,
            Location::POSTAL_CODE,
            Location::DESCRIPTION,
            Location::LATITUDE,
            Location::LONGITUDE,
        ]);
        $credentials[Location::USER_ID] = $user->id;

        if ($user->locations()->exists())
            $credentials[Location::IS_MAIN] = '0';
        else 
            $credentials[Location::IS_MAIN] = '1';

        Location::create($credentials);

        return APIresponse(true, 'Data Lokasi Berhasil Ditambahkan !!', $credentials);
    }
}
