<?php

namespace App\Http\Controllers\Api\Customer\V1\Location;

use App\Http\Controllers\Controller;
use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DeleteLocation extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Location::ID        => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $location = Location::find($request->id);

        if (!empty($location)) {
            $location->delete();
            return APIresponse(true, 'Data Lokasi User Berhasil Dihapus!!', null);
        }

        return APIresponse(false, 'Data Lokasi Gagal Terhapus, Tidak Ditemukan!!', null);
    }
}
