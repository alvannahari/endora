<?php

namespace App\Http\Controllers\Api\Customer\V1\Location;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Location;

class GetList extends Controller {
    
    function __invoke(Request $request) {
        $user = $request->user();

        $data = $user->locations;

        return APIresponse(true, 'Data Lokasi Berhasil Didapatkan !!', $data->toArray());
    }
}
