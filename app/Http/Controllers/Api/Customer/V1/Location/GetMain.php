<?php

namespace App\Http\Controllers\Api\Customer\V1\Location;

use App\Http\Controllers\Controller;
use App\Models\Location;
use Illuminate\Http\Request;

class GetMain extends Controller {
    
    function __invoke(Request $request) {
        $user_id = $request->user()->id;

        $location = Location::where(Location::USER_ID, $user_id)->where(Location::IS_MAIN, '1')->first();

        if (empty($location)) 
            return APIresponse(false, 'Main location not found.', null, 404);
        
        return APIresponse(true, 'Main location successfully retrieved.', $location);
    }
}
