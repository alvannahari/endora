<?php

namespace App\Http\Controllers\Api\Customer\V1\Location;

use App\Http\Controllers\Controller;
use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class IsMain extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Location::ID        => 'required|numeric'
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 400);
        
        $user_id = $request->user()->id;

        $temp = Location::where(Location::USER_ID, $user_id)->update([Location::IS_MAIN => '0']);

        $location = Location::find($request->id);

        if ($location->update([Location::IS_MAIN => '1']))
            return APIresponse(true, 'Main Location User Successfully to Update.', null);

        return APIresponse(true, 'Main Location User Failed to Update!', null);
    }
}
