<?php

namespace App\Http\Controllers\Api\Customer\V1\Location;

use App\Http\Controllers\Controller;
use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UpdateLocation extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Location::ID                => 'required|numeric',
            Location::CATEGORY          => 'required|string',
            Location::RECEIVER          => 'required|string',
            Location::PHONE             => 'required|string',
            Location::ADDRESS           => 'required|string',
            Location::SUBDISTRICT_ID    => 'required|numeric',
            // Location::LATITUDE          => 'required|regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/',
            // Location::LONGITUDE         => 'required|regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/',
            Location::LATITUDE          => 'required|string',
            Location::LONGITUDE         => 'required|string',
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $location = Location::find($request->id);
        $credentials = $request->only([
            Location::CATEGORY,
            Location::RECEIVER,
            Location::PHONE,
            Location::ADDRESS,
            Location::SUBDISTRICT_ID,
            Location::POSTAL_CODE,
            Location::LATITUDE,
            Location::LONGITUDE,
        ]);

        if ($location->update($credentials))
            return APIresponse(true, 'Data Lokasi Berhasil Diperbarui!!', $credentials);

        return APIresponse(false, 'Terjadi Kesalahan !!', null);
    }
}
