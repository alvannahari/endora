<?php

namespace App\Http\Controllers\Api\Customer\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class Login extends Controller {

    function __invoke(Request $request) {
        $type = $request->input('type');
        if($type != 'username') {
            $validator = Validator::make($request->all(), [
                User::USERNAME      => 'required|string|min:6|regex:/^\S*$/u',
                User::DEVICE_ID     => 'required|string',
                User::TOKEN_FCM     => 'required|string',
                User::TOKEN_SOCMED  => 'required|string',
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                User::USERNAME      => 'required|string|min:6|regex:/^\S*$/u',
                User::PASSWORD      => 'required|string',
                User::DEVICE_ID     => 'required|string',
                User::TOKEN_FCM     => 'required|string',
            ]);
        }

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $credentials = $request->only(User::USERNAME, User::PASSWORD);

        $customer = User::where(User::USERNAME, $credentials[User::USERNAME])->orWhere(User::EMAIL, $credentials[User::USERNAME])->withTrashed()->first();
        // $customer = User::where(User::USERNAME, $credentials[User::USERNAME])->orWhere(User::EMAIL, $credentials[User::USERNAME])->first();
        $update[User::DEVICE_ID] = $request->input(User::DEVICE_ID);
        $update[User::TOKEN_FCM] = $request->input(User::TOKEN_FCM);
        if ($type != 'username') 
            $update[User::TOKEN_SOCMED] = $request->input(User::TOKEN_SOCMED);

            
        if (empty($customer)) 
        return APIresponse(false, 'Akun Username Belum Terdaftar!', null, 404);

        if ($customer->deleted_at != null) 
            return APIresponse(false, 'Akun anda telah di nonaktifkan. Silahkan login menggunakan akun lainnya.', null, 202);
            
        if ($customer->is_active == 0) 
            return APIresponse(false, 'Akun anda sedang di takedown. Silahkan Hubungi Admin.', null, 202);

        if ($type != 'username') {
            $customer->update($update);
            $customer->generateVAIfNotExist();
            $customer->virtualAccounts;
            $customer['token'] =  $customer->createToken('Login',['customer'])->accessToken; 
            return APIresponse(true, 'Login Berhasil!', $customer);
        }

        if (password_verify('endora_password', $customer->password))
            return APIresponse(false, 'Silahkan masuk menggunakan google atau facebook. Password akun ini belum pernah diperbarui!', null);

        if(password_verify($credentials[User::PASSWORD], $customer->password)){
            $customer->update($update);
            $customer->generateVAIfNotExist();
            $customer->virtualAccounts;
            $customer['token'] =  $customer->createToken('Login',['customer'])->accessToken; 
            return APIresponse(true, 'Login Berhasil!', $customer);
        }

        return APIresponse(false, 'Username atau Password Salah!', null, 202);
    }
}
