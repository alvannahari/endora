<?php

namespace App\Http\Controllers\Api\Customer\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NotifList extends Controller {

    function __invoke(Request $request) {
        $user = $request->user();

        $data = $user->unreadNotifications;

        return APIresponse(true, 'Data Notification Successfully.', $data);
    }
}
