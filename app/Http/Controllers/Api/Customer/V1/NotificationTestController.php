<?php

namespace App\Http\Controllers\Api\Customer\V1;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\ShippingHistory;
use App\Notifications\Customer\NewStatusPayment;
use App\Notifications\Customer\NewStatusShipping;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NotificationTestController extends Controller {
    
    function testPayment(Request $request) {
        $user = $request->user();

        $status = $request->input(Payment::STATUS_PAYMENT) ?? '1';
        $id = $request->input(Payment::ID) ?? null;

        if ($id != null) $payment = Payment::find($id);
        else $payment = $user->payments->first();

        $payment->update([
            Payment::STATUS_PAYMENT => $status
        ]);

        $user->notify(new NewStatusPayment($payment));

        return APIresponse(true, 'test notification status payment succesfully.', $payment);
    }

    function testShipment(Request $request) {
        $validator = Validator::make($request->all(), [
            ShippingHistory::PAYMENT_ID => 'required',
            ShippingHistory::STATUS     => 'required|numeric',
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 202);

        $user = $request->user();

        $credentials = $request->only(ShippingHistory::PAYMENT_ID, ShippingHistory::STATUS);
        $credentials[ShippingHistory::LATITUDE] = '-7.9606';
        $credentials[ShippingHistory::LONGITUDE] = '112.6173';
        ShippingHistory::create($credentials);

        $payment = Payment::with(['histories' => function ($q) {
            $q->latest()->with('status');
        },'shipmentable','user'])->find($request->input(ShippingHistory::PAYMENT_ID));

        $user->notify(new NewStatusShipping($payment));

        return APIresponse(true, 'test notification status shipment succesfully.', $payment);
    }
}
