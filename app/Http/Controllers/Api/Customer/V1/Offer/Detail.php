<?php

namespace App\Http\Controllers\Api\Customer\V1\Offer;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Item;
use Illuminate\Http\Request;
use App\Models\Offer;
use Illuminate\Support\Facades\Validator;

class Detail extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Offer::ID        => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $offer  = Offer::find($request->id);

        $data = $offer->load(['categories.offer','categories.items' => function($q) {
            $q->inRandomOrder()->limit(6)->with('promo', 'images');
        },'brands.items' => function($q) {
            $q->inRandomOrder()->limit(6)->with('promo', 'images')->has('categories');
        }])->toArray();

        $data['items'] = [];
        $ids_item = [];

        if (count($data['categories']) > 0) {
            foreach ($data['categories'] as $keyCat => $valCat) {
                foreach ($valCat['items'] as $key => $value) {
                    if (!in_array($value[Item::ID], $ids_item)) {
                        $data['items'][] = $valCat['items'][$key];
                        array_push($ids_item, $value[Item::ID]);
                    }
                }
                unset($data['categories'][$keyCat]['items']);
            }
        }
        if (count($data['brands']) > 0) {
            foreach ($data['brands'] as $keyBrand => $valBrand) {
                foreach ($valBrand['items'] as $key => $value) {
                    if (!in_array($value[Item::ID], $ids_item)) {
                        $data['items'][] = $valBrand['items'][$key];
                        array_push($ids_item, $value[Item::ID]);
                    }
                }
                unset($data['brands'][$keyBrand]['items']);
            }
        }

        return APIresponse(true, 'Data Berhasil Didapatkan !!!', $data);

    }
}
