<?php

namespace App\Http\Controllers\Api\Customer\V1\Offer;

use App\Http\Controllers\Controller;
use App\Models\Offer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GetList extends Controller {

    function __invoke(Request $request) {
        $data = Offer::where(Offer::IS_ACTIVE, '1')->get()->toArray();

        return APIresponse(true, 'Data Penawaran Berhasil Didapatkan !', $data);
    }
}
