<?php

namespace App\Http\Controllers\Api\Customer\V1\Payment;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\ReasonCancel;
use Illuminate\Support\Facades\DB;
use Xendit\Invoice;
use Xendit\Xendit;

class Canceled extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Payment::ID             => 'required',
            ReasonCancel::REASON     => 'required|string',
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 400);

        $payment = Payment::find($request->id);

        if ($payment->checkCancel()) 
            return APIresponse(false, 'payment has been previously canceled! ', null);

        DB::beginTransaction();

        try {
            $payment->setCancelled($request, 'customer');

            if ($payment->type != 'bca-va'){
                Xendit::setApiKey(config('services.xendit.key'));
                Invoice::expireInvoice($payment->xendit_id);
            }

            DB::commit();
            return APIresponse(true, 'Status Payment Successfully to Canceled.', null);
        } catch (\Throwable $th) {
            DB::rollBack();
            return APIresponse(false, 'Something Wrong!', $th->getMessage(), 202);
        }
    }
}
