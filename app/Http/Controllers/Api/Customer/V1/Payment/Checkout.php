<?php

namespace App\Http\Controllers\Api\Customer\V1\Payment;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Payment;
use App\Helpers\Constants;
use App\Models\Item;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Notifications\Admin\NewOrder;
use Illuminate\Support\Facades\DB;
use App\Models\Xendits;
use App\Notifications\Customer\NewStatusPayment;
use Xendit\Invoice;
use Xendit\Xendit;

class Checkout extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Payment::LOCATION_ID        => 'required|numeric',
            Payment::COURIER_ID         => 'numeric',
            Payment::COURIER_TYPE       => 'required|string',
            Payment::TOTAL_SHIPMENT     => 'required|numeric',
            Payment::TOTAL              => 'required|numeric',
            Payment::TYPE               => 'required|string',
            'item'                      => 'required|array'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $user = $request->user();
        $items = $request->item;

        $payments = new Payment();
        $code_id = $payments->generatePaymentId();

        DB::beginTransaction();

        try {
            //check inventory and stock reduction
            foreach ($items as $key => $value) {
                $item = Item::find($value['item_id']);
                $check = (int) $item->inventory > (int) $value['qty'];
                if (!$check) {
                    DB::rollBack();
                    return APIresponse(false, 'Ada beberapa item yang kehabisan stok', null);
                } else {
                    $item->stockReduction($value['qty']);
                }
            }
            
            if($request->type === "bca-va"){
                $dataInvoice = Constants::$BCA_PAYMENT_DATA;
            }else{
                $bank_request = explode('-',$request->input(Payment::TYPE));

                $credentialsXendit[Xendits::EXTERNAL_ID] = $code_id;
                $credentialsXendit[Xendits::PAYER_EMAIL] = $user->email;
                $credentialsXendit[Xendits::AMOUNT] = $request->total;
                $credentialsXendit[Xendits::DESCRIPTION] = 'Pembayaran untuk pemesanan di Endora Care.';
                $credentialsXendit[Xendits::INVOICE_DURATION] = 3600;
                $credentialsXendit[Xendits::PAYMENT_METHODS] = [count($bank_request) > 1 ? strtoupper($bank_request[1]) : 'BRI'];
                $credentialsXendit[Xendits::CURRENCY] = 'IDR'; 
                Xendit::setApiKey(config('services.xendit.key'));
                $dataInvoice = Invoice::create($credentialsXendit);
            }

            $credentials = $request->only(Payment::LOCATION_ID, Payment::COURIER_TYPE, Payment::DESCRIPTION, Payment::TOTAL_SHIPMENT, Payment::TOTAL, Payment::TYPE);
            $credentials[Payment::ID]               = $code_id;
            $credentials[Payment::USER_ID]          = $user->id;
            $credentials[Payment::TYPE]             = $request->input(Payment::TYPE);
            $credentials[Payment::TYPE_UID]         = $dataInvoice['id'] ?? '009990009991111';
            $credentials[Payment::EXPIRE_DATE]      = now()->addHour(1);
            
            if ($request->input(Payment::COURIER_TYPE) == 'courier') {
                $credentials[Payment::COURIER_ID] = '0';
                $credentials[Payment::INVOICE_NUMBER]   = $code_id;
            }
            else {
                $credentials[Payment::INVOICE_NUMBER]   = $dataInvoice['available_banks'][0]['bank_account_number'] ?? '009990009991111';
                $credentials[Payment::COURIER_ID] = $request->input(Payment::COURIER_ID);
            }

            $payment = Payment::create($credentials);

            if($request->type === "bca-va"){
                $bcava = $user->virtualAccounts->where("type","bca")->first();
                if(!$bcava){
                    throw new \Exception("BCA VA not found");
                }
                $payment->vaPayments()->create([
                    "va_id" => $bcava->id
                ]);
            } else {
                $payment->transaction()->create([
                    Transaction::ID => $code_id,
                    Transaction::INCOME => $payment->total,
                    Transaction::STATUS => '0',
                ]);
            }

            foreach ($items as $key => $value) {
                $credentials_item[Order::PAYMENT_ID] = $code_id;
                $credentials_item[Order::ITEM_ID] = $value[Order::ITEM_ID];
                $credentials_item[Order::NAME] = $value[Order::NAME];
                $credentials_item[Order::PRICE] = $value[Order::PRICE];
                $credentials_item[Order::QTY] = $value[Order::QTY];
                $credentials_item[Order::VARIANT] = $value[Order::VARIANT];
                $credentials_item[Order::T_WEIGHT] = $value[Order::T_WEIGHT];
                $credentials_item[Order::DISCOUNT] = $value[Order::DISCOUNT];
                $credentials_item[Order::TOTAL] = $value[Order::TOTAL];
                $credentials_item[Order::NOTE] = $value[Order::NOTE];

                Order::create($credentials_item);
                Cart::where(Cart::USER_ID, $user->id)->where(Cart::ITEM_ID, $value[Order::ITEM_ID])->delete();
            }

            Admin::find(1)->notify(new NewOrder($request->user(), $payment));
            $user->notify(new NewStatusPayment($payment));

            $credentials['transaction'] = $dataInvoice ?? 'The payment method has not been set';

            DB::commit();
            return APIresponse(true, 'Data payment successfully to created.', $credentials);
        } catch (\Throwable $th) {
            DB::rollBack();
            return APIresponse(false, $th->getMessage(), null, 202);
        }
    }
}
