<?php

namespace App\Http\Controllers\Api\Customer\V1\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ShippingHistory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class Confirmation extends Controller
{
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            ShippingHistory::PAYMENT_ID => 'required',
            ShippingHistory::LATITUDE   => 'required|string',
            ShippingHistory::LONGITUDE  => 'required|string',
            ShippingHistory::IMAGE      => 'image|mimes:png,jpg,jpeg'
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 400);
        
        $credentials = $request->only(ShippingHistory::PAYMENT_ID, ShippingHistory::LATITUDE, ShippingHistory::LONGITUDE);
        $credentials[ShippingHistory::STATUS] = '4';

        if ($request->hasfile(ShippingHistory::IMAGE)) {
            $photo = Storage::disk('public')->put(ShippingHistory::IMAGE_PATH, $request->file(ShippingHistory::IMAGE));
            $credentials[ShippingHistory::IMAGE] = basename($photo);
        }

        ShippingHistory::create($credentials);

        return APIresponse(true, 'Confirmation Item Shipping from Buyer', null);
    }
}
