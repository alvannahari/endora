<?php

namespace App\Http\Controllers\Api\Customer\V1\Payment;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\Shipment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Steevenz\Rajaongkir;

class Detail extends Controller {
    
    function __invoke($notifId = null, Request $request) {
        $validator = Validator::make($request->all(), [
            Payment::ID     => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        if ($notifId != null) {
            $notif = $request->user()->unreadNotifications()->where('id', $notifId)->first();
            if (!empty($notif)) 
                $notif->markAsRead();
        }

        $payment_id = $request->id;
        $payment = Payment::with(['shipmentable', 'location' => function ($q) {
            $q->withTrashed();
        }, 'order.item' => function ($q) {
            $q->withTrashed()->with('images');
        }, 'histories.status', 'review'])->find($payment_id)->toArray();

        if ($payment[Payment::COURIER_TYPE] == 'shipment') {
            $rajaongkir = new Rajaongkir(config('services.rajaongkir.key'), config('services.rajaongkir.package'));
            $waybill = $rajaongkir->getWaybill($payment[Payment::WAYBILL_NUMBER], $payment['shipmentable'][Shipment::CODE]);

            $payment['histories'] = $waybill;
        }

        if (empty($payment['histories'])) $payment['histories'] = [];

        return APIresponse(true, 'Data Detail Succesfully Retrieved.', $payment);
    }
}
