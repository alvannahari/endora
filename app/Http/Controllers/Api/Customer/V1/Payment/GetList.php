<?php

namespace App\Http\Controllers\Api\Customer\V1\Payment;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Shipment;
use App\Models\ShippingHistory;
use Illuminate\Http\Request;
use Steevenz\Rajaongkir;

class GetList extends Controller {

    function __invoke(Request $request) {
        $user = $request->user();

        $data['unpaid'] = Payment::with(['order' => function ($q_order) {
            $q_order->with(['item' => function($q_item){
                $q_item->withTrashed()->with('images')->select(Item::ID,Item::NAME);
            }])->select(Order::ID, Order::PAYMENT_ID, Order::ITEM_ID);
        }])->where(Payment::STATUS_PAYMENT, '0')->where(Payment::USER_ID, $user->id)->get()->toArray();

        $data['packed'] = [];
        $data['sent'] = [];
        $data['finished'] = [];
        $data['submission'] = Payment::with(['order' => function ($q_order) {
            $q_order->with(['item' => function($q_item){
                $q_item->withTrashed()->with('images')->select(Item::ID,Item::NAME);
            }])->select(Order::ID, Order::PAYMENT_ID, Order::ITEM_ID);
        }])->where(Payment::STATUS_PAYMENT, '97')->where(Payment::USER_ID, $user->id)->get()->toArray();

        $data['canceled'] = Payment::with(['cancel', 'order' => function ($q_order) {
            $q_order->with(['item' => function($q_item){
                $q_item->withTrashed()->with('images')->select(Item::ID,Item::NAME);
            }])->select(Order::ID, Order::PAYMENT_ID, Order::ITEM_ID);
        }])->where(Payment::USER_ID, $user->id)->whereIn(Payment::STATUS_PAYMENT, ['99','98'])->get()->toArray();

        $payments = Payment::with(['shipmentable','histories' => function ($q) {
            $q->orderBy(ShippingHistory::CREATED_AT, 'desc')->with('status');
        }, 'submission', 'review', 'order' => function ($q_order) {
            $q_order->with(['item' => function($q_item){
                $q_item->withTrashed()->with('images')->select(Item::ID,Item::NAME);
            }])->select(Order::ID, Order::PAYMENT_ID, Order::ITEM_ID);
        }])->where(Payment::STATUS_PAYMENT, '1')->where(Payment::USER_ID, $user->id)->get()->toArray();

        foreach ($payments as $key => $value) {
            if($value[Payment::COURIER_TYPE] == 'courier') {
                $histories = reset($value['histories']);

                if ( $histories == null || $histories[ShippingHistory::STATUS] == '1') 
                    $data['packed'][] = $payments[$key];
                else if ($histories[ShippingHistory::STATUS] == '2') 
                    $data['sent'][] = $payments[$key];
                else if ($histories[ShippingHistory::STATUS] == '3' || $histories[ShippingHistory::STATUS] == '4') 
                    $data['finished'][] = $payments[$key];
                else if ($histories[ShippingHistory::STATUS] == '5') 
                    $data['submission'][] = $payments[$key];
            } else {
                $rajaongkir = new Rajaongkir(config('services.rajaongkir.key'), config('services.rajaongkir.package'));
                $waybill = $rajaongkir->getWaybill($value[Payment::WAYBILL_NUMBER], $value['shipmentable'][Shipment::CODE]);

                if (!$waybill || count($waybill) < 1) {
                    $data['packed'][] = $payments[$key];
                } else if ($waybill['summary']['status'] == 'ON PROCESS') {
                    $data['sent'][] = $payments[$key];
                } else if ($waybill['summary']['status'] == 'DELIVERED') {
                    $data['finished'][] = $payments[$key];
                };
            }
        }
        
        return APIresponse(true, 'Daftar Pembayaran Berhasil Didapatkan !!!', $data);
    }
}
