<?php

namespace App\Http\Controllers\Api\Customer\V1\Payment;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\Transaction;
use App\Models\Xendits;
use App\Notifications\Customer\NewStatusPayment;
use Illuminate\Http\Request;

class PaidCallback extends Controller {
    
    function __invoke(Request $request) {
        $request->toArray();
        $user = $request->user();
        $payment = Payment::find($request[Xendits::EXTERNAL_ID]);
        if (empty($payment))
            return APIresponse(false, 'Data Not Found', null, 400);

        if ($request['status'] = 'PAID') {
            $payment->setPaid()->save();
            $payment->transaction->update([
                Transaction::STATUS => '1'
            ]);
            // $user->notify(new NewStatusPayment($payment));
            return APIresponse(true, 'data new status', 'data diterima dengan baik.');
        }

        return APIresponse(false, 'Data not found', null, 202);
    }
}
