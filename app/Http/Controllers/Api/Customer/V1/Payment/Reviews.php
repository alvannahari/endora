<?php

namespace App\Http\Controllers\Api\Customer\V1\Payment;

use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\ShippingHistory;

class Reviews extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Review::PAYMENT_ID          => 'required',
            Review::RATING              => 'required|numeric',
            Review::REVIEW              => 'required|string',
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 400);

        $credentials = $request->only(Review::PAYMENT_ID, Review::RATING, Review::REVIEW);

        Review::create($credentials);

        return APIresponse(true, 'Review Data Successfully to Submitted', null);
    }
}
