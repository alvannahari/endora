<?php

namespace App\Http\Controllers\Api\Customer\V1\Payment;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SetSubmission extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Payment::ID => 'required'
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 400);
        
        $payment = Payment::find($request->input(Payment::ID));
        $payment->setSubmission();

        return APIresponse(true, 'status submission has been successfully received.', $payment);
    }
}
