<?php

namespace App\Http\Controllers\Api\Customer\V1;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Offer;
use Illuminate\Support\Facades\Validator;

class Preview extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Offer::ID       => 'required|numeric',
            Offer::TYPE     => 'required|string',
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $id = $request->id;
        $type = $request->type;

        if ($type == 'offer') {
            $data = Offer::where(Offer::ID, $id)->with('categories', 'brands')->get()->toArray();
        } else if ($type == 'category') {
            $data = Category::where(Category::ID, $id)->with('subCategories')->get();
            if (empty($data->sub_categories)) {
                $data = $data->load('items');
            };
            $data = $data->toArray();
        } else if ($type == 'brand') {
            $data = Brand::where(Brand::ID, $id)->with(['offer', 'items.images'])->get()->toArray();
        } else {
            return APIresponse(false, 'Terjadi Kesalahan !', 400);
        }

        return APIresponse(true, 'Data Berhasil Didapatkan', $data);
    }
}

