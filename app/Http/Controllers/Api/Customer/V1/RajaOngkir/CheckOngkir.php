<?php

namespace App\Http\Controllers\Api\Customer\V1\RajaOngkir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Steevenz\Rajaongkir;

class CheckOngkir extends Controller {

    const ORIGIN = 'origin';
    const DESTINATION = 'destination';
    const WEIGHT = 'weight';
    const COURIER = 'courier';
    const SERVICE = 'service';

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Self::ORIGIN        => 'required',
            Self::DESTINATION   => 'required',
            Self::WEIGHT        => 'required',
            Self::COURIER       => 'required',
            Self::SERVICE       => 'required'
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 400);
        
        $rajaongkir = new Rajaongkir(config('services.rajaongkir.key'), config('services.rajaongkir.package'));

        $courier = $request->input(Self::COURIER);
        if ($courier == 'j&t') 
            $courier = 'jnt';

        $data = $rajaongkir->getCost(
            ['city' => $request->input(Self::ORIGIN)], 
            ['subdistrict' => $request->input(Self::DESTINATION)], 
            $request->input(Self::WEIGHT), 
            $courier
        );

        if (empty($data)) 
            return APIresponse(false, 'Bad Request!', null, 400);

        $result['code'] = $data['code'];
        $result['name'] = $data['name']; 

        foreach ($data['costs'] as $key => $value) {
            if ($request->input(Self::SERVICE) == $value[Self::SERVICE]) {
                $result['cost'] = $value;
            }
        }
        
        if (empty($result['cost'])) 
            return APIresponse(false, 'Delivery to this area is not supported.', $result);

        return APIresponse(true, 'Check ongkos kirim pengiriman berhasil.', $result);
    }
}
