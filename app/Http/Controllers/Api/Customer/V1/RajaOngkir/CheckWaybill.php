<?php

namespace App\Http\Controllers\Api\Customer\V1\RajaOngkir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Steevenz\Rajaongkir;

class CheckWaybill extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            'waybill' => 'required',
            'courier' => 'required'
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 400);

        $rajaongkir = new Rajaongkir(config('services.rajaongkir.key'), config('services.rajaongkir.package'));
        $waybill = $rajaongkir->getWaybill($request->input('waybill'), $request->input('courier'));

        if ($waybill) 
            return APIresponse(true, 'Data waybill successfully retrieved.', $waybill);
        
        return APIresponse(false, 'Data waybill not found!', $rajaongkir->getErrors());
    }
}
