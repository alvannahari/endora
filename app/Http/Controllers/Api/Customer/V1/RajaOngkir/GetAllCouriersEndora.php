<?php

namespace App\Http\Controllers\Api\Customer\V1\RajaOngkir;

use App\Http\Controllers\Controller;
use App\Models\Shipment;
use Illuminate\Http\Request;
use Steevenz\Rajaongkir;
use Illuminate\Support\Facades\Cache;

class GetAllCouriersEndora extends Controller {
    
    function __invoke() {
        // $shipment = Cache::remember('courier-express-list', 60, function () {
        //     $rajaongkir = new Rajaongkir(config('services.rajaongkir.key'), config('services.rajaongkir.package'));
        //     return $rajaongkir->getSupportedCouriers();
        // });

        $shipment = Shipment::where(Shipment::IS_ACTIVE, '1')->get()->toArray();

        return APIresponse(true, 'Data all shipment express succesfully retrieved.', $shipment);
    }
}
