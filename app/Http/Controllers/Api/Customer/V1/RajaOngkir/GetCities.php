<?php

namespace App\Http\Controllers\Api\Customer\V1\RajaOngkir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Steevenz\Rajaongkir;

class GetCities extends Controller {
    
    function __invoke(Request $request) {
        $city = Cache::remember('cities-list', 900, function () {
            $rajaongkir = new Rajaongkir(config('services.rajaongkir.key'), config('services.rajaongkir.package'));
            return $rajaongkir->getCities();
        });
        return APIresponse(true, 'Data city successfully retrieved.', $city);
    }
}
