<?php

namespace App\Http\Controllers\Api\Customer\V1\RajaOngkir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Steevenz\Rajaongkir;

class GetCitiesByProvinceId extends Controller{
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            'id'        => 'required'
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 400);

        $provinceId = $request->input('id');

        $cities = Cache::remember('cities-province-'.$provinceId, 900, function () use ($provinceId) {
            $rajaongkir = new Rajaongkir(config('services.rajaongkir.key'), config('services.rajaongkir.package'));
            return $rajaongkir->getCities($provinceId);
        });

        return APIresponse(true, 'Data city succesfully retrieved.', $cities);
    }
}
