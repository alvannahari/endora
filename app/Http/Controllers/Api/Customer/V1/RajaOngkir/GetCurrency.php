<?php

namespace App\Http\Controllers\Api\Customer\V1\RajaOngkir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Steevenz\Rajaongkir;
use Illuminate\Support\Facades\Cache;

class GetCurrency extends Controller {
    
    function __invoke() {
        $currency = Cache::remember('currency', 900, function () {
            $rajaongkir = new Rajaongkir(config('services.rajaongkir.key'), config('services.rajaongkir.package'));
            return $rajaongkir->getCurrency();
        });

        return APIresponse(true, 'Data all currency succesfully retrieved.', $currency);
    }
}
