<?php

namespace App\Http\Controllers\Api\Customer\V1\RajaOngkir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Steevenz\Rajaongkir;

class GetDetailSubdistrict extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            'subdistrict_id' => 'required|numeric'
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 400);
        
        $subdistrict_id = $request->input('subdistrict_id');

        $subdistrict = Cache::remember('subdistrict-'.$subdistrict_id, 900, function () use ($subdistrict_id) {
            $rajaongkir = new Rajaongkir(config('services.rajaongkir.key'), config('services.rajaongkir.package'));
            return $rajaongkir->getSubdistrict($subdistrict_id);
        });

        return APIresponse(true, 'Data city succesfully retrieved.', $subdistrict);
    }
}
