<?php

namespace App\Http\Controllers\Api\Customer\V1\RajaOngkir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Steevenz\Rajaongkir;

class GetProvinces extends Controller {

    function __invoke(Request $request) {
        $provinces = Cache::remember('provinces-list', 900, function () {
            $rajaongkir = new Rajaongkir(config('services.rajaongkir.key'), config('services.rajaongkir.package'));
            return $rajaongkir->getProvinces();
        });

        return APIresponse(true, 'Data all provinces succesfully retrieved.', $provinces);
    }
}
