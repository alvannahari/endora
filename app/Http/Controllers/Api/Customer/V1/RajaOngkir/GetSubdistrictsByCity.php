<?php

namespace App\Http\Controllers\Api\Customer\V1\RajaOngkir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Steevenz\Rajaongkir;
use Illuminate\Support\Facades\Cache;

class GetSubdistrictsByCity extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            'city_id'    => 'required'
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 400);
        
        $city_id = $request->input('city_id');

        $cities = Cache::remember('subdistrict-city-'.$city_id, 900, function () use ($city_id) {
            $rajaongkir = new Rajaongkir(config('services.rajaongkir.key'), config('services.rajaongkir.package'));
            return $rajaongkir->getSubdistricts($city_id);
        });

        return APIresponse(true, 'Data city succesfully retrieved.', $cities);
    }
}
