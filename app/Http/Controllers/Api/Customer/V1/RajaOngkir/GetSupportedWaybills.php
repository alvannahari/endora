<?php

namespace App\Http\Controllers\Api\Customer\V1\RajaOngkir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Steevenz\Rajaongkir;
use Illuminate\Support\Facades\Cache;

class GetSupportedWaybills extends Controller {
    
    function __invoke() {
        $waybills = Cache::remember('supported-waybills', 900, function () {
            $rajaongkir = new Rajaongkir(config('services.rajaongkir.key'), config('services.rajaongkir.package'));
            return $rajaongkir->getSupportedWayBills();
        });

        return APIresponse(true, 'Data all waybills succesfully retrieved.', $waybills);
    }
}
