<?php

namespace App\Http\Controllers\Api\Customer\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Register extends Controller {
    
    function __invoke(Request $request) {
        $type = $request->input('type');
        if($type != 'username') {
            $validator = Validator::make($request->all(), [
                User::USERNAME      => 'required|string|min:6|regex:/^\S*$/u',
                User::EMAIL         => 'required|string|email|unique:users',
                User::TOKEN_FCM     => 'required|string',
                User::TOKEN_SOCMED  => 'required|string',
                User::DEVICE_ID     => 'required|string'
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                User::USERNAME      => 'required|string|min:6|regex:/^\S*$/u|unique:users',
                User::EMAIL         => 'required|string|email|unique:users',
                User::PASSWORD      => 'required|string',
                User::TOKEN_FCM     => 'required|string',
                User::DEVICE_ID     => 'required|string'
            ]);
        }

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $customer = null;
        if($type != 'username') {
            $username = $request->input(User::USERNAME);
            if (User::where(User::USERNAME, $username)->withTrashed()->exists() ) {
                $user = new User;
                $username = $username.$user->generateUsername();
            }
            $credentials = $request->only([User::EMAIL, User::TOKEN_FCM, User::DEVICE_ID, User::TOKEN_SOCMED]);
            $credentials[User::USERNAME] = $username;
            $credentials[User::PASSWORD] = bcrypt('endora_password');
            $credentials[User::IS_ACTIVE] = '1';
            $customer = User::create($credentials);
            $customer->generateVAIfNotExist();
            $customer->virtualAccounts;
            $customer['token'] =  $customer->createToken('Register',['customer'])->accessToken; 
        } else {
            $credentials = $request->only([User::USERNAME, User::EMAIL, User::PASSWORD, User::TOKEN_FCM, User::DEVICE_ID]);
            $credentials[User::PASSWORD] = bcrypt($credentials[User::PASSWORD]);
            $credentials[User::IS_ACTIVE] = '1';
            $customer = User::create($credentials);
            $customer->generateVAIfNotExist();
            $customer->virtualAccounts;
            $customer['token'] =  null;
        }

        // if($customer){
        //     $customer->generateVAIfNotExist();
        //     $customer->virtualAccounts;
        // }

        return APIresponse(true, 'Registrasi Akun Pelanggan Berhasil!', $customer);
    }
}
