<?php

namespace App\Http\Controllers\Api\Customer\V1\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class ChangePassword extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            // User::PASSWORD      => 'required',
            'new_password'      => 'confirmed|min:8|different:password',
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };
        
        $user = $request->user();

        $user->update([
            User::PASSWORD => bcrypt($request->new_password)
        ]);
        return APIresponse(true, 'Password User Updated Successfully.', null);
        // if(password_verify($request->password, $user->password)){
        // }

        // return APIresponse(false, 'Password User Failed to Update!', null);
    }
}
