<?php

namespace App\Http\Controllers\Api\Customer\V1\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ChangePhoto extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            User::IMAGE      => 'required|image|mimes:png,jpg,jpeg|max:8096',
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 400);

        $user = $request->user();

        $arr_image = explode("/",$user->image);
        $image = end($arr_image);

        if ($image != 'default.png') 
            Storage::disk('public')->delete(User::IMAGE_PATH, $image);

        $photo = Storage::disk('public')->put(User::IMAGE_PATH, $request->file(User::IMAGE));

        if ($user->update([User::IMAGE => basename($photo)]))
            return APIresponse(true, 'Photo Profile Updated Successfully', $user);

        return APIresponse(false, 'Photo Profile Failed to Update!', null);
    }
}
