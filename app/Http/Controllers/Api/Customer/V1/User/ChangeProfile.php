<?php

namespace App\Http\Controllers\Api\Customer\V1\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class ChangeProfile extends Controller {
    
    function __invoke(Request $request) {
        $user = $request->user();
        $validator = Validator::make($request->all(), [
            User::USERNAME      => 'required|string|regex:/^\S*$/u|unique:users,username,'.$user->id.',id',
            User::EMAIL         => 'required|email|unique:users,email,'.$user->id.',id'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 400);
        };

        $credentials = $request->only(User::USERNAME, User::EMAIL);

        if($user->update($credentials))
            return APIresponse(true, 'User Data Updated Successfully.', $user);

        return APIresponse(false, 'User Data Failed to Update!', null);
    }
}
