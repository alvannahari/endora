<?php

namespace App\Http\Controllers\Api\Customer\V1\User;

use App\Http\Controllers\Controller;
use App\Models\SearchHistory;
use Illuminate\Http\Request;

class ClearSearch extends Controller {
    
    function __invoke(Request $request) {
        $user = $request->user();

        $searchs = SearchHistory::where(SearchHistory::USER_ID, $user->id);

        if ($searchs->delete())
            return APIresponse(true, 'Search Histories Deleted Successfully.', null);

        return APIresponse(false, 'Search Histories Failed to Delete!', null);
    }
}
