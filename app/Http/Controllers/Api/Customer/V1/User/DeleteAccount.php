<?php

namespace App\Http\Controllers\Api\Customer\V1\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class DeleteAccount extends Controller {
    
    function __invoke(Request $request) {
        $user = $request->user();

        $arr_image = explode("/",$user->image);
        $image = end($arr_image);

        if ($image != 'default.png') {
            $delete = Storage::disk('public')->delete(User::IMAGE_PATH, $image);
            if (!$delete)
                return APIresponse(false, 'Photo Profile Failed to Delete!', null);
        }

        if ($user->delete())
            return APIresponse(true, 'Data User Has Been Successfully Deleted.', null);

        return APIresponse(false, 'Data User Failed to Delete!', null);
    }
}
