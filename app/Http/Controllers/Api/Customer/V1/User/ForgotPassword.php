<?php

namespace App\Http\Controllers\Api\Customer\V1\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgotPasswordEmail;

class ForgotPassword extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            User::EMAIL     => 'required|email'
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 4004);
        
        $user = User::where(User::EMAIL, $request->email)->first();

        if (empty($user)) {
            return APIresponse(true, 'sorry email has not been registered ', null, 202);
        }

        $new_password = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,10);

        $user->update([
            User::PASSWORD => bcrypt($new_password)
        ]);

        $user->toArray();
        $user[User::PASSWORD] = $new_password;

        Mail::to($request->email)->send(new ForgotPasswordEmail($user));

        return APIresponse(true, 'Password data has been successfully sent to your email', null);
    }
}
