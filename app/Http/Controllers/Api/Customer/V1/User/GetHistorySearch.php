<?php

namespace App\Http\Controllers\Api\Customer\V1\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GetHistorySearch extends Controller {
    
    function __invoke(Request $request) {
        $user = $request->user();

        $data = $user->searchs;

        return APIresponse(true, 'user search history has been successfully obtained', $data);
    }
}
