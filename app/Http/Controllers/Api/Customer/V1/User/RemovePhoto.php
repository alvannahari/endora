<?php

namespace App\Http\Controllers\Api\Customer\V1\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RemovePhoto extends Controller {

    function __invoke(Request $request) {
        $user = $request->user();

        $arr_image = explode("/",$user->image);
        $image = end($arr_image);
        if ($image == 'default.png') 
            return APIresponse(false, 'Photo Profile has been Empty', null);

        $delete = Storage::disk('public')->delete(User::IMAGE_PATH, $image);
        if ($delete){
            $user->update([User::IMAGE => null]);
            return APIresponse(true, 'Photo Profile Has Been Successfully Deleted', $user->image);
        }

        return APIresponse(false, 'Photo Profile Failed to Deleted', null);
    }
}
