<?php

namespace App\Http\Controllers\Api\Customer\V1\Xendit\Invoice;

use App\Http\Controllers\Controller;
use App\Models\Xendits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Xendit\Xendit;

class Create extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Xendits::EXTERNAL_ID        => 'required',
            Xendits::PAYER_EMAIL        => 'required|email',
            Xendits::AMOUNT             => 'required',
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 202);
        
        $credentials = $request->only(Xendits::EXTERNAL_ID, Xendits::PAYER_EMAIL, Xendits::AMOUNT);
        $credentials[Xendits::DESCRIPTION] = 'Pembayaran untuk pemesanan di Endora Care.';
        $credentials[Xendits::INVOICE_DURATION] = 3600;
        $credentials[Xendits::PAYMENT_METHODS] = ['BCA'];
        $credentials[Xendits::CURRENCY] = 'IDR';

        Xendit::setApiKey('xnd_development_JxUTuqsCKcgJFsK8EVPavJblC2AXWwBkpTcbETuttMf7IrU69950UdE4g5S2');
        $data = \Xendit\Invoice::create($credentials);

        return APIresponse(true, 'Data invoice successfully created.', $data);
    }
}
