<?php

namespace App\Http\Controllers\Api\Customer\V1\Xendit\VA;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\Xendits;
use Illuminate\Http\Request;

class CallbackCreateVA extends Controller {
    
    function __invoke(Request $request) {
        $credentials = $request->only(Xendits::EXTERNAL_ID, Xendits::ID, Xendits::BANK_CODE, 'status', 'account_number', 'created');

        $payment = Payment::find($credentials[Xendits::EXTERNAL_ID])->update([
            Payment::INVOICE_NUMBER => $credentials['account_number'],
            Payment::STATUS_PAYMENT => 1,
        ]);

        return true;
    }
}
