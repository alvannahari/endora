<?php

namespace App\Http\Controllers\Api\Customer\V1\Xendit\VA;

use App\Http\Controllers\Controller;
use App\Models\Xendits;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Xendit\Xendit;

class CreateVA extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Xendits::EXTERNAL_ID        => 'required',
            Xendits::BANK_CODE          => 'required',
            Xendits::NAME               => 'required',
            Xendits::EXPECTED_AMOUNNT   => 'required',
        ]);

        if ($validator->fails()) 
            return APIresponse(false, $validator->errors(), null, 202);

        $request->toArray();

        $credentials = [
            Xendits::EXTERNAL_ID        => $request[Xendits::EXTERNAL_ID],
            Xendits::BANK_CODE          => $request[Xendits::BANK_CODE],
            Xendits::NAME               => $request[Xendits::NAME],
            Xendits::EXPECTED_AMOUNNT   => $request[Xendits::EXPECTED_AMOUNNT],
            Xendits::IS_CLOSED          => true,
            Xendits::EXPIRATION_DATE    => Carbon::now()->addDays(1)->toISOString(),
            Xendits::IS_SINGLE_USE      => true,
        ];

        Xendit::setApiKey('xnd_development_JxUTuqsCKcgJFsK8EVPavJblC2AXWwBkpTcbETuttMf7IrU69950UdE4g5S2');
        $createVA = \Xendit\VirtualAccounts::create($credentials);

        return APIresponse(true, 'Data virtual account successfully submitted.', $createVA);
    }
}
