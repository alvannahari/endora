<?php

namespace App\Http\Controllers\Api\Customer\V1\Xendit\VA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Xendit\Xendit;

class GetListVA extends Controller {
    
    function __invoke() {
        Xendit::setApiKey('xnd_development_JxUTuqsCKcgJFsK8EVPavJblC2AXWwBkpTcbETuttMf7IrU69950UdE4g5S2');

        $listVa = \Xendit\VirtualAccounts::getVABanks();

        return APIresponse(true, 'List of virtual account succesfulley retrieved.', $listVa);
    }
}
