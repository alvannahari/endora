<?php

namespace App\Http\Controllers;

use App\Exports\Report\BalanceReport;
use App\Models\Admin;
use App\Models\HistoryAdmin;
use App\Models\Payment;
use App\Models\Transaction;
use App\Models\User;
use App\Models\VAPayments;
use App\Models\Xendits;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Xendit\Balance;
use Xendit\Invoice;
use Xendit\Payouts;
use Xendit\Xendit;

class BalanceController extends Controller {
    
    function index() {
        if (request()->ajax()) {
            $transaction = Transaction::where(Transaction::STATUS, '1')->with('payment')->get()->toArray();
            $va_bca = VAPayments::where(Transaction::STATUS, '1')->with('payment')->get()->toArray();
    
            // $payment = Payment::where(Payment::STATUS_PAYMENT, '1')->get()->toArray();
            // $payout = HistoryAdmin::get()->toArray();
            $balance = array_merge($transaction, $va_bca);
            usort($balance, function($a, $b) {
                return $a['updated_at'] <=> $b['updated_at'];
            });
            return response(['data' => $balance]);
        }
        $balance_week = (int) Transaction::where(Transaction::STATUS, '1')->whereBetween(Transaction::UPDATED_AT, [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
                        ->value(DB::raw("SUM(income - outcome)"));
        $balance['total'] = (int) Transaction::where(Transaction::STATUS, '1')->value(DB::raw("SUM(income - outcome)"));
        if ($balance['total'] - $balance_week == 0)  $balance['increase'] = 100;
        else $balance['increase'] = round($balance_week / ($balance['total'] - $balance_week) * 100, 1);
        $balance['bca-va'] = (int) Payment::where(Payment::STATUS_PAYMENT, '1')->where(Payment::TYPE, 'bca-va')->sum(Payment::TOTAL);
        $balance['xendit'] =  $balance['total'] - $balance['bca-va'];
        // dd($balance_week);
        // $amount_week = (int) Payment::where(Payment::STATUS_PAYMENT, '1')->whereBetween(Payment::UPDATED_AT, [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])->sum(Payment::TOTAL);
        // $amount['total'] = (int) Payment::where(Payment::STATUS_PAYMENT, '1')->sum(Payment::TOTAL);
        // $amount['increase'] = round($amount_week / ($amount['total'] - $amount_week) * 100, 1);
        // $amount['bca-va'] = (int) Payment::where(Payment::STATUS_PAYMENT, '1')->where(Payment::TYPE, 'bca-va')->sum(Payment::TOTAL);
        // $amount['xendit'] =  $amount['total'] - $amount['bca-va'];
        // dd($amount);

        $xendits = Xendit::setApiKey(config('services.xendit.key'));
        $xendit = Balance::getBalance('CASH');
        // $xendit['balance'] = 650000;
        $payout = HistoryAdmin::with('transaction','admin')->where(HistoryAdmin::ACTIVITY, 'payout xendit')->get()->toArray();
        $xendit['payout'] = 0;
        foreach ($payout as $key => $value) {
            $xendit['payout'] += $value['transaction']['outcome'];
        };
        // dd($xendit);

        return view('balance.index', compact('balance', 'xendit'));
    }

    function payout(Request $request) {
        $validator = Validator::make($request->all(), [
            HistoryAdmin::EMAIL => 'required|email',
            Transaction::OUTCOME => 'required',
            Admin::PASSWORD => 'required',
        ]);

        if ($validator->fails())
            return response(['error' => $validator->errors()]);

        $user = $request->user();

        if (!password_verify($request->input(Admin::PASSWORD), $user->password)) {
            return response(['error' => ['password' => 'password confirmation failed']]);
        }

        $outcome = $request->input(Transaction::OUTCOME);
        $email = $request->input(HistoryAdmin::EMAIL);

        $transaction = new Transaction();
        $id_transaction = $transaction->generateId();

        DB::beginTransaction();

        try {
            Xendit::setApiKey(config('services.xendit.key'));
            $payout = Payouts::create([
                Xendits::EXTERNAL_ID => $id_transaction,
                Xendits::AMOUNT => $outcome,
                HistoryAdmin::EMAIL => $email,
            ]);

            $tr = Transaction::create([
                Transaction::ID => $id_transaction,
                Transaction::PAYMENT_ID => null,
                Transaction::OUTCOME => $outcome,
                Transaction::STATUS => '1',
            ]);

            $tr->history()->create([
                HistoryAdmin::ADMIN_ID => $request->user()->id,
                HistoryAdmin::ACTIVITY => 'payout xendit',
                HistoryAdmin::EMAIL => $email,
            ]);
    
            DB::commit();
            return response([
                'error' => false,
                'data' => $payout
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response(['error' => $th->getMessage()]);
        }
    }

    function getPayout($id) {
        return response(['id' => $id]);
        // $xendit_id = HistoryAdmin::where(HistoryAdmin::EX)
        // Xendit::setApiKey(config('services.xendit.key'));
        // $data = Payouts::retrieve($id);
        // return response(['data' => $data]);
    }

    function exportReport() {
        $transaction = Transaction::where(Transaction::STATUS, '1')->with('payment.user', 'history.admin')->get()->toArray();
        foreach ($transaction as $key => $value) {
            if ($value['income'] != '0')  {
                $data[$key]['email'] = $value['payment']['user']['email'];
                $data[$key]['payment_id'] = $value['payment_id'];
                $data[$key]['income'] = $value['income'];
                $data[$key]['outcome'] = '-';
                $data[$key]['date'] = $value['updated_at'];
            } else {
                $data[$key]['email'] = $value['history']['admin']['email'];
                $data[$key]['payment_id'] = $value['id'];
                $data[$key]['income'] = '-';
                $data[$key]['outcome'] = $value['outcome'];
                $data[$key]['date'] = $value['updated_at'];
            }
            $data[$key]['type'] = 'Xendit';
        }

        $va_bca = VAPayments::where(Transaction::STATUS, '1')->with('payment.user')->get()->toArray();
        foreach ($va_bca as $key => $value) {
            $data[$key]['email'] = $value['payment']['user']['email'];
            $data[$key]['payment_id'] = $value['payment_id'];
            $data[$key]['income'] = $value['payment']['total'];
            $data[$key]['outcome'] = '-';
            $data[$key]['date'] = $value['updated_at'];
            $data[$key]['type'] = 'BCA-VA';
        }

        usort($data, function($a, $b) {
            return $a['date'] <=> $b['date'];
        });

        return Excel::download(new BalanceReport($data), 'report_balance_'.date('d-M-Y_H-i-s').'.xlsx');

        // return response(['data' => $data]);
    }
}
