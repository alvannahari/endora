<?php

namespace App\Http\Controllers;

use App\Models\Billboard;
use App\Models\Offer;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Brand;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class BillboardController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (request()->ajax()) {
            $data = Billboard::get()->toArray();
            return response(['data' => $data]);
        }
        return view('billboard.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $categories = Category::with(['subCategories.subCategories'])->doesnthave('categories')->get()->toArray();
        $brands = Brand::select(Brand::ID, Brand::NAME)->orderBy(Brand::NAME)->get()->toArray();
        $offers = Offer::orderBy(Offer::NAME)->get()->toArray();
        return view('billboard.create', compact('categories', 'brands', 'offers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            Billboard::NAME                 => 'required|string',
            Billboard::COVER_SMALL          => 'required|image|mimes:jpg,png,jpeg|max:1024|dimensions:max_width=365,max_height=58|dimensions:min_width=355,min_height=55',
            Billboard::COVER_BIG            => 'image|mimes:jpg,png,jpeg|max:1048|dimensions:max_width=360,max_height=360',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        $credentials = $request->only(Billboard::NAME, Billboard::IS_ACTIVE, Billboard::IS_IMPORTANT);

        $cover_small = Storage::disk('public')->put(Billboard::COVER_PATH, $request->file(Billboard::COVER_SMALL));
        $credentials[Billboard::COVER_SMALL] = basename($cover_small);

        if ($request->has(Billboard::COVER_BIG)) {
            $cover_big = Storage::disk('public')->put(Billboard::COVER_PATH, $request->file(Billboard::COVER_BIG));
            $credentials[Billboard::COVER_BIG] = basename($cover_big);
        }

        $billboard = Billboard::create($credentials);

        if ($request->has('offer')) $billboard->offers()->attach($request->input('offer'));
        if ($request->has('category')) $billboard->categories()->attach($request->input('category'));
        if ($request->has('brand')) $billboard->brands()->attach($request->input('brand'));
        
        return response(['error' => false]);
    }

    function state(Billboard $billboard) {
        $state = $billboard->is_active;
        if ($state == '1') 
            $billboard->update([Billboard::IS_ACTIVE => '0']);
        else 
            $billboard->update([Billboard::IS_ACTIVE => '1']);
        
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Billboard $billboard) {
        $billboard->delete();

        return redirect()->back();
    }
}
