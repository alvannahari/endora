<?php

namespace App\Http\Controllers;

use App\Exports\BrandExport;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;
use App\Imports\BrandImport;
use App\Models\Item;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;

class BrandController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (request()->ajax()) {
            $data = Brand::withCount('items')->get()->toArray();
            return response(['data' => $data]);
        }
        return view('brand.index');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            Brand::NAME             => 'required|string',
            Brand::DESCRIPTION      => 'required|string',
            Brand::IS_VALIDATE      => 'required',
            Brand::LOGO             => 'required|image|mimes:jpg,png,jpeg|max:4096|dimensions:max_width=640,max_height=640|dimensions:min_width=320,min_height=320',
            Brand::IMAGE            => 'required|image|mimes:jpg,png,jpeg|max:4096|dimensions:max_width=720,max_height=480|dimensions:min_width=480,min_height=240',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        $credentials = $request->only(Brand::NAME, Brand::DESCRIPTION, Brand::IS_VALIDATE);

        $logo = Storage::disk('public')->put(Brand::LOGO_PATH, $request->file(Brand::LOGO));
        $credentials[Brand::LOGO] = basename($logo);

        $image = Storage::disk('public')->put(Brand::IMAGE_PATH, $request->file(Brand::IMAGE));
        $credentials[Brand::IMAGE] = basename($image);

        Brand::create($credentials);

        return response(['error' => false]);
    }

    function import(Request $request) {
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        if ($validator->fails()) 
            return response(['error' => $validator->errors()]);

        $imageUrl = array();
        $spreadsheet = IOFactory::load($request->file('file'));
        $getDrawingCollection = $spreadsheet->getActiveSheet()->getDrawingCollection();

        $lengthRow = (int) $spreadsheet->getActiveSheet()->getHighestRow() - 1;

        DB::beginTransaction();

        // Check the length of the image array
        if ((count($getDrawingCollection)% 2) == 1 || (count($getDrawingCollection)/ 2) != $lengthRow) {
            DB::rollback();
            return response(['error' => 'some cells don`t have an image']);
        }
        foreach ( $getDrawingCollection as $drawing) {
            $pngUrl = time() . rand(1, 99999) . ".png";
            if ($drawing instanceof MemoryDrawing) {
                ob_start();
                call_user_func(
                    $drawing->getRenderingFunction(),
                    $drawing->getImageResource()
                );
                $imageContents = ob_get_contents();
                ob_end_clean();
            }
            else if ($drawing instanceof Drawing) {
                $zipReader = fopen($drawing->getPath(), 'r');
                $imageContents = '';
                while (! feof($zipReader)) {
                    $imageContents .= fread($zipReader, 1024);
                }
                fclose($zipReader);
            }
            $coordinate = $drawing->getCoordinates();
            if (substr($coordinate, 0, 1) == 'C') 
                Storage::disk('public')->put(Brand::LOGO_PATH.$pngUrl, $imageContents);
            else 
                Storage::disk('public')->put(Brand::IMAGE_PATH.$pngUrl, $imageContents);
            $imageUrl[$coordinate] = $pngUrl;
        }

        // Check there is an empty image cell
        // if ((count($imageUrl)% 2) == 1) {
        //     DB::rollback();
        //     return response(['error' => 'some cells don`t have an image']);
        // }
        $splitUrl = array_chunk($imageUrl, ceil(count($imageUrl) / 2), true);
        ksort($splitUrl[0]);
        ksort($splitUrl[1]);
        // if (count($splitUrl[0]) != count($splitUrl[1])) {
        //     DB::rollback();
        //     return response(['error' => 'Some rows contain empty cells']);
        // }
        // if (count($splitUrl[0]) != $lengthRow || count($splitUrl[1]) != $lengthRow) {
        //     DB::rollback();
        //     return response(['error' => 'some cells don`t have an image']);
        // }
        $partUrl[0] = array_values($splitUrl[0]);
        $partUrl[1] = array_values($splitUrl[1]);

        $import = new BrandImport($partUrl);
        Excel::import($import, $request->file('file'));

        DB::commit();
        return response(['error' => false]);
    }

    function export() {
        return Excel::download(new BrandExport, 'brand_endora_'.date('d-M-Y_H-i-s').'.xlsx');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand) {
        $brand = $brand->load('items.images')->loadCount('items')->toArray();
        // dd($brand);
        return view('brand.detail', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand) {
        $validator = Validator::make($request->all(), [
            Brand::NAME             => 'required|string',
            Brand::DESCRIPTION      => 'required|string',
            Brand::IS_VALIDATE      => 'required',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        $credentials = $request->only(Brand::NAME, Brand::DESCRIPTION, Brand::IS_VALIDATE);

        if ($brand->update($credentials))
            return response(['error' => false]);
            
        return response(['error' => 'Something Wrong !!!']);
    }

    public function updateLogo(Request $request, Brand $brand) {
        $validator = Validator::make($request->all(), [
            Brand::LOGO             => 'required|image|mimes:jpg,png,jpeg|max:8096',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        $arr_logo = explode("/",$brand->logo);
        Storage::disk('public')->delete(Brand::LOGO_PATH.end($arr_logo));

        $logo = Storage::disk('public')->put(Brand::LOGO_PATH, $request->file(Brand::LOGO));

        if ($brand->update([Brand::LOGO => basename($logo)]))
            return response(['error' => false]);
            
        return response(['error' => 'Something Wrong !!!']);
    }

    public function updateCover(Request $request, Brand $brand) {
        $validator = Validator::make($request->all(), [
            Brand::IMAGE             => 'required|image|mimes:jpg,png,jpeg|max:8096',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        $arr_image = explode("/",$brand->image);
        Storage::disk('public')->delete(Brand::IMAGE_PATH.end($arr_image));

        $image = Storage::disk('public')->put(Brand::IMAGE_PATH, $request->file(Brand::IMAGE));

        if ($brand->update([Brand::IMAGE => basename($image)]))
            return response(['error' => false]);
            
        return response(['error' => 'Something Wrong !!!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand) {
        $brand->delete();

        return redirect()->back();
    } 
}
