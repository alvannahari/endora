<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (request()->ajax()) {
            $categories = Category::with('categories')->withCount('subCategories')->get()->toArray();
            return response(['data' => $categories]);
        }
        $categories = Category::with(['subCategories'])->doesnthave('categories')->get()->toArray();
        return view('category.index', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCategory(Request $request) {
        $validator = Validator::make($request->all(), [
            Category::NAME          => 'required|string',
            Category::TYPE          => 'required|string',
            Category::IMAGE         => 'required|image|mimes:png,jpg,jpeg|max:8096',
            Category::DISCOUNT      => 'numeric',
            Category::MAXIMUM       => 'numeric',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        if ($request->input(Category::TYPE) == 'label') 
            $credentials = $request->only(Category::NAME, Category::TYPE);
        else 
            $credentials = $request->only(Category::NAME, Category::TYPE, Category::DISCOUNT, Category::MAXIMUM);

        $image = Storage::disk('public')->put(Category::IMAGE_PATH, $request->file(Category::IMAGE));
        $credentials[Category::IMAGE] = basename($image);

        Category::create($credentials);

        return response(['error' => false]);
    }

    public function storeSubCategory(Request $request) {
        $validator = Validator::make($request->all(), [
            'upper_category'         => 'required|numeric',
            Category::NAME          => 'required|string',
            Category::IMAGE         => 'required|image|mimes:png,jpg,jpeg|max:8096',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        $credentials = $request->only(Category::NAME);
        $credentials[Category::TYPE] = 'label';

        $image = Storage::disk('public')->put(Category::IMAGE_PATH, $request->file(Category::IMAGE));
        $credentials[Category::IMAGE] = basename($image);

        $category = Category::create($credentials);
        Category::find($request->input('upper_category'))->subCategories()->attach([$category->id]);

        return response(['error' => false]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category) {
        if ($category->delete()) 
            return response(['error' => false]);
        
        return response(['error' => 'Something Wrong!!!']);
    }
}
