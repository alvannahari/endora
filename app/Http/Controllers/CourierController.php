<?php

namespace App\Http\Controllers;

use App\Models\Courier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use App\Models\ShippingHistory;
use Illuminate\Support\Facades\Storage;

class CourierController extends Controller{    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (request()->ajax()) {
            $data = Courier::withCount('payment')->get()->toArray();
            return response(['data' => $data]);
        }
        return view('courier.index');
    }

    function getAllCouriers() {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            Courier::EMAIL      => 'required|email'
        ]); 

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        $credentials = $request->only(Courier::EMAIL, Courier::FULLNAME);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Courier $courier) {
        $user = $courier->load(['payment.histories' => function ($q) {
            $q->orderBy(ShippingHistory::CREATED_AT, 'desc')->with('status');
        }])->loadCount('payment')->toArray();
        // dd($user);
        return view('courier.detail', compact('user'));
    }

    public function update(Request $request, Courier $courier) {
        $validator = Validator::make($request->all(), [
            Courier::EMAIL              => 'required|string|email',
            Courier::FULLNAME           => 'required|string',
            Courier::GENDER             => 'required|string',
            Courier::NUMBER_PLATE       => 'required|string',
            Courier::DATE_BIRTH         => 'required',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        $credentials = $request->only(Courier::EMAIL, Courier::FULLNAME, Courier::GENDER, Courier::NUMBER_PLATE, Courier::DATE_BIRTH);

        if ($courier->update($credentials)) 
            return response(['error' => false]);

        return response(['error' => 'Something Wrong']);
    }

    function updateImage(Request $request, Courier $courier) {
        $validator = Validator::make($request->all(), [
            Courier::IMAGE     => 'required|image|mimes:png,jpg,jpeg|max:4096'
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        $arr_image = explode("/",$courier->image);
        if (end($arr_image) != 'default.png') 
            Storage::disk('public')->delete(Courier::IMAGE_PATH.end($arr_image));

        $image = Storage::disk('public')->put(Courier::IMAGE_PATH, $request->file(Courier::IMAGE));

        if ($courier->update([Courier::IMAGE => basename($image)]))
            return response(['error' => false]);

        return response(['error' => 'Something Wrong !!!']);
    }

    function changeState(Courier $courier) {
        $state = $courier->is_active;
        if ($state == 1) $update = $courier->update([Courier::IS_ACTIVE => '0']);
        else $update = $courier->update([Courier::IS_ACTIVE => '1']);

        if ($update) return response(['error' => false]);

        return response(['error' => 'Something Wrong!!!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Courier $courier){
        if($courier->delete()) 
            return response(['error' => false]);

        return response(['error' => 'Failed to delete this user courier']);
    }

    function destroyImage(Courier $courier) {
        $arr_image = explode("/",$courier->image);
        $image = end($arr_image);

        if ($image == 'default.png') 
            return response(['error' => 'image has been empty']);
        
        if($courier->update([Courier::IMAGE => null])) {
            Storage::disk('public')->delete(Courier::IMAGE_PATH.$image);
            return response(['error' => false]);
        }

        return response(['error' => 'Failed to delete this image photo']);
    }
}
