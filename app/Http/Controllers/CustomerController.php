<?php

namespace App\Http\Controllers;

use App\Models\ShippingHistory;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class CustomerController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (request()->ajax()) {
            $data = User::withCount('payments')->get()->toArray();
            return response(['data' => $data]);
        }
        return view('customer.index');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user) {
        $user = $user->load(['payments.histories' => function ($q) {
            $q->orderBy(ShippingHistory::CREATED_AT, 'desc')->with('status');
        }])->loadCount('payments')->toArray();
        // dd($user);
        return view('customer.detail', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user) {
        $validator = Validator::make($request->all(), [
            User::USERNAME          => 'required|string|regex:/^\S*$/u',
            User::EMAIL             => 'required|string|email',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        $credentials = $request->only(User::USERNAME, User::EMAIL);

        if ($user->update($credentials)) 
            return response(['error' => false]);

        return response(['error' => 'Something Wrong']);
    }

    function updateImage(Request $request, User $user) {
        $validator = Validator::make($request->all(), [
            User::IMAGE     => 'required|image|mimes:png,jpg,jpeg|max:4096'
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        $arr_image = explode("/",$user->image);
        if (end($arr_image) != 'default.png') 
            Storage::disk('public')->delete(User::IMAGE_PATH.end($arr_image));

        $image = Storage::disk('public')->put(User::IMAGE_PATH, $request->file(User::IMAGE));

        if ($user->update([User::IMAGE => basename($image)]))
            return response(['error' => false]);

        return response(['error' => 'Something Wrong !!!']);
    }

    function changeState(User $user) {
        $state = $user->is_active;
        if ($state == 1) $update = $user->update([User::IS_ACTIVE => '0']);
        else $update = $user->update([User::IS_ACTIVE => '1']);

        if ($update) return response(['error' => false]);

        return response(['error' => 'Something Wrong!!!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user) {
        if($user->delete())
            return response(['error' => false]);

        return response(['error' => 'Failed to delete this user customer']);
    }

    function destroyImage(User $user) {
        $arr_image = explode("/",$user->image);
        $image = end($arr_image);

        if ($image == 'default.png') 
            return response(['error' => 'image has been empty']);
        
        if($user->update([User::IMAGE => null])) {
            Storage::disk('public')->delete(User::IMAGE_PATH.$image);
            return response(['error' => false]);
        };

        return response(['error' => 'Failed to delete this inage photo']);
    }
}
