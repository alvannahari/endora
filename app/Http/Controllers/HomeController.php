<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Payment;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller {
    
    function index() {
        // get data graph order activity
        $result_activity = Payment::whereBetween(Payment::CREATED_AT, [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])->groupBy('date')->select(DB::raw('DATE_FORMAT(created_at, "%d") as date'), DB::raw('count(*) as total'))->get()->toArray();
        $activity = $this->_graphActivity($result_activity);
        
        // get data pie chart state payment
        $result_payment = Payment::groupBy(Payment::STATUS_PAYMENT)->orderBy(Payment::STATUS_PAYMENT)->select(DB::raw('count(id) as total'))->get()->toArray();
        $state_payment = $this->_getEmptyArray(3);
        foreach ($result_payment as $key => $item) {
            if (empty($state_payment[$key])) 
            $state_payment[$key] = $item['total'];
        }
        
        // get data pie state shipping order
        $result_shipment = Payment::where(Payment::STATUS_PAYMENT, '1')->get()->toArray();
        $arr_shipment = $this->_getEmptyArray(6);
        foreach ($result_shipment as $key => $item) {
            $arr_shipment[$item[Payment::STATUS_SHIPMENT]] ++;
        }
        $arr_shipment[1]+=$arr_shipment[0];
        unset($arr_shipment[0]);
        $state_shipment = array_values($arr_shipment);

        // get data count products or items
        $items['year'] = Order::whereYear(Order::CREATED_AT, date('Y'))->whereHas('payment', function($q) {
                            $q->where(Payment::STATUS_PAYMENT, '1');
                        })->count();
        $items['month'] = Order::whereYear(Order::CREATED_AT, date('Y'))->whereMonth(Order::CREATED_AT, date('m'))->whereHas('payment', function($q) {
                            $q->where(Payment::STATUS_PAYMENT, '1');
                        })->count();
        $items['week'] = Order::whereBetween(Order::CREATED_AT, [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])->whereHas('payment', function($q) {
                            $q->where(Payment::STATUS_PAYMENT, '1');
                        })->count();
        $items['today'] = Order::whereDate(Order::CREATED_AT, Carbon::today())->whereHas('payment', function($q) {
                            $q->where(Payment::STATUS_PAYMENT, '1');
                        })->count();

        // result data recent history order
        $histories = Payment::with(['user' => function ($q) {
            $q->withTrashed();
        }, 'histories.status'])->orderBy(Payment::CREATED_AT, 'desc')->limit(6)->get()->toArray();

        return view('home', compact('activity','state_payment','state_shipment','items','histories'));
    }

    function notifications() {
        return auth()->user()->unreadNotifications()->get()->toArray();
    }

    private function _graphActivity(array $activity) {
        $data['value'] = [];

        $data['label'] = [
            'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu','Minggu'
        ];

        for ($i=0; $i<7; $i++){
            $temp = false;
            $thisDate = Carbon::now()->startOfWeek()->addDay($i)->format('d');
            foreach($activity as $key => $value) {
                if($value['date'] == $thisDate){
                    $data['value'][$i] = $value['total'];
                    $temp = true;
                    break;
                };
            };
            if(!$temp) {
                $data['value'][$i] = 0;
            }
        };

        return $data;
    }

    private function _getEmptyArray($length) {
        for ($i=0; $i < $length; $i++) 
            $array[$i] = 0;
        return $array;
    }
}
