<?php

namespace App\Http\Controllers;

use App\Models\ShopDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class InfoController extends Controller {
    
    function index() {
        $data = ShopDetail::first()->toArray();
        // dd($data['name']);
        return view('info', compact('data'));
    }

    function update(Request $request) {
        $validator = Validator::make($request->all(), [
            ShopDetail::NAME        => 'required|string',
            ShopDetail::ADDRESS     => 'required|string',
            ShopDetail::PROVINCE    => 'required|string',
            ShopDetail::CITY        => 'required|string',
            ShopDetail::POSTAL_CODE => 'required|string',
            ShopDetail::LATITUDE    => 'required|numeric',
            ShopDetail::LONGITUDE   => 'required|numeric',
            ShopDetail::PHONE       => 'required|string',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        $credentials = $request->all();

        $data = ShopDetail::first();
        if ($data->update($credentials))
            return response(['error' => false]);

        return response(['error' => 'Something Wrong!!']);
    }

    function updateLogo(Request $request) {
        $validator = Validator::make($request->all(), [
            ShopDetail::LOGO    => 'required|image|mimes:png,jpg,jpeg|max:8096'
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        $data = ShopDetail::first();

        $arr_logo = explode("/",$data->logo);
        if (end($arr_logo) != 'default.png') 
            Storage::disk('public')->delete(ShopDetail::LOGO_PATH.end($arr_logo));

        $logo = Storage::disk('public')->put(ShopDetail::LOGO_PATH, $request->file(ShopDetail::LOGO));

        if ($data->update([ShopDetail::LOGO => basename($logo)]))
            return response(['error' => false]);

        return response(['error' => 'Something Wrong !!!']);
    }
}
