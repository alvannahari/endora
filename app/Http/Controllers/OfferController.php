<?php

namespace App\Http\Controllers;

use App\Models\Offer;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Brand;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class OfferController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (request()->ajax()) {
            $offer = Offer::with('billboard', 'categories', 'brands')->get()->toArray();
            return response(['data' => $offer]);
        }
        return view('offer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $categories = Category::with(['subCategories.subCategories'])->doesnthave('categories')->get()->toArray();
        $brands = Brand::select(Brand::ID, Brand::NAME)->orderBy(Brand::NAME)->get()->toArray();
        return view('offer.create', compact('categories', 'brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            Offer::NAME             => 'required|string',
            Offer::START_OFFER      => 'required',
            Offer::END_OFFER        => 'required',
            Offer::COVER            => 'required|image|mimes:jpg,png,jpeg|max:2048|dimensions:min_width=320,min_height=140',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        $credentials = $request->only(Offer::NAME, Offer::START_OFFER, Offer::END_OFFER, Offer::TYPE, Offer::IS_ACTIVE);

        $cover = Storage::disk('public')->put(Offer::COVER_PATH, $request->file(Offer::COVER));
        $credentials[Offer::COVER] = basename($cover);

        $offer = Offer::create($credentials);

        if ($request->has('category')) $offer->categories()->attach($request->input('category'));
        if ($request->has('brand')) $offer->brands()->attach($request->input('brand'));

        return response(['error' => false]);
    }

    function state (Offer $offer) {
        $state = $offer->is_active;
        if ($state == '1') $offer->update([Offer::IS_ACTIVE => '0']);
        else $offer->update([Offer::IS_ACTIVE => '1']);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $offer){
        $offer->delete();
        return redirect()->back();
    }
}
