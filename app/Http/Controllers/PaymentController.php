<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Payment;
use App\Notifications\Courier\NewShipping;
use App\Models\Courier;
use App\Models\ReasonCancel;
use App\Models\Shipment;
use App\Models\ShippingHistory;
use App\Models\ShopDetail;
use App\Notifications\Customer\NewStatusShipping;
use Illuminate\Support\Facades\Cache;
use Steevenz\Rajaongkir;
use App\Models\User;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Report\PaymentReport;

class PaymentController extends Controller {

    public function index() {
        if (request()->ajax()) {
            $result_paid_shipment = Payment::with(['user' => function ($q) {
                $q->withTrashed()->select(User::ID, User::EMAIL);
            }, 'shipmentable', 'transaction'])->where(Payment::STATUS_PAYMENT, '1')->where(Payment::COURIER_TYPE, 'shipment')->get();
    
            $payment_paid_shipment = Cache::remember('payment-paid-shipment', 300, function () use ($result_paid_shipment) {
                $result = [];
                foreach ($result_paid_shipment as $key => $value) {
                    if ($value[Payment::STATUS_SHIPMENT] != 0 && $value[Payment::STATUS_SHIPMENT] != 4) {
                        $rajaongkir = new Rajaongkir(config('services.rajaongkir.key'), config('services.rajaongkir.package'));
                        $waybill = $rajaongkir->getWaybill($value[Payment::WAYBILL_NUMBER], $value['shipmentable'][Shipment::CODE]);
        
                        if (!$waybill || count($waybill) < 1) {
                            $status = 1;
                        } else if ($waybill['summary']['status'] == 'ON PROCESS') {
                            $status = 2;
                        } else if ($waybill['summary']['status'] == 'DELIVERED') {
                            $status = 4;
                        };
                        if ($status != 1) {
                            $value->histories->first()->update([
                                ShippingHistory::STATUS => $status
                            ]);
                        }
                    }
                    $result[] = $value;
                }
                return $result;
            });
    
            $payment_other_shipment = Payment::with(['user' => function ($q) {
                $q->withTrashed()->select(User::ID, User::EMAIL);
            }, 'shipmentable', 'transaction'])->whereIn(Payment::STATUS_PAYMENT, ['0','97','98','99'])->where(Payment::COURIER_TYPE, 'shipment')->get()->toArray();
    
            $payment_courier = Payment::with(['user' => function ($q) {
                $q->withTrashed()->select(User::ID, User::EMAIL);
            }, 'transaction'])->where(Payment::COURIER_TYPE, 'courier')->get()->toArray();
    
            $all_payment = array_merge($payment_paid_shipment, $payment_other_shipment, $payment_courier);
    
            return response(['data' => $all_payment]);
        }

        return view('payment.index');
    }

    function synchronizePaymentShipment (Request $request) {
        Cache::forget('payment-paid-shipment');
        return redirect()->back();
    }

    function exportReport() {
        $payments = Payment::with(['user', 'histories.status', 'shipmentable', 'transaction', 'vaPayments.va', 'location' => function ($q) {
            $q->withTrashed();
        }])->withCount('order')->get()->toArray();
        // return $payments;

        $data = [];

        foreach ($payments as $key => $value) {
            $data[$key]['payment_id'] = $value[Payment::ID];
            $data[$key][User::USERNAME] = $value['user']['username'];
            $data[$key][User::EMAIL] = $value['user']['email'];
            $data[$key]['address'] = $value['location']['detail']['subdistrict_name'].' '.$value['location']['detail']['city'].' '.$value['location']['detail']['province'];
            $data[$key][Payment::TOTAL] = $value[Payment::TOTAL];
            $data[$key][Payment::TYPE] = $value[Payment::TYPE];
            $data[$key]['status'] = (string) $value[Payment::STATUS_PAYMENT];
            $data[$key][Payment::STATUS_SHIPMENT] = (string) $value[Payment::STATUS_SHIPMENT];
            
            if ($value['va_payments'] == null && $value['transaction'] == null) {
                $data[$key][Payment::STATUS_PAYMENT] = 'Not found';
                $data[$key][Payment::INVOICE_NUMBER] = 'Something wrong';
            }
            else if ($value[Payment::TYPE] == 'bca-va') {
                $data[$key][Payment::STATUS_PAYMENT] = (string) $value['va_payments'][0]['status'];
                $data[$key][Payment::INVOICE_NUMBER] = (float) $value['va_payments'][0]['va']['number'];
            } else {
                $data[$key][Payment::STATUS_PAYMENT] = (string) $value['transaction']['status'];
                $data[$key][Payment::INVOICE_NUMBER] = (string) $value[Payment::INVOICE_NUMBER];
            }

            if ($value[Payment::COURIER_ID] == '0') 
                $data[$key]['shipment'] = '-';
            else if ($value[Payment::COURIER_TYPE] == 'shipment') 
                $data[$key]['shipment'] = $value['shipmentable']['name'];
            else 
                $data[$key]['shipment'] = $value['shipmentable']['fullname'];

            $data[$key]['created_at'] = $value[Payment::CREATED_AT];
            $data[$key]['updated_at'] = $value[Payment::UPDATED_AT];
        }

        // return $data;

        if (count($data) > 0) 
            return Excel::download(new PaymentReport($data), 'report_payment_'.date('d-M-Y_H-i-s').'.xlsx');

        return redirect()->back();
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment) {
        $notif = auth()->user()->unreadNotifications()->where('data->payment->id', $payment->id)->get();
        if (!empty($notif)) 
            $notif->markAsRead();
        
        $payment = $payment->load(['user' => function ($q) {
            $q->withTrashed();
        },'location' => function ($q) {
            $q->withTrashed();
        },'shipmentable','histories.status', 'order.item' => function ($q) {
            $q->withTrashed()->with('images');
        }, 'review','cancel', 'transaction', 'vaPayments'])->toArray();

        if ($payment[Payment::COURIER_TYPE] == 'shipment') {
            $waybill = [];
            if ($payment[Payment::WAYBILL_NUMBER] != null) {
                $rajaongkir = new Rajaongkir(config('services.rajaongkir.key'), config('services.rajaongkir.package'));
                $waybill = $rajaongkir->getWaybill($payment[Payment::WAYBILL_NUMBER], $payment['shipmentable'][Shipment::CODE]);
            }
            $payment['histories'] = $waybill;
        }

        $couriers = Courier::get()->toArray();
        $shipments = Shipment::get()->toArray();
        // dd($payment);
        return view('payment.detail', compact('payment', 'couriers', 'shipments'));
    }

    function cancel(Payment $payment, Request $request) {
        $validator = Validator::make($request->all(), [
            ReasonCancel::REASON => 'required'
        ]);

        // return response(['data' => $request->toArray()]);

        if ($validator->fails())
            return response(['error' => $validator->errors()]);

        if ($payment->checkCancel()) 
            return redirect()->back();

        $payment->setCancelled($request, 'admin');

        return redirect()->back();
    }

    public function updatePayment(Request $request, Payment $payment) {
        $validator = Validator::make($request->all(), [
            Payment::STATUS_PAYMENT     => 'required',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        $credentials = $request->only(Payment::STATUS_PAYMENT);
        $credentials[Payment::WAYBILL_NUMBER] = mt_rand(1000000000, 9999999999);

        if ($payment->update($credentials))
            return response(['error' => false]);

        return response(['error', 'Something Wrong!!!']);
    }

    function updateShipment(Request $request, Payment $payment) {
        $validator = Validator::make($request->all(), [
            Payment::COURIER_ID         => 'required|numeric',
            Payment::COURIER_TYPE       => 'required|string',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }
    }

    function selectShipping (Payment $payment, Request $request) {
        $validator = Validator::make($request->all(), [
            Courier::ID     => 'required|numeric'
        ]);

        if ($validator->fails()) 
            return response(['error' => $validator->errors()]);

        if ($payment->status_payment == '98' || $payment->status_payment == '99') {
            return response(['error' => 'This payment has been cancelled.']);
        }

        if ($payment->histories()->exists()) {
            $payment->update([
                Payment::WAYBILL_NUMBER => $request->input(Payment::WAYBILL_NUMBER),
                Payment::COURIER_ID => $request->input(Courier::ID),
                ]);
            return response(['error' => false]);
        }

        $info = ShopDetail::first()->toArray();

        $credentials[ShippingHistory::PAYMENT_ID] = $payment->id;
        $credentials[ShippingHistory::STATUS] = '1';
        $credentials[ShippingHistory::LATITUDE] = $info[ShopDetail::LATITUDE];
        $credentials[ShippingHistory::LONGITUDE] = $info[ShopDetail::LONGITUDE];

        ShippingHistory::create($credentials);

        $payment->update([
            Payment::WAYBILL_NUMBER => $request->input(Payment::WAYBILL_NUMBER),
            Payment::COURIER_ID => $request->input(Courier::ID),
        ]);
        
        if ($payment->shipmentable_type == 'courier') {
            $user = $payment->user;
            $payment->load(['histories' => function ($q) {
                    $q->latest()->with('status');
                },'shipmentable','user']);
            $user->notify(new NewStatusShipping($payment));
            
            Courier::find($request->input(Courier::ID))->notify(new NewShipping($payment));
        }

        return response(['error' => false]);
    }

    function printInvoice(Request $request) {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
