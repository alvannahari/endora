<?php

namespace App\Http\Controllers;

use App\Exports\ProductExport;
use App\Imports\ProductImport;
use App\Models\Brand;
use App\Models\Category;
use App\Models\ImageItem;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class ProductController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function index() {
        if (request()->ajax()) {
            $products = Item::with(['brand:id,name','images'])->select(Item::ID, Item::BRAND_ID, Item::NAME, Item::PRICE, Item::INVENTORY, Item::CREATED_AT)->get()->toArray(); 
            return response(['data' => $products]);
        }
        return view('product.index');
    }
    
    function create() {
        $categories = Category::with(['subCategories.subCategories'])->doesnthave('categories')->get()->toArray();
        $brands = Brand::select(Brand::ID, Brand::NAME)->orderBy(Brand::NAME)->get()->toArray();
        // dd($categories);
        return view('product.create', compact('brands', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            Item::NAME              => 'required|string|max:255',
            Item::BRAND_ID          => 'required|numeric',
            Item::DESCRIPTION       => 'required|string',
            Item::PRICE             => 'required|numeric',
            Item::INVENTORY         => 'required|numeric',
            Item::VARIANT           => 'required|string',
            Item::WEIGHT            => 'required|numeric',
            'image'                 => 'required|array|min:2',
            'image.*'               => 'required|image|mimes:jpg,jpeg,png|max:2048|dimensions:max_width=320,max_height=240|dimensions:min_width=160,min_height=100',
            'category'              => 'required|array|min:1',
            'category.*'            => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $response = $validator->errors()->toArray();
            foreach ($response as $key => $value) {
                if (strpos($key, 'image.') !== false) 
                    $response['image['.substr($key, -1).']'] = $value;
            };
            return response(['error' => $response]);
            exit();
        }

        $credentials = $request->only(Item::NAME, Item::BRAND_ID, Item::DESCRIPTION, Item::PRICE, Item::INVENTORY, Item::VARIANT, Item::WEIGHT);

        $product = Item::create($credentials);

        $product->categories()->sync($request->input('category'));

        foreach ($request->image as $key => $image) {
            $new_image = Storage::disk('public')->put(ImageItem::IMAGE_PATH, $image);

            ImageItem::create([
                ImageItem::ITEM_ID  => $product->id,
                ImageItem::NAME     => basename($new_image)
            ]);
        }

        return response(['error', false]);
    }

    function import(Request $request) {
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        $imageUrl = array();
        $spreadsheet = IOFactory::load($request->file('file'));
        $getDrawingCollection = $spreadsheet->getActiveSheet()->getDrawingCollection();

        $lengthRow = (int) $spreadsheet->getActiveSheet()->getHighestRow() - 1;

        DB::beginTransaction();

        // Check the length of the image array
        if (count($getDrawingCollection) != $lengthRow) {
            DB::rollback();
            return response(['error' => 'some cells don`t have an image']);
        }

        foreach ( $getDrawingCollection as $drawing) {
            $pngUrl = time() . rand(1, 99999) . ".png";
            if ($drawing instanceof MemoryDrawing) {
                ob_start();
                call_user_func(
                    $drawing->getRenderingFunction(),
                    $drawing->getImageResource()
                );
                $imageContents = ob_get_contents();
                ob_end_clean();
            }
            else if ($drawing instanceof Drawing) {
                $zipReader = fopen($drawing->getPath(), 'r');
                $imageContents = '';
                while (! feof($zipReader)) {
                    $imageContents .= fread($zipReader, 1024);
                }
                fclose($zipReader);
            }
            $coordinate = $drawing->getCoordinates();
            Storage::disk('public')->put(ImageItem::IMAGE_PATH.$pngUrl, $imageContents);
            $imageUrl[$coordinate] = $pngUrl;
        }

        // if (count($imageUrl) != $lengthRow) {
        //     DB::rollback();
        //     return response(['error' => 'some cells don`t have an image']);
        // }
        $imageName = array_values($imageUrl);

        $import = new ProductImport($imageName);
        Excel::import($import, $request->file('file'));
        // Excel::import(new ProductImport, $request->file('file'));

        DB::commit();
        return redirect()->back();
    }

    function export() {
        return Excel::download(new ProductExport, 'product_endora_'.date('d-M-Y_H-i-s').'.xlsx');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item) {
        $product = $item->load('brand','images','categories.categories.categories')->toArray();
        // dd($product);
        return view('product.detail', compact('product'));
    }

    function storeImage(Request $request,Item $item) {
        $validator = Validator::make($request->all(),[
            'order'             => 'required|numeric',
            ImageItem::NAME     => 'required|image|mimes:jpg,jpeg,png|max:2048|dimensions:max_width=320,max_height=240|dimensions:min_width=160,min_height=100'
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        // $order = $request->input('order');
        $image = Storage::disk('public')->put(ImageItem::IMAGE_PATH, $request->file(ImageItem::NAME));

        // $destinationPath = public_path(UPLOAD_IMAGE_ITEM);
        // $filename = $item->id.'_item_O'.$order.'_'.rand(11111, 99999).'.'.$image->getClientOriginalExtension();
        // $image->move($destinationPath, $filename);

        ImageItem::create([
            ImageItem::ITEM_ID  => $item->id,
            ImageItem::NAME     => basename($image),
        ]);

        return response(['error' => false]);
    }

    function edit(Item $item) {
        $items = Category::with(['subCategories.subCategories'])->doesnthave('categories')->get()->toArray();
        $brands = Brand::select(Brand::ID, Brand::NAME)->get()->toArray();
        $product = $item->load('brand','images','categories.categories.categories')->toArray();
        // dd($categories);
        return view('product.update', compact('brands', 'items','product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item) {
        $validator = Validator::make($request->all(), [
            Item::NAME              => 'required|string|max:255',
            Item::BRAND_ID          => 'required|numeric',
            Item::DESCRIPTION       => 'required|string',
            Item::PRICE             => 'required|numeric',
            Item::INVENTORY         => 'required|numeric',
            Item::VARIANT           => 'required|string',
            Item::WEIGHT            => 'required|numeric',
            'category'              => 'required|array|min:1',
            'category.*'            => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        $credentials = $request->only(Item::NAME, Item::BRAND_ID, Item::DESCRIPTION, Item::PRICE, Item::INVENTORY, Item::VARIANT, Item::WEIGHT);

        if (!$item->update($credentials)) return response(['error' => 'Something Wrong !!!']);

        $item->categories()->sync($request->input('category'));

        return response(['error' => false]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item) {
        $item->delete();
        return redirect()->back();
    }

    function destroyImage(ImageItem $imageItem) {
        if ($imageItem->delete()) 
            return response(['error' => false]);

        return response(['error' => 'Something Wrong !!']);
    }
}
