<?php

namespace App\Http\Controllers;

use App\Models\Shipment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Steevenz\Rajaongkir;

class ShipmentController extends Controller {
    
    function index() {
        // $expedition = Cache::remember('courier-express-list', 300, function () {
        //     $rajaongkir = new Rajaongkir(config('services.rajaongkir.key'), config('services.rajaongkir.package'));
        //     return $rajaongkir->getSupportedCouriers();
        // });
        // foreach ($expedition as $key => $value) {
            
        // }
        // $shipment = 
        if (request()->ajax()) {
            $data = Shipment::get()->toArray();
            return response(['data' => $data]);
        }
        return view('shipment.index');
    }

    function update(Request $request) {
        $shipments = Shipment::get();
        $result = $request->toArray();

        // return response(['data' => $result]);

        foreach ($shipments as $key => $value) {
            if ($value->is_active == '0' && in_array($value->id, $result['shipment']) )  {
                $value->update([
                    Shipment::IS_ACTIVE => '1'
                ]);
            } else if ($value->is_active == '1' && !in_array($value->id, $result['shipment']) ) {
                $value->update([
                    Shipment::IS_ACTIVE => '0'
                ]);
            }
        }

        return redirect()->back();
    }
}
