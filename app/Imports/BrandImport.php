<?php

namespace App\Imports;

use App\Models\Brand;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;

class BrandImport implements ToCollection, WithHeadingRow, WithChunkReading, ShouldQueue {

    protected $imageUrl;

    public function __construct($imageUrl) {
        $this->imageUrl = $imageUrl;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows) {
        foreach ($rows as $key => $row) {
            Brand::create([
                Brand::NAME => $row[Brand::NAME],
                Brand::DESCRIPTION => $row[Brand::DESCRIPTION],
                Brand::LOGO => $this->imageUrl[0][$key],
                Brand::IMAGE => $this->imageUrl[1][$key],
                Brand::IS_VALIDATE => (string) $row[Brand::IS_VALIDATE],
            ]);
        }
    }

    public function chunkSize(): int {
        return 1000;
    }
}
