<?php

namespace App\Imports;

use App\Models\Brand;
use App\Models\ImageItem;
use App\Models\Item;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\ToCollection;

class ProductImport implements ToCollection, WithHeadingRow, WithChunkReading, ShouldQueue {

    protected $imageUrl;

    public function __construct($imageUrl) {
        $this->imageUrl = $imageUrl;
    }

    public function collection(Collection $rows) {
        // return new Item([
        //     Item::NAME          => $row[Item::NAME],
        //     Item::BRAND_ID      => Brand::where(Brand::NAME, $row['brand'])->first()->id ?? '5',
        //     Item::DESCRIPTION   => $row[Item::DESCRIPTION],
        //     Item::PRICE         => $row[Item::PRICE],
        //     Item::VARIANT       => $row[Item::VARIANT],
        //     Item::INVENTORY     => $row[Item::INVENTORY],
        //     Item::WEIGHT        => $row[Item::WEIGHT]
        // ]);
        foreach ($rows as $key => $row) {
            $item = Item::create([
                Item::NAME          => $row[Item::NAME],
                Item::BRAND_ID      => Brand::where(Brand::NAME, $row['brand'])->first()->id ?? '5',
                Item::DESCRIPTION   => $row[Item::DESCRIPTION],
                Item::PRICE         => $row[Item::PRICE],
                Item::VARIANT       => $row[Item::VARIANT],
                Item::INVENTORY     => $row[Item::INVENTORY],
                Item::WEIGHT        => $row[Item::WEIGHT],
            ]);

            $item->images()->create([
                ImageItem::NAME => $this->imageUrl[$key]
            ]);
        }
    }

    public function chunkSize(): int {
        return 1000;
    }
}
