<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable {

    use HasFactory, Notifiable;

    const ID = 'id';
    const EMAIL = 'email';
    const EMAIL_VERIFIED = 'email_verified_at';
    const PASSWORD = 'password';
    const IS_ACTIVE = 'is_active';

    protected $guarded = [];

    protected $hidden = [Self::PASSWORD];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function history() {
        return $this->hasMany('App\Models\HistoryAdmin');
    }

    public function receivesBroadcastNotificationsOn() {
        return 'App.Admin.'.$this->id;
    }
}
