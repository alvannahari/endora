<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BCACredential extends Model {
    use HasFactory;

    protected $table = "bca_creds";
}
