<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Billboard extends Model {

    use HasFactory;

    const COVER_PATH = 'billboard/';

    const ID = 'id';
    const NAME = 'name';
    const COVER_SMALL = 'cover_small';
    const COVER_BIG = 'cover_big';
    const IS_IMPORTANT = 'is_important';
    const IS_ACTIVE = 'is_active';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    // public static function boot()
    // {
    //     static::all(function ($model) {
    //         $model->timestamps = false;
    //         $model->created_at = now();
    //     });

    //     parent::boot();

    //     static::creating(function ($model) {
    //         $model->timestamps = false;
    //         $model->created_at = now();
    //     });
    // }

    function delete() {
        Storage::disk('public')->delete(Self::COVER_PATH.$this->attributes[Self::COVER_SMALL]);

        if ($this->attributes[SELF::COVER_BIG] != null) 
            Storage::disk('public')->delete(Self::COVER_PATH.$this->attributes[Self::COVER_BIG]);

        return parent::delete();
    }

    public function getCoverSmallAttribute($value) {
        return asset('storage/'.Self::COVER_PATH.$value);
        // return Storage::url(Self::COVER_PATH.$value);
    }

    public function getCoverBigAttribute($value) {
        if ($this->attributes[SELF::COVER_BIG] == null) return asset('storage/'.Self::COVER_PATH.'not_found.png');
        else return asset('storage/'.Self::COVER_PATH.$value);
        // if ($this->attributes[SELF::COVER_BIG] == null) return Storage::url(Self::COVER_PATH.'not_found.png');
        // else return Storage::url(Self::COVER_PATH.$value);
    }

    public function offers() {
        return $this->morphedByMany('App\Models\Offer', 'contentable');
    }

    public function categories() {
        return $this->morphedByMany('App\Models\Category', 'contentable');
    }

    public function brands() {
        return $this->morphedByMany('App\Models\Brand', 'contentable');
    }
}
