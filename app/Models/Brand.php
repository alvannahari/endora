<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Brand extends Model {

    use HasFactory;

    const IMAGE_PATH = 'brand/';
    const LOGO_PATH = 'brand/logo/';

    const ID = 'id';
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const LOGO = 'logo';
    const IMAGE = 'image';
    const IS_VALIDATE = 'is_validate';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function delete() {
        Storage::disk('public')->delete(Self::LOGO_PATH.$this->attributes[Self::LOGO]);
        
        if ($this->attributes[SELF::IMAGE] != null) 
            Storage::disk('public')->delete(Self::IMAGE_PATH.$this->attributes[Self::IMAGE]);
        
        return parent::delete();
    }

    public function getLogoAttribute($value) {
        return asset('storage/'.Self::LOGO_PATH.$value);
        // return Storage::url(Self::LOGO_PATH.$value);
    }

    public function getImageAttribute($value) {
        if ($this->attributes[SELF::IMAGE] == null) return asset('storage/'.Self::IMAGE_PATH.'not_found.png');
        else return asset('storage/'.Self::IMAGE_PATH.$value);
    }

    function items() {
        return $this->hasMany('App\Models\Item');
    }

    function billboard() {
        return $this->morphToMany('App\Models\Billboard', 'contentable');
    }

    function offer() {
        return $this->morphToMany('App\Models\Offer', 'offerable');
    }
}
