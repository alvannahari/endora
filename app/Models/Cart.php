<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model {

    use HasFactory;

    const ID = 'id';
    const USER_ID = 'user_id';
    const ITEM_ID = 'item_id';
    const VARIANT = 'variant';
    const QTY = 'qty';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    protected $hidden = [SELF::CREATED_AT, SELF::UPDATED_AT];

    function user() {
        return $this->belongsTo('App\Models\User');
    }

    function item() {
        return $this->belongsTo('App\Models\Item');
    }

    function checkItem($id, $variant) {
        if ($this->find($id)->where(Self::VARIANT, $variant)) {
            return true;
        }

        return false;
    }
}
