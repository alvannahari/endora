<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Category extends Model {

    use HasFactory;

    const IMAGE_PATH = 'category/';

    const ID = 'id';
    const NAME = 'name';
    const TYPE = 'type';
    const IMAGE = 'image';
    const DISCOUNT = 'discount';
    const MAXIMUM = 'maximum';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    protected $hidden = [SELF::UPDATED_AT];

    // function labels() {
    //     return $this->hasMany('App\Models\Label');
    // }

    function delete() {
        Storage::disk('public')->delete(Self::IMAGE_PATH.$this->attributes[Self::IMAGE]);

        $this->subCategories()->delete();
        return parent::delete();
    }

    function getImageAttribute($value) {
        if ($value == null && auth()->guard()->check()) return asset('storage/'.Self::IMAGE_PATH.'/not_found.png');
        else if ($value == null) return null;
        else return asset('storage/'.Self::IMAGE_PATH.$value);
        // if ($value == null && auth()->guard()->check()) return Storage::url(Self::IMAGE_PATH.'/not_found.png');
        // else if ($value == null) return null;
        // else return Storage::url(Self::IMAGE_PATH.$value);
    }
    
    function subCategories() {
        return $this->belongsToMany(self::class, 'sub_categories', 'category_id', 'sub_category_id');
    }

    function categories() {
        return $this->belongsToMany(self::class, 'sub_categories', 'sub_category_id', 'category_id');
    }

    function items() {
        return $this->belongsToMany('App\Models\Item', 'tags', 'category_id', 'item_id');
    }

    function billboard() {
        return $this->morphToMany('App\Models\Billboard', 'contentable');
    }

    function offer() {
        return $this->morphToMany('App\Models\Offer', 'offerable');
    }
}
