<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contentable extends Model { 
    
    const ID = 'id';
    const BILLBOARD_ID = 'billboard_id';
    const CONTENT_ID = 'contentable_id';
    const CONTENT_TYPE = 'contentable_type';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    protected $hidden = [SELF::CREATED_AT];
}
