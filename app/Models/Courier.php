<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

class Courier extends Authenticatable {

    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    const IMAGE_PATH = 'profile/courier/';

    const ID = 'id';
    const EMAIL = 'email';
    const FULLNAME = 'fullname';
    const GENDER = 'gender';
    const PASSWORD = 'password';
    const NUMBER_PLATE = 'number_plate';
    const DATE_BIRTH = 'date_birth';
    const IMAGE = 'image';
    const TOKEN_FCM = 'token_fcm';
    const DEVICE_ID = 'device_id';
    const IS_ACTIVE = 'is_active';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    protected $hidden = [SELF::PASSWORD];

    function forceDelete() {
        if ($this->attributes[SELF::IMAGE] != null) 
            Storage::disk('public')->delete(Self::IMAGE_PATH.$this->attributes[Self::IMAGE]);
            
        return parent::forceDelete();
    }

    public function getImageAttribute($value) {
        if ($this->attributes[SELF::IMAGE] == null) return asset('storage/'.Self::IMAGE_PATH.'default.png');
        else return asset('storage/'.Self::IMAGE_PATH.$value);
        // if ($this->attributes[SELF::IMAGE] == null) return Storage::url(Self::IMAGE_PATH.'default.png');
        // else return Storage::url(Self::IMAGE_PATH.$value);
    }

    function payment() {
        return $this->morphMany('App\Models\Payment', 'shipmentable');
    }

    public function routeNotificationForFcm($notification) {
        return $this->token_fcm;
    }
}
