<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Favourite extends Model {

    use HasFactory;

    const ID = 'id';
    const USER_ID = 'user_id';
    const ITEM_ID = 'item_id';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    protected $hidden = [SELF::CREATED_AT];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->timestamps = false;
            $model->created_at = now();
        });
    }

    function user() {
        return $this->belongsTo('App\Models\User');
    }

    function item() {
        return $this->belongsTo('App\Models\Item');
    }
}
