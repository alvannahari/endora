<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryAdmin extends Model {

    use HasFactory;

    const ID = 'id';
    const ADMIN_ID = 'admin_id';
    const ACTIVITY = 'activity';
    const EMAIL = 'email';
    const TRANSACTION_ID = 'transaction_id';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    public $incrementing = false;

    function admin() {
        return $this->belongsTo('App\Models\Admin');
    }

    function transaction() {
        return $this->belongsTo(Transaction::class);
    }

    public function setUpdatedAt($value) {
        // Do nothing.
    }
}
