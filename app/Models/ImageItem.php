<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ImageItem extends Model {
    
    const IMAGE_PATH = 'item/';

    const ID = 'id';
    const ITEM_ID = 'item_id';
    const NAME = 'name';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    protected $hidden = [SELF::CREATED_AT, SELF::UPDATED_AT];

    function delete() {
        Storage::disk('public')->delete(Self::IMAGE_PATH.$this->attributes[Self::NAME]);
        
        return parent::delete();
    }

    public function getNameAttribute($value) {
        if ($this->attributes[SELF::NAME] == null) return asset('storage/'.Self::IMAGE_PATH.'not_found.png');
        else return asset('storage/'.Self::IMAGE_PATH.$value);
        // if ($this->attributes[SELF::NAME] == null) return Storage::url(Self::IMAGE_PATH.'not_found.png');
        // else return Storage::url(Self::IMAGE_PATH.$value);
    }

    function item() {
        return $this->belongsTo('App\Models\Item');
    }
}
