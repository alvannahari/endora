<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model {

    use HasFactory, SoftDeletes;

    const ID = 'id';
    const BRAND_ID = 'brand_id';
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const PRICE = 'price';
    const VARIANT = 'variant';
    const INVENTORY = 'inventory';
    const WEIGHT = 'weight';

    const IS_FAVORITE = 'is_favorite';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s',
    ];

    protected $fillable = [
        'brand_id',
        'name',
        'description',
        'price',
        'variant',
        'inventory',
        'weight',
    ];

    protected $appends = [Self::IS_FAVORITE];

    function delete() {
        $this->favourites()->delete();
        $this->carts()->delete();
        
        return parent::delete();
    }
    
    function forceDelete() {
        $this->favourites()->delete();
        $this->carts()->delete();
        $this->images()->delete();
        $this->categories()->detach();

        return parent::forceDelete();
    }

    function favourites() {
        return $this->hasMany('App\Models\Favourite');
    }

    function carts() {
        return $this->hasMany('App\Models\Cart');
    }

    function orders() {
        return $this->hasMany('App\Models\Order');
    }

    function categories() {
        return $this->belongsToMany('App\Models\Category', 'tags', 'item_id', 'category_id');
    }

    function promo() {
        return $this->categories()->where('type', 'promo');
    }

    function brand() {
        return $this->belongsTo('App\Models\Brand');
    }

    function images() {
        return $this->hasMany('App\Models\ImageItem');
    }

    function stockReduction($amount) {
        $inventory = (int) $this->inventory - (int) $amount;
        $this->update([Self::INVENTORY => $inventory]);
        return $this;
    }
    
    function stockAdditional($amount) {
        $inventory = (int) $this->inventory + (int) $amount;
        $this->update([Self::INVENTORY => $inventory]);
        return $this;
    }

    public function getIsFavoriteAttribute() {
        if (auth()->guard('api-customer')->check()) {
            $user_id = auth()->guard('api-customer')->user()->id;
            return (boolean) $this->favourites()->where('user_id', $user_id)->first(['favourites.'.SELF::ID]);
        }
        return false;
    }

    function getVariantAttribute() {
        if (auth()->guard('api-customer')->check()) {
            $variant = explode(',', $this->attributes[SELF::VARIANT]);
            if ($variant[0] == null) {
                return null;
            }
            return $variant;
        }
        return $this->attributes[SELF::VARIANT];
    } 
}
