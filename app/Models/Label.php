<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Label extends Model {

    use HasFactory;

    const ID = 'id';
    const CATEGORY_ID = 'category_id';
    const NAME = 'name';
    const DISCOUNT = 'discount';
    const MAXIMUM = 'maximum';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function items() {
        return $this->belongsToMany('App\Model\Item', 'tags', 'label_id', 'item_id');
    }

    function category() {
        $this->belongsTo('App\Models\Label');
    }
}
