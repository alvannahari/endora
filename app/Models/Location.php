<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Steevenz\Rajaongkir;

class Location extends Model {

    use HasFactory, SoftDeletes;

    const ID = 'id';
    const USER_ID = 'user_id';
    const CATEGORY = 'category';
    const RECEIVER = 'receiver_name';
    const PHONE = 'phone';
    const ADDRESS = 'address';
    const SUBDISTRICT_ID = 'subdistrict_id';
    const POSTAL_CODE = 'postal_code';
    const DESCRIPTION = 'description';
    const LATITUDE = 'latitude';
    const LONGITUDE = 'longitude';
    const IS_MAIN = 'is_main';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    protected $hidden = [SELF::CREATED_AT, SELF::UPDATED_AT];

    protected $appends = ['detail'];

    function user () {
        return $this->belongsTo('App\Models\User');
    }

    function getDetailAttribute() {
        $subdistrict_id = $this->subdistrict_id;
        $subdistrict = Cache::remember('subdistrict-'.$subdistrict_id, 900, function () use ($subdistrict_id) {
            $rajaongkir = new Rajaongkir(config('services.rajaongkir.key'), config('services.rajaongkir.package'));
            return $rajaongkir->getSubdistrict($subdistrict_id);
        });
        return $subdistrict;
    }
}
