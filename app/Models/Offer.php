<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Offer extends Model {

    use HasFactory;

    const COVER_PATH = 'offer/';

    const ID = 'id';
    const NAME = 'name';
    const COVER = 'cover';
    const TYPE = 'type';
    const START_OFFER = 'start_offer';
    const END_OFFER = 'end_offer';
    const IS_ACTIVE = 'is_active';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function delete() {
        Storage::disk('public')->delete(Self::COVER_PATH.$this->attributes[Self::COVER]);

        return parent::delete();
    }

    public function getCoverAttribute($value) {
        return asset('storage/'.Self::COVER_PATH.$value);
        // return Storage::url(Self::COVER_PATH.$value);
    }

    function billboard() {
        return $this->morphToMany('App\Models\Billboard', 'contentable');
    }

    public function categories() {
        return $this->morphedByMany('App\Models\Category', 'offerable');
    }

    public function brands() {
        return $this->morphedByMany('App\Models\Brand', 'offerable');
    }
}
