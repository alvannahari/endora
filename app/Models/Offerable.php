<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Offerable extends Model {
    
    const ID = 'id';
    const OFFER_ID = 'offer_id';
    const OFFERABLE_ID = 'offerable_id';
    const OFFERABLE_TYPE = 'offerable_type';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    protected $hidden = [SELF::CREATED_AT];
}
