<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    use HasFactory;

    const ID = 'id';
    const PAYMENT_ID = 'payment_id';
    const ITEM_ID = 'item_id';
    const NAME = 'name';
    const PRICE = 'price';
    const QTY = 'qty';
    const VARIANT = 'variant';
    const T_WEIGHT = 't_weight';
    const DISCOUNT = 'discount';
    const TOTAL = 'total';
    const NOTE = 'note';

    // this for mass assignment to 
    protected $guarded = [];

    // for mass assignment
    protected $fillable = [
        'payment_id',
        'item_id',
        'name',
        'price',
        'qty',
        'variant',
        't_weight',
        'discount',
        'total',
        'note',
    ];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    protected $hidden = [SELF::CREATED_AT, SELF::UPDATED_AT];

    function payment() {
        return $this->belongsTo('App\Models\Payment');
    }

    function item() {
        return $this->belongsTo('App\Models\Item');
    }
}
