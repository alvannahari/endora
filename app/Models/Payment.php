<?php

namespace App\Models;

use App\Notifications\Admin\InvoicePaid;
use App\Notifications\Customer\NewStatusPayment;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class Payment extends Model {

    use HasFactory;

    const ID = 'id';
    const USER_ID = 'user_id';
    const LOCATION_ID = 'location_id';
    const COURIER_ID = 'shipmentable_id';
    const COURIER_TYPE = 'shipmentable_type';
    const TYPE = 'type';
    const TYPE_UID = 'type_uid';
    const INVOICE_NUMBER = 'invoice_number';
    const WAYBILL_NUMBER = 'waybill_number';
    const DESCRIPTION = 'description';
    const TOTAL_SHIPMENT = 'total_shipment';
    const TOTAL = 'total';
    const STATUS_PAYMENT = 'status_payment';
    const EXPIRE_DATE = 'expire_date';
    
    const STATUS_PENDING = '0';
    const STATUS_PAID = '1';
    const STATUS_CANCELED = '99';

    const STATUS_SHIPMENT = 'status_shipment';

    protected $guarded = [];

    public $incrementing = false;

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    protected $appends = [Self::STATUS_SHIPMENT];

    protected function getStatusShipmentAttribute() {
        return $this->histories()->orderBy(Self::CREATED_AT, 'desc')->first()->status_id ?? 0;
    }
    
    function histories() {
        return $this->hasMany('App\Models\ShippingHistory');
    }

    function order() {
        return $this->hasMany('App\Models\Order');
    }

    function review() {
        return $this->hasOne('App\Models\Review');
    }

    function user() {
        return $this->belongsTo('App\Models\User');
    }

    function location() {
        return $this->belongsTo('App\Models\Location');
    }

    public function cancel() {
        return $this->hasOne('App\Models\ReasonCancel');
    }

    public function submission() {
        return $this->hasOne('App\Models\Submission');
    }

    function transaction() {
        return $this->hasOne(Transaction::class);
    }

    function shipmentable() {
        if ($this->shipmentable_type == 'courier')
            return $this->morphTo()->withTrashed();

        return $this->morphTo();
    }

    function generatePaymentId() {
        $id = 'ENDORA-'.substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,12);

        if ($this->paymentIdExists($id)) return $this->generatePaymentId();

        return $id;
    }

    function paymentIdExists($id) {
        return $this->where(Self::ID, $id)->exists();
    }

    /**
     * 
     * @return QueryBuilder
     * with pre defined where type = bca-va, 
     * chainable w/ eloquent methods
     * 
     */
    function bca() {
        return $this->where(Self::TYPE, "bca-va");
    }
    function whereBca() {
        return $this->where("type", "bca-va");
    }

    function bcaPending() {
        return $this->whereBca()->where("status", "0");
    }

    public function vaPayments(){
        return $this->hasMany(VAPayments::class,"payment_id");
    }

    public function setPaid(){
        $this->status_payment = "1";
        $user = $this->user;

        try{
            $user->notify(new NewStatusPayment($this));
            Admin::find(1)->notify(new InvoicePaid($user, $this));
        }catch(\Exception $e){
            Log::debug('MyNotification failed');
        }

        return $this;
    }

    function checkCancel() {
        $status_shipment = $this->status_shipment;
        if ($status_shipment == '2' || $status_shipment == '3' || $status_shipment == '4' || $status_shipment == '5') 
            return true;

        $status = $this->status_payment;
        if ($status == '99') 
            return true;
        return false;
    }

    function setCancelled(Request $request, $applicant) {
        if ($applicant == 'customer') {
            if ($this->status_payment == '0') {
                $this->stockReturns();
                $this->update([Self::STATUS_PAYMENT => '99']);
                $this->cancel()->create([
                    ReasonCancel::REASON => $request->input(ReasonCancel::REASON),
                    ReasonCancel::CANCEL_BY => 'customer',
                    ReasonCancel::REFUND => null,
                ]);
            } else {
                $this->update([Self::STATUS_PAYMENT => '98']);
                $this->cancel()->create([
                    ReasonCancel::REASON => $request->input(ReasonCancel::REASON),
                    ReasonCancel::CANCEL_BY => 'customer',
                    ReasonCancel::REFUND => null,
                ]);
            }
        } else {
            $this->stockReturns();
            if ($this->status_payment == '98') {
                $this->update([Self::STATUS_PAYMENT => '99']);
                $this->cancel()->update([
                    ReasonCancel::REFUND => $request->input(ReasonCancel::REFUND),
                ]);
            } else {
                $this->update([Self::STATUS_PAYMENT => '99']);
                $this->cancel()->create([
                    ReasonCancel::REASON => $request->input(ReasonCancel::REASON),
                    ReasonCancel::CANCEL_BY => 'admin',
                    ReasonCancel::REFUND => $request->input(ReasonCancel::REFUND),
                ]);
            }
        }

        return $this;
    }

    function setSubmission() {
        $this->update([
            Self::STATUS_PAYMENT => (string) '97'
        ]);

        return $this;
    }

    function stockReturns() {
        $orders = $this->order;
        foreach ($orders as $order) {
            $qty = $order->qty;
            $stock = (int) $order->item->inventory + (int) $qty;
            $order->item()->update([
                Item::INVENTORY => $stock
            ]);
        }
        return $this;
    }

    function now(){
        return Carbon::now();
    }

    function isExpired(){
        return $this->expire_date <= $this->now();
    }
}
