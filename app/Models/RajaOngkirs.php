<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RajaOngkirs extends Model {

    const ID = 'id';
    const ORIGIN = 'origin';
    const DESTINATION = 'destination';
    const WEIGHT = 'weight';
    const COURIER = 'courier';

}
