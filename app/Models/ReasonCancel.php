<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReasonCancel extends Model {
    use HasFactory;

    const ID = 'id';
    const PAYMENT_ID = 'payment_id';
    const REFUND = 'refund';
    const CANCEL_BY = 'cancel_by';
    const REASON = 'reason';

    protected $guarded = [];

    // protected $append = [Self::CANCEL_BY];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];
    
    public function payment() {
        return $this->belongsTo('App\Models\Payment');
    }

    // function getCancelByAttributes() {
    //     $code = $this->payment->status_payment;

    //     if ($code == '98')
    //         return 'Customer';
    //     else  if ($code == '99')
    //         return 'Admin';
    //     else 
    //         return 'Belum Dibayar';
    // }
}
