<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model {

    use HasFactory;

    const ID = 'id';
    const PAYMENT_ID = 'payment_id';
    const RATING = 'rating';
    const REVIEW = 'review';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function payment() {
        return $this->belongsTo('App\Models\Payment');
    }
    
}
