<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SearchHistory extends Model {
    
    use HasFactory;

    const ID = 'id';
    const USER_ID = 'user_id';
    const SEARCH = 'search';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function user() {
        return $this->belongsTo('App\Models\User');
    }
}
