<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shipment extends Model {

    use HasFactory;

    const ID = 'id';
    const NAME = 'name';
    const CODE = 'code';
    const SERVICE = 'service';
    const IS_ACTIVE = 'is_active';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    protected $hidden = [SELF::CREATED_AT];

    function payment() {
        return $this->morphMany('App\Models\Payment', 'shipmentable');
    }
}
