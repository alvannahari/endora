<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ShippingHistory extends Model {

    const IMAGE_PATH = 'shipment/';

    const ID = 'id';
    const PAYMENT_ID = 'payment_id';
    const STATUS = 'status_id';
    const IMAGE = 'image';
    const LATITUDE = 'latitude';
    const LONGITUDE = 'longitude';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    public function getImageAttribute($value) {
        if ($value == null) return null;
        else return asset('storage/'.Self::IMAGE_PATH.$value);
        // if ($value == null) return null;
        // else return Storage::url(Self::IMAGE_PATH.$value);
    }

    function status() {
        return $this->belongsTo('App\Models\Status');
    }
}
