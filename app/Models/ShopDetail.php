<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ShopDetail extends Model {

    const LOGO_PATH = 'shop/';

    const ID = 'id';
    const LOGO = 'logo';
    const NAME = 'name';
    const ADDRESS = 'address';
    const PROVINCE = 'province';
    const CITY = 'city';
    const POSTAL_CODE = 'postal_code';
    const LATITUDE = 'latitude';
    const LONGITUDE = 'longitude';
    const PHONE = 'phone';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    public function getLogoAttribute($value) {
        return asset('storage/'.Self::LOGO_PATH.$value);
        // return Storage::url(Self::LOGO_PATH.$value);
    }
}
