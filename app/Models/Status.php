<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model {

    const ID = 'id';
    const NAME = 'name';
    const DESCRIPTION = 'description';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    protected $hidden = [SELF::CREATED_AT, SELF::UPDATED_AT];

    function histories() {
        return $this->hasMany('App\Models\ShippingHistory');
    }
}
