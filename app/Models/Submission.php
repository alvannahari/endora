<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Submission extends Model {
    
    use HasFactory;

    const ID = 'id';
    const PAYMENT_ID = 'payment_id';
    const TYPE = 'type';
    const VIDEO = 'video';
    const DESCRIPTION = 'description';
    const SOLUTION = 'solution';
    const PHONE = 'phone';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];
    
    public function payment() {
        return $this->belongsTo('App\Models\Payment');
    }
}
