<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

    const ID = 'id';
    const CATEGORY_ID = 'category_id';
    const ITEM_ID = 'item_id';
    const LABEL_ID = 'label_id';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s'
    ];
}
