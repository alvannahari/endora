<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {
    
    use HasFactory;

    const ID = 'id';
    const PAYMENT_ID = 'payment_id';
    const INCOME = 'income';
    const OUTCOME = 'outcome';
    const STATUS = 'status';

    protected $guarded = [];

    public $incrementing = false;

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function generateId() {
        $id = 'PAYOUT-'.substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,12);

        if ($this->idExists($id)) return $this->generateId();

        return $id;
    }

    function idExists($id) {
        return $this->where(Self::ID, $id)->exists();
    }

    function payment() {
        return $this->belongsTo(Payment::class);
    }

    function history() {
        return $this->hasOne(HistoryAdmin::class);
    }
}
