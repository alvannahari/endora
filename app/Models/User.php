<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Constants;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable {

    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    const IMAGE_PATH = 'profile/customer/';

    const ID = 'id';
    const USERNAME = 'username';
    const EMAIL = 'email';
    const EMAIL_VERIFIED = 'email_verified_at';
    const PASSWORD = 'password';
    const REMEMBER_TOKEN = 'remember_token';
    const IMAGE = 'image';
    const TOKEN_FCM = 'token_fcm';
    const TOKEN_SOCMED = 'token_socmed';
    const DEVICE_ID = 'device_id';
    const IS_ACTIVE = 'is_active';

    protected $guarded = [];
    
    protected $hidden = [SELF::PASSWORD];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function delete() {
        $this->favourites()->delete();
        $this->carts()->delete();
        $this->locations()->delete();
        $this->searchs()->delete();
        return parent::delete();
    }

    function forceDelete() {
        if ($this->attributes[SELF::IMAGE] != null) 
            Storage::disk('public')->delete(Self::IMAGE_PATH.$this->attributes[Self::IMAGE]);
            
        return parent::forceDelete();
    }

    public function getImageAttribute($value) {
        if ($value == null) return asset('storage/'.Self::IMAGE_PATH.'default.png'); 
        else return asset('storage/'.Self::IMAGE_PATH.$value);
        // if ($value == null) return Storage::url(Self::IMAGE_PATH.'default.png');
        // else return Storage::url(Self::IMAGE_PATH.$value);
    }

    function favourites() {
        return $this->hasMany('App\Models\Favourite');
    }

    function carts() {
        return $this->hasMany('App\Models\Cart');
    }

    function locations() {
        return $this->hasMany('App\Models\Location');
    }

    function payments() {
        return $this->hasMany('App\Models\Payment');
    }

    function searchs() {
        return $this->hasMany('App\Models\SearchHistory');
    }

    function virtualAccounts() {
        return $this->hasMany(VirtualAccounts::class);
    }

    function baseVA($va = null) {
        return $va ?? Constants::wrapBCA($this->id);
    }

    function generateVAIfNotExist() {
        $vas = $this->virtualAccounts;
        $ret = $vas->first();
        $vaNumbers = $vas->pluck('number');
        if(!in_array( $this->baseVA(), $vaNumbers->toArray())){
            $ret = $this->generateVA();
        }
        
        return $ret;
    }

    function generateVA($type = "bca", $va = null) {
        $va = $this->baseVA();
        $created = $this->virtualAccounts()->create([
            "alias" => Constants::vaAliasPrefix($this->username),
            "type" => $type,
            "number" => $va,
        ]);

        return $created;
    }

    function is_favourite($item_id) {
        return (boolean) $this->favourites()->where('item_id', $item_id)->first(['user_'.SELF::ID]);
    }

    function generateUsername() {
        $username = substr(str_shuffle('123456789'),1,4);

        if ($this->usernameExists($username)) return $this->generateUsername();

        return $username;
    }

    function usernameExists($username) {
        return $this->where(Self::USERNAME, $username)->exists();
    }

    public function routeNotificationForFcm($notification) {
        return $this->token_fcm;
    }
}
