<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VAPayments extends Model {

    protected $table = "va_payments";
    
    protected $fillable = [
        "va_id",
        "payment_id",
    ];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function va() {
        return $this->hasOne(VirtualAccounts::class, "id", "va_id");
    }

    public function payment(){
        return $this->hasOne(Payment::class, "id", "payment_id");
    }

    public function pending(){
        return $this->where("status", "0");
    }

    public function setPaid(){
        $this->status = "1";
        $this->payment->setPaid()->save();
        return $this;
    }
}
