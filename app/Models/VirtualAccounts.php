<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VirtualAccounts extends Model {
    use HasFactory;

    protected $table = "virtual_accounts";
    
    protected $fillable = [
        "alias",
        "type",
        "number",
        "valid_until",
    ];

    public function vaPayments(){
        return $this->hasMany(VAPayments::class,"va_id");
    }
}
