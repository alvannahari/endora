<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Xendits extends Model {
    
    const ID = 'id';
    const EXTERNAL_ID = 'external_id';
    const BANK_CODE = 'bank_code';
    const NAME = 'name';
    const EXPECTED_AMOUNNT = 'expected_amount';
    const IS_CLOSED = 'is_closed';
    const EXPIRATION_DATE = 'expiration_date';
    const IS_SINGLE_USE = 'is_single_use';

    const PAYER_EMAIL = 'payer_email';
    const DESCRIPTION = 'description';
    const AMOUNT = 'amount';
    const INVOICE_DURATION = 'invoice_duration';
    const SUCCESS_REDIRECT = 'success_redirect_url';
    const FAILURE_REDIRECT = 'failure_redirect_url';
    const PAYMENT_METHODS = 'payment_methods';
    const CURRENCY = 'currency';
    

}
