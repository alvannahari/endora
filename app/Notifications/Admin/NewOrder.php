<?php

namespace App\Notifications\Admin;

use App\Models\Payment;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewOrder extends Notification {

    use Queueable;

    private $user;
    private $payment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, Payment $payment) {
        $this->user = $user;
        $this->payment = $payment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) {
        return ['database','broadcast'];
    }

    public function toDatabase($notifiable) {
        return [
            User::USERNAME  => $this->user[User::USERNAME],
            User::IMAGE     => $this->user[User::IMAGE],
            'payment'  => [
                Payment::ID                 => $this->payment[Payment::ID],
                Payment::STATUS_PAYMENT     => $this->payment[Payment::STATUS_PAYMENT],
            ]
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'created_at' => now(),
            'data' => [
                User::USERNAME  => $this->user[User::USERNAME],
                User::IMAGE     => $this->user[User::IMAGE],
                'payment'  => [
                    Payment::ID                 => $this->payment[Payment::ID],
                    Payment::STATUS_PAYMENT     => $this->payment[Payment::STATUS_PAYMENT],
                ]
            ],
        ];
    }

}
