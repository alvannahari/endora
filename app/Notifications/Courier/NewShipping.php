<?php

namespace App\Notifications\Courier;

use App\Models\Payment;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Benwilkins\FCM\FcmMessage;

class NewShipping extends Notification {

    use Queueable;

    private $user;
    private $payment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Payment $payment) {
        $this->payment = $payment;
        $this->user = $payment->user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) {
        return ['database','fcm'];
    }

    public function toDatabase($notifiable) {
        return [
            User::USERNAME  => $this->user[User::USERNAME],
            'payment'  => [
                Payment::ID                 => $this->payment[Payment::ID],
                Payment::INVOICE_NUMBER     => $this->payment[Payment::INVOICE_NUMBER],
                Payment::WAYBILL_NUMBER     => $this->payment[Payment::WAYBILL_NUMBER],
                Payment::TOTAL_SHIPMENT     => $this->payment[Payment::TOTAL_SHIPMENT],
                Payment::DESCRIPTION        => $this->payment[Payment::DESCRIPTION],
                'shipper_name'              => $this->payment->shipmentable->fullname,
                'status_shipment'           => $this->payment->histories[0]->status->name
            ]
        ];
    }
    
    public function toFcm($notifiable) {
        $message = new FcmMessage();
        $message->content([
            'title'        => 'Pengiriman Baru', 
            'body'         => 'Anda menerima pengiriman paket yang harus dikirimkan.', 
        ])->data([
            Payment::ID                 => $this->payment[Payment::ID],
            'notification_id'           => $notifiable->id,
            Payment::INVOICE_NUMBER     => $this->payment[Payment::INVOICE_NUMBER],
            Payment::WAYBILL_NUMBER     => $this->payment[Payment::WAYBILL_NUMBER],
            Payment::TOTAL_SHIPMENT     => $this->payment[Payment::TOTAL_SHIPMENT],
            'shipper_name'              => $this->payment->shipmentable->fullname,
            'status_shipment'           => $this->payment->histories[0]->status->name
        ]);
        // ])->priority(FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.
    
        return $message;
    }
}
