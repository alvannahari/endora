<?php

namespace App\Notifications\Customer;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Benwilkins\FCM\FcmMessage;
use App\Models\Payment;
use App\Models\User;

class NewStatusPayment extends Notification {

    use Queueable;

    private $payment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Payment $payment) {
        $this->payment = $payment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) {
        return ['database','fcm'];
    }

    public function toDatabase($notifiable) {
        return [
            User::USERNAME  => $this->payment->user->username,
            'payment'  => [
                Payment::ID             => $this->payment[Payment::ID],
                'status_payment'        => $this->payment->status_payment
            ]
        ];
    }
    
    public function toFcm($notifiable) {
        $message = new FcmMessage();
        if ($this->payment->status_payment == '0') {
            $message->content([
                'title'        => 'Status Pembayaran', 
                'body'         => 'Silahkan lakukan pembayaran tagihan.', 
            ]);
        } else if ($this->payment->status_payment == '1') {
            $message->content([
                'title'        => 'Status Pembayaran', 
                'body'         => 'Pembayaran tagihan telah berhasil dilakukan.', 
            ]);
        }
        $message->data([
            Payment::ID                 => $this->payment[Payment::ID],
            'notification_id'           => $notifiable->id,
            // Payment::COURIER_ID         => $this->payment[Payment::COURIER_ID],
            // Payment::COURIER_TYPE       => $this->payment[Payment::COURIER_TYPE],
            'status_payment'            => $this->payment->status_payment
        ]);
        
        // ])->priority(FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.
    
        return $message;
    }
}
