<?php

namespace App\Notifications\Customer;

use App\Models\Payment;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Benwilkins\FCM\FcmMessage;

class NewStatusShipping extends Notification
{
    use Queueable;

    private $payment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Payment $payment) {
        $this->payment = $payment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) {
        return ['database','fcm'];
    }

    public function toDatabase($notifiable) {
        return [
            User::USERNAME  => $this->payment->user->username,
            'payment'  => [
                Payment::ID                 => $this->payment[Payment::ID],
                Payment::COURIER_ID         => $this->payment[Payment::COURIER_ID],
                Payment::COURIER_TYPE       => $this->payment[Payment::COURIER_TYPE],
                'shipper_name'              => $this->payment->shipmentable->fullname ?? $this->payment->shipmentable->name ?? 'null',
                'status_shipment'           => $this->payment->histories[0]->status->name
            ]
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    // public function toArray($notifiable)
    // {
    //     return [
    //         'id' => $this->id,
    //         'created_at' => now(),
    //         'data' => [
    //             User::USERNAME  => 'test username',
    //             'payment'  => [
    //                 Payment::ID                 => $this->payment[Payment::ID],
    //                 Payment::COURIER_ID         => $this->payment[Payment::COURIER_ID],
    //                 Payment::COURIER_TYPE       => $this->payment[Payment::COURIER_TYPE],
    //                 Payment::STATUS_PAYMENT     => $this->payment[Payment::STATUS_PAYMENT],
    //             ]
    //         ],
    //     ];
    // }

    public function toFcm($notifiable) {
        $message = new FcmMessage();
        $message->content([
            'title'        => 'Status Pengiriman', 
            'body'         => 'Status paket anda '.$this->payment->histories[0]->status->name.'.', 
        ])->data([
            Payment::ID                 => $this->payment[Payment::ID],
            'notification_id'           => $notifiable->id,
            Payment::COURIER_ID         => $this->payment[Payment::COURIER_ID],
            Payment::COURIER_TYPE       => $this->payment[Payment::COURIER_TYPE],
            'shipper_name'              => $this->payment->shipmentable->fullname ?? $this->payment->shipmentable->name,
            'status_shipment'           => $this->payment->histories[0]->status->name
        ]);
        // ])->priority(FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.
    
        return $message;
    }
}
