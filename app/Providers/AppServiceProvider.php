<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path() . '/Helpers/APIHelp.php';
        // require_once app_path() . '/Constant.php';
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'offer'     => 'App\Models\Offer',
            'category'  => 'App\Models\Category',
            'brand'     => 'App\Models\Brand',
            'courier'   => 'App\Models\Courier',
            'shipment'  => 'App\Models\Shipment',
        ]);
    }
}
