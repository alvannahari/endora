<?php

namespace App\Services\Abstracts;

abstract class AbstractAuth{
    public $access_token,
        $header,
        $baseUrl;
    
    protected $client_id = null, 
        $client_secret = null;

    const OAUTH_AUTH = "/api/oauth/token";

    public function __construct($client_id = null, $client_secret = null){
    }

    public function setHeader(){
    }

    public function doAuth(){
        $url = $this->getBaseUrl().self::OAUTH_AUTH;

        $access_token = "retirvd";
        preout($url);
        $this->setAccessToken($access_token);
    }

    //regenerate or from storage
    abstract function getConditionalAccessToken();
    
    /* typical usage

    public function getConditionalAccessToken(){
        //retrieve from db
        $accToken = $this->getAccTokenFromDb();

        // if exist and not expired
        if($accToken){
            $this->setAccessToken($access_token);
        }else{
            //regenerate
            $this->doAuth();
        }

        return ;
    }

    */

    // return a row of acc token
    abstract function getAccTokenFromDb();
    
    abstract function isUsable($accToken);
 
	public function getAccessToken(){
		return $this->access_token;
	}

	public function setAccessToken($access_token){
		$this->access_token = $access_token;

		return $this;
	}
 
	public function getBaseUrl(){
		return $this->baseUrl;
	}

	public function setBaseUrl($baseUrl){
		$this->baseUrl = $baseUrl;

		return $this;
	}
}