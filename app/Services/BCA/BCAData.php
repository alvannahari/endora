<?php

namespace App\Services\BCA;

class BCAData{
    public 
        $CompanyCode = 1,
        $CustomerNumber = 1,
        $RequestID = 1,
        $AdditionalData = 1;
        // $ChannelType = 1,
        // $TransactionDate = 1,
    
    public function setCoreData($CompanyCode,
        $CustomerNumber,
        $RequestID,
        $AdditionalData){
            $this->AdditionalData = $AdditionalData;
            $this->RequestID = $RequestID;
            $this->CustomerNumber = $CustomerNumber;
            $this->CompanyCode = $CompanyCode;

            return $this;
    }
}