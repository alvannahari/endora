<?php

namespace App\Services\BCA;

use App\Services\Abstracts\AbstractAuth;

class BCAOAuth extends AbstractAuth{
    const OAUTH_AUTH = "/api/oauth/token";

    public function __construct($client_id = null, $client_secret = null){
        parent::__construct($client_id, $client_secret);
    }

    public function setHeader(){
    }

    public function doAuth(){
        $url = $this->getBaseUrl().self::OAUTH_AUTH;
        $access_token = "a";
        preout($url);
        $this->setAccessToken($access_token);

        return $this;
    }

    public function getConditionalAccessToken(){
        //retrieve from db
        $accToken = $this->getAccTokenFromDb();

        // if exist and not expired
        if($this->isUsable($accToken)){
            $this->setAccessToken($accToken);
        }else{
            //regenerate
            $this->doAuth();
        }

        return $this->getAccessToken();
    }

    // return a row of acc token
    public function getAccTokenFromDb(){
        return null;
    }

    // return true if acc token isUsable($accToken)
    public function isUsable($accToken){
        $at = $accToken;
        
        $expired = date('Y-m-d H:i:s') >= $accToken->expires_in;

        if(!$at){
            return false;
        }

        if($expired){
            return false;
        }
        return true;
    }
}