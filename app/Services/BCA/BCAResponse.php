<?php

namespace App\Services\BCA;

// response from 3rd party for bca server
class BCAResponse extends BCAData{
    public 
        $CustomerName = '',
        $CurrencyCode = 'IDR',
        $TotalAmount = 0,
        // $SubCompany = '00000',
        $DetailBills = null,
		$FreeTexts = []
        ;
 
	public function getDetailBills(){
		return $this->DetailBills;
	}

	public function initDetailBills(){
		$this->DetailBills = [];

		return $this;
	}
	
	public function setDetailBills(DetailBills $DetailBills){
		$this->DetailBills = $DetailBills;
		$this->setTotalAmount($DetailBills->getBillAmount());

		return $this;
    }
	
	public function addDetailBills(DetailBills $DetailBills){
		if(!$this->DetailBills){
			$this->initDetailBills();
		}
		$this->DetailBills[] = $DetailBills;
		$this->addTotalAmount($DetailBills->getBillAmount());

		return $this;
	}
 
	public function getFreeTexts(){
		return $this->FreeTexts;
	}

	public function setFreeTexts($FreeTexts){
		$this->FreeTexts = $FreeTexts;

		return $this;
	}
    
    public function addFreeTexts($FreeTexts){
		$this->FreeTexts[] = $FreeTexts;

		return $this;
	}
 
	public function getTotalAmount(){
		return $this->TotalAmount;
	}

	public function setTotalAmount($TotalAmount){
		$this->TotalAmount = $TotalAmount;

		return $this;
	}

	public function setTotalAmountToString(){
		$this->TotalAmount = (string) $this->TotalAmount;

		return $this;
	}

	public function addTotalAmount($TotalAmount){
		$this->TotalAmount += $TotalAmount;

		return $this;
	}
 
	public function getCustomerName(){
		return $this->CustomerName;
	}

	public function setCustomerName($CustomerName){
		$this->CustomerName = $CustomerName;

		return $this;
	}
}