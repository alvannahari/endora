<?php

namespace App\Services\BCA;

use DB;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Constants;
use App\Models\VirtualAccounts;
use App\Services\BCA\Handlers\InquiryHandler;
use App\Services\BCA\Exceptions\BCAException;

class BCAServices{

    const URL_BILLS = "/va/bills";
    const URL_PAYMENT = "/va/payments";
    
    public $bcaAuth;
    public static $CompanyCode;

    public function __construct(){
        $this->bcaAuth = new BCAOAuth();
        self::$CompanyCode = config("bca.companycode");
    }

    // thrower
    public function throwsExcept($id, $eng){
        $be = new BCAException();
        $be->setIdn($id)->setEng($eng);
        throw new $be;
    }

    public function auth(){
        // $accToken = $this->bcaAuth->getConditionalAccessToken();
        // preout($accToken);
    }

    public function getVaPayment($number){
        // $vaTrans = VirtualAccounts::where("number", $number)->first();
        $vaTrans = VirtualAccounts::where("number", "LIKE", "%".$number)->first();

        if(!$vaTrans){
            throw new \Exception("va.notexist");
        }

        $vaPayment = $vaTrans->vaPayments->where('status',"0")->first();

        // preout($vaPayment);
        if(!$vaPayment){
            throw new \Exception("zero.pending.transaction");
        }else{
            // with va & payment data
            $vaPayment->va;
            $vaPayment->payment;
        }

        return $vaPayment;
    }

    public function validateMandatoryParam($class, $payload){
        foreach($class::$required as $r){
            if(empty($payload[$r])){
                throw new \Exception($r.".empty");
            }
        }

        if(!is_numeric($payload["CompanyCode"])){
            throw new \Exception("CompanyCode.notnumeric");
        }
        if(!is_numeric($payload["CustomerNumber"])){
            throw new \Exception("CustomerNumber.notnumeric");
        }
        
        $dateFmt = Constants::BCA_VA_TRANS_DATE_FMT;
        $validDate = preg_match($dateFmt, $payload["TransactionDate"]);

        if(!$validDate){
            throw new \Exception("Invalid date fmt");
        }
    }

    public function validatePayload($type = "", $payload = null){
        $valid = true;
        $msg = [];
        $data = null;
        $payload = json_decode($payload);

        try{
            if(!$payload){
                throw new \Exception("invalid.payload");
            }
            switch($type){
                case "inquiry":
                    $payload = (array) $payload;
                    
                    $this->validateMandatoryParam(InquiryResponse::class, $payload);
                    $vaPayment = $this->getVaPayment($payload["CustomerNumber"]);

                    $data = $vaPayment;
                break;
                case "flags":
                    $payload = (array) $payload;
                    
                    $this->validateMandatoryParam(FlaggingResponse::class, $payload);

                    if(! in_array($payload['FlagAdvice'],["Y","N"]) ){
                        throw new \Exception("Invalid Flag advice");
                    }
                break;
                default:
                break;
            }
        // try{
        }catch(\Exception $e){
            $msg = $e->getMessage();
            $valid = false;
        }

        return [
            "message" => $msg,
            "valid" => $valid,
            "data" => $data,
        ];
    }

    /**
     * convert endora transct to bca inquiry resp
     * 
     * @return InquiryResponse
     * 
     */
    public function transactionToInquiryData($payload, $vaPayment = null, $isValid = false, $msg = ""){
        $response = new InquiryResponse();
        try{
            $CompanyCode = $payload->CompanyCode;
            $RequestID = $payload->RequestID;
            $CustomerNumber = $payload->CustomerNumber;
            $AdditionalData = $payload->AdditionalData;
            
            $response->setCoreData($CompanyCode, $CustomerNumber, $RequestID, $AdditionalData);
            
            if(!$isValid){
                $response->setStatusFail()
                    ->setInquiryReason( Multilang::ideng($msg, $msg) );
            }else{
                $payment = $vaPayment->payment;
                $total = abs($payment->total) + abs($payment->total_shipment);
                $response->setCustomerName($vaPayment->va->alias)
                    ->setTotalAmount($total)
                    ->setTotalAmountToString();
                
                if($payment->isExpired()){
                    throw new \Exception("Bill expired");
                }
                // $orders = $payment->order()->with('item')->get();
                
                // foreach($orders as $key => $ord){
                //     $detailBills = new DetailBills;
                //     $detailBills->setBillDescriptionLang($ord->name, $ord->name)
                //         ->setBillAmountAndNum($ord->total, $key + 1);
                    
                //     $response->addDetailBills($detailBills);
                // }
            }
        }catch(\Exception $e){
            $response->setStatusFail()
                ->setInquiryReason( Multilang::ideng($e->getMessage(), $e->getMessage()) );
        }
        
        return $response;
    }

    /**
     * flag endora transct as paid
     * 
     * @return FlaggingResponse
     * 
     */
    public function flagPayment($payload, $vaPayment = null, $isValid = false, $msg = ""){
        $response = new FlaggingResponse();
        try{
            DB::beginTransaction();
            
            $CompanyCode = $payload->CompanyCode;
            $RequestID = $payload->RequestID;
            $CustomerNumber = $payload->CustomerNumber;
            
            $CustomerName = $payload->CustomerName;
            $PaidAmount = $payload->PaidAmount;
            $TotalAmount = $payload->TotalAmount;
            $TransactionDate = $payload->TransactionDate;
            
            $response->setCoreData($CompanyCode, $CustomerNumber, $RequestID, "");
            $response->setCustomerName($CustomerName)
                ->setTotalAmount($TotalAmount)
                ->setPaidAmount($PaidAmount)
                ->setTransactionDate($TransactionDate);
            
            if($isValid){
                $vaPayment = $this->getVaPayment($payload->CustomerNumber);
                $payment = $vaPayment->payment;
                $total = abs($payment->total) + abs($payment->total_shipment);

                if((int) $PaidAmount != (int) $total){
                    // throw new \Exception("PaidAmount != Total @DB");
                    throw new \Exception("Pembayaran tidak sesuai dengan total checkout terakhir");
                }
                
                if((int) $TotalAmount != (int) $total){
                    // throw new \Exception("TotalAmount != Total @DB");
                    throw new \Exception("Total Pembayaran tidak sesuai dengan total checkout terakhir");
                }

                if($payment->isExpired()){
                    throw new \Exception("Bill expired");
                }

                $vaPayment->setPaid()->save();
                
                DB::commit();
            }else{
                DB::rollBack();
                throw new \Exception("Invalid payload ".$msg);
            }
        // try{
        }catch(\Exception $e){
            DB::rollBack();
            $response->setStatusFail()
                ->setPaymentFlagReason( Multilang::ideng($e->getMessage(), $e->getMessage()) );
        }
        
        return $response;
    }

    public function handleInquiry($payload = null){
        $result = [];
        $validation = $this->validatePayload("inquiry", $payload);

        $inquiry = json_decode($payload);
        $result = $this->transactionToInquiryData($inquiry, $validation['data'], $validation['valid'], $validation['message']);
        
        return $result;
    }

    public function handleFlagging($payload = null){
        $result = [];

        $validation = $this->validatePayload("flags", $payload);

        $flags = json_decode($payload);
        $result = $this->flagPayment($flags, $validation['data'], $validation['valid'], $validation['message']);
        
        return $result;
    }
}