<?php

namespace App\Services\BCA;

class DetailBills{
    public $BillDescription = [],
    $BillAmount = 0,
    $BillNumber = 0,
    $BillSubCompany = 0;
    
    public function __construct(){
        $this->setBillDescription(Multilang::ideng());
    }

    public function getBillDescription(){
		return $this->BillDescription;
	}

	public function setBillDescription($BillDescription){
		$this->BillDescription = $BillDescription;

		return $this;
    }
    
	public function setBillDescriptionLang($idn, $eng){
		$this->BillDescription['Indonesian'] = $idn;
		$this->BillDescription['English'] = $eng;

		return $this;
	}

	public function getBillAmount(){
		return $this->BillAmount;
	}

	public function setBillAmountAndNum($BillAmount, $BillNumber){
		$this->setBillAmount($BillAmount);
		$this->setBillNumber($BillNumber);

		return $this;
	}

	public function setBillAmount($BillAmount){
		$this->BillAmount = $BillAmount;

		return $this;
	}
 
	public function getBillNumber(){
		return $this->BillNumber;
	}

	public function setBillNumber($BillNumber){
		$this->BillNumber = $BillNumber;

		return $this;
	}
 
	public function getBillSubCompany(){
		return $this->BillSubCompany;
	}

	public function setBillSubCompany($BillSubCompany){
		$this->BillSubCompany = $BillSubCompany;

		return $this;
	}
}