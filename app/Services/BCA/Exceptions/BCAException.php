<?php

namespace App\Services\BCA\Exceptions;

// use Exception;

class BCAException extends \Exception{
    public $idn = "err", $eng = "err";
 
	public function getEng(){
		return $this->eng;
	}

	public function setEng($eng){
		$this->eng = $eng;
		$this->message = $eng;

		return $this;
	}
 
	public function getIdn(){
		return $this->idn;
	}

	public function setIdn($idn){
		$this->idn = $idn;
		$this->message = $idn;

		return $this;
	}
}