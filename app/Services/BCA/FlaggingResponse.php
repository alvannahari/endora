<?php

namespace App\Services\BCA;

// response from 3rd party for bca server
class FlaggingResponse extends BCAResponse{
    public 
		$PaidAmount = 0,
		$PaymentFlagStatus = "00",
		$PaymentFlagReason = [
            "Indonesian" => "Sukses",
            "English" => "Success",
		],
		// $Reference = "",
		// $FlagAdvice = "N",
		$TransactionDate = "";
	
	public static $required = [
		"CompanyCode",
		"CustomerNumber",
		"CustomerName",
		"CurrencyCode",
		"TotalAmount",
		"RequestID",
		"ChannelType",
		"TransactionDate",
		"PaidAmount",
		"SubCompany",
		"Reference",
	];

	const STATUS_OK = "00";
    const STATUS_FAIL = "01";
    const STATUS_TIMEOUT = "02";
    
    public function __construct(){

    }
 
	public function getPaidAmount(){
		return $this->PaidAmount;
	}

	public function setPaidAmount($PaidAmount){
		$this->PaidAmount = $PaidAmount;

		return $this;
	}
 
	public function getPaymentFlagStatus(){
		return $this->PaymentFlagStatus;
	}

	public function setStatusOk(){
		return $this->setPaymentFlagStatus(self::STATUS_OK);
	}

	public function setStatusFail(){
		return $this->setPaymentFlagStatus(self::STATUS_FAIL);
	}

	public function setPaymentFlagStatus($PaymentFlagStatus){
		$this->PaymentFlagStatus = $PaymentFlagStatus;

		return $this;
	}
 
	public function getPaymentFlagReason(){
		return $this->PaymentFlagReason;
	}

	public function setPaymentFlagReason($PaymentFlagReason){
		$this->PaymentFlagReason = $PaymentFlagReason;

		return $this;
	}
 
	public function getReference(){
		return $this->Reference;
	}

	public function setReference($Reference){
		$this->Reference = $Reference;

		return $this;
	}
 
	public function getFlagAdvice(){
		return $this->FlagAdvice;
	}

	public function setFlagAdvice($FlagAdvice){
		$this->FlagAdvice = $FlagAdvice;

		return $this;
	}
 
	public function getTransactionDate(){
		return $this->TransactionDate;
	}

	public function setTransactionDate($TransactionDate){
		$this->TransactionDate = $TransactionDate;

		return $this;
	}
}