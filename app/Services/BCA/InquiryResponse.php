<?php

namespace App\Services\BCA;

// response from 3rd party for bca server
class InquiryResponse extends BCAResponse{
    public 
        $InquiryStatus = "00",
        $InquiryReason = [
            "Indonesian" => "Sukses",
            "English" => "Success",
		],
		$SubCompany = '00000',
		$AdditionalData = '';
	
	public static $required = [
		"CompanyCode",
		"CustomerNumber",
		"RequestID",
		"ChannelType",
		"TransactionDate",
	];

    const STATUS_OK = "00";
    const STATUS_FAIL = "01";
    
    public function __construct(){

    }
 
	public function getInquiryReason(){
		return $this->InquiryReason;
	}

	// Multilang::ideng, to do : validate
	public function setInquiryReason($InquiryReason){
		$this->InquiryReason = $InquiryReason;

		return $this;
	}
 
	public function getInquiryStatus(){
		return $this->InquiryStatus;
	}

	public function setStatusOk(){
		return $this->setInquiryStatus(self::STATUS_OK);
	}

	public function setStatusFail(){
		return $this->setInquiryStatus(self::STATUS_FAIL);
	}

	public function setInquiryStatus($InquiryStatus){
		$this->InquiryStatus = $InquiryStatus;

		return $this;
	}
}