<?php

namespace App\Services\BCA;

class Multilang{
    static function ideng($id = "", $eng = ""){
        return [
            "Indonesian" => $id,
            "English" => $eng
        ];
    }
}