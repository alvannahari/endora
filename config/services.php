<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'fcm' => [
        'key' => env('FCM_SECRET_KEY')
    ],
    'xendit' => [
        'key' => env('XENDIT_KEY')
    ],
    'bca' => [
        'client' => [
            'id' => env('BCA_CLIENT_ID'),
            'secret' => env('BCA_CLIENT_SECRET'),
            'acctokendev' => env('BCA_CLIENT_ACC_TOKEN_DEV'),
        ],
        'companycode' => env('BCA_COMPANY_CODE'),
    ],
    'rajaongkir' => [
        'key' => env('RAJAONGKIR_API_KEY'),
        'package' => env('RAJAONGKIR_PACKAGE')
    ]
];
