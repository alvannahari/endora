<?php

namespace Database\Factories;

use App\Models\HistoryAdmin;
use Illuminate\Database\Eloquent\Factories\Factory;

class HistoryAdminFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = HistoryAdmin::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
