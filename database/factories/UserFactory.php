<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * 
     */

    public function definition() {
        return [
            User::USERNAME          => $this->faker->firstName,
            User::EMAIL             => $this->faker->unique()->email,
            User::EMAIL_VERIFIED    => now(),
            User::PASSWORD          => bcrypt('userdataseeder'),
            User::REMEMBER_TOKEN    => Str::random(10),
            User::TOKEN_FCM         => Str::random(15),
            User::DEVICE_ID         => rand(100, 900).'.'.rand(100, 900).'.'.rand(100, 900),
            User::IS_ACTIVE         => '1',
            User::CREATED_AT        => now(),
            User::UPDATED_AT        => now()
        ];
    }
}
