<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments(User::ID);
            $table->string(User::USERNAME)->unique();
            $table->string(User::EMAIL)->unique();
            $table->timestamp(User::EMAIL_VERIFIED)->nullable();
            $table->string(User::PASSWORD);
            $table->rememberToken();
            $table->string(User::IMAGE)->nullable();
            $table->string(User::TOKEN_FCM)->nullable();
            $table->string(User::DEVICE_ID);
            $table->enum(User::IS_ACTIVE, [0,1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
