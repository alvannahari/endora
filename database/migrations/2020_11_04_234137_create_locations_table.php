<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Location;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments(Location::ID);
            $table->integer(Location::USER_ID);
            $table->string(Location::CATEGORY);
            $table->string(Location::RECEIVER);
            $table->string(Location::PHONE);
            $table->string(Location::ADDRESS);
            $table->string('district');
            $table->string('city');
            $table->string('province');
            $table->integer(Location::POSTAL_CODE);
            $table->text(Location::DESCRIPTION)->nullable();
            $table->decimal(Location::LATITUDE, 10 ,8);
            $table->decimal(Location::LONGITUDE, 11, 8);
            $table->enum(Location::IS_MAIN, [0,1])->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
