<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Payment;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->string(Payment::ID, 21)->primary();
            $table->string("type")->nullable()->default('xendit');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer(Payment::LOCATION_ID);
            $table->integer(Payment::COURIER_ID);
            $table->enum(Payment::COURIER_TYPE, ['shipment', 'courier']);
            $table->string(Payment::TYPE, 100);
            $table->string(Payment::TYPE_UID, 100);
            $table->decimal(Payment::INVOICE_NUMBER, 21);
            $table->decimal(Payment::WAYBILL_NUMBER, 21)->nullable();
            $table->text(Payment::DESCRIPTION)->nullable();
            $table->integer(Payment::TOTAL_SHIPMENT);
            $table->integer(Payment::TOTAL);
            $table->enum(Payment::STATUS_PAYMENT, [0,1,97,98,99])->default(0);
            $table->timestamp(Payment::EXPIRE_DATE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
