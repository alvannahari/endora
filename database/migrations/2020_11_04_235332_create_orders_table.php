<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Order;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments(Order::ID);
            $table->string(Order::PAYMENT_ID);
            $table->string(Order::NAME);
            $table->integer(Order::PRICE);
            $table->integer(Order::QTY);
            $table->string(Order::VARIANT);
            $table->integer(Order::T_WEIGHT);
            $table->integer(Order::DISCOUNT)->nullable();
            $table->integer(Order::TOTAL);
            $table->text(Order::NOTE)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
