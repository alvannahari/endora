<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Billboard;

class CreateBillboardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billboards', function (Blueprint $table) {
            $table->increments(Billboard::ID);
            $table->string(Billboard::NAME);
            $table->string(Billboard::COVER_SMALL)->nullable();
            $table->string(Billboard::COVER_BIG)->nullable();
            $table->enum(Billboard::IS_IMPORTANT, [0,1])->default(0);
            $table->enum(Billboard::IS_ACTIVE, [0,1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billboards');
    }
}
