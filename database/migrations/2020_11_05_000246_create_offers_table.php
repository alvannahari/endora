<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Offer;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->increments(Offer::ID);
            $table->string(Offer::NAME);
            $table->string(Offer::COVER)->nullable();
            $table->enum(Offer::TYPE, ['low', 'med', 'high']);
            $table->dateTime(Offer::START_OFFER);
            $table->dateTime(Offer::END_OFFER);
            $table->enum(Offer::IS_ACTIVE, [0,1])->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
