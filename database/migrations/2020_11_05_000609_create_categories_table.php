<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Category;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments(Category::ID);
            $table->string(Category::NAME);
            $table->enum(Category::TYPE, ['promo', 'label']);
            $table->string(Category::IMAGE)->nullable();
            $table->integer(Category::DISCOUNT)->nullable();
            $table->integer(Category::MAXIMUM)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
