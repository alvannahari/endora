<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Item;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments(Item::ID);
            $table->integer(Item::BRAND_ID);
            $table->string(Item::NAME);
            $table->text(Item::DESCRIPTION)->nullable();
            $table->integer(Item::PRICE);
            $table->string(Item::VARIANT)->nullable();
            $table->integer(Item::INVENTORY);
            $table->integer(Item::WEIGHT);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
