<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Courier;

class CreateCouriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('couriers', function (Blueprint $table) {
            $table->increments(Courier::ID);
            $table->string(Courier::EMAIL);
            $table->enum(Courier::GENDER, ['Pria', 'Wanita']);
            $table->string(Courier::PASSWORD);
            $table->string(Courier::NUMBER_PLATE);
            $table->date(Courier::DATE_BIRTH);
            $table->string(Courier::IMAGE)->nullable();
            $table->string(Courier::TOKEN_FCM);
            $table->string(Courier::DEVICE_ID);
            $table->enum(Courier::IS_ACTIVE, [0,1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('couriers');
    }
}
