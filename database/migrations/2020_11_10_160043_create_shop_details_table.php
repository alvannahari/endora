<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\ShopDetail;

class CreateShopDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_details', function (Blueprint $table) {
            $table->increments(ShopDetail::ID);
            $table->string(ShopDetail::LOGO);
            $table->string(ShopDetail::NAME);
            $table->string(ShopDetail::ADDRESS);
            $table->string(ShopDetail::PROVINCE);
            $table->string(ShopDetail::CITY);
            $table->string(ShopDetail::POSTAL_CODE);
            $table->decimal(ShopDetail::LATITUDE,  10 ,8);
            $table->decimal(ShopDetail::LONGITUDE,  11 ,8);
            $table->string(ShopDetail::PHONE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_details');
    }
}
