<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\ShippingHistory;

class CreateShippingHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_histories', function (Blueprint $table) {
            $table->increments(ShippingHistory::ID);
            $table->string(ShippingHistory::PAYMENT_ID);
            $table->integer(ShippingHistory::STATUS);
            $table->string(ShippingHistory::IMAGE)->nullable();
            $table->decimal(ShippingHistory::LATITUDE, 10 ,8);
            $table->decimal(ShippingHistory::LONGITUDE, 11, 8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_histories');
    }
}
