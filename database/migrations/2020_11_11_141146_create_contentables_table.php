<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Contentable;

class CreateContentablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contentables', function (Blueprint $table) {
            $table->increments(Contentable::ID);
            $table->integer(Contentable::BILLBOARD_ID);
            $table->integer(Contentable::CONTENT_ID);
            $table->enum(Contentable::CONTENT_TYPE, ['category', 'offer', 'brand']);
            $table->timestamp(Contentable::CREATED_AT)->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contentables');
    }
}
