<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Offerable;

class CreateOfferablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offerables', function (Blueprint $table) {
            $table->increments(Offerable::ID);
            $table->integer(Offerable::OFFER_ID);
            $table->integer(Offerable::OFFERABLE_ID);
            $table->enum(Offerable::OFFERABLE_TYPE, ['category', 'brand']);
            $table->timestamp(Offerable::CREATED_AT)->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offerables');
    }
}
