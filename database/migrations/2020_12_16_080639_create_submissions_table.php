<?php

use App\Models\Submission;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submissions', function (Blueprint $table) {
            $table->increments(Submission::ID);
            $table->string(Submission::PAYMENT_ID);
            $table->string(Submission::TYPE);
            $table->string(Submission::VIDEO);
            $table->text(Submission::DESCRIPTION);
            $table->string(Submission::SOLUTION);
            $table->string(Submission::PHONE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submissions');
    }
}
