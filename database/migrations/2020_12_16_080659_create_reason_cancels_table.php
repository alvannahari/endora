<?php

use App\Models\ReasonCancel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReasonCancelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reason_cancels', function (Blueprint $table) {
            $table->increments(ReasonCancel::ID);
            $table->string(ReasonCancel::PAYMENT_ID);
            $table->string(ReasonCancel::REASON);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reason_cancels');
    }
}
