<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVaPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('va_payments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('va_id');
            $table->foreign('va_id')->references('id')->on('virtual_accounts')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->string('payment_id', 21);
            $table->foreign('payment_id')->references('id')->on('payments')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->enum('status', ['0','1'])->default('0');
            $table->enum("status", [0,1,99])->default(0); // same w/ payments

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('va_payments');
    }
}
