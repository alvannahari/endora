<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSoftDeletesToLocationItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->softDeletes()->after('is_main');
        });
        Schema::table('items', function (Blueprint $table) {
            $table->softDeletes()->after('weight');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('items', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
