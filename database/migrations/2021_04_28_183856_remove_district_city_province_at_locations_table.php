<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveDistrictCityProvinceAtLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->dropColumn('district');
            $table->dropColumn('city');
            $table->dropColumn('province');
            $table->integer('subdistrict_id')->after('address');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->string('district');
            $table->string('city');
            $table->string('province');
            $table->dropColumn('subdistrict_id');
            //
        });
    }
}
