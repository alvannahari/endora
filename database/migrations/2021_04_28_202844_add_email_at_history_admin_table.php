<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEmailAtHistoryAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_admins', function (Blueprint $table) {
            $table->string('email')->after('activity')->nullable();
            $table->string('transaction_id')->after('activity')->nullable();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('history_admins', function (Blueprint $table) {
            $table->dropColumn('email');
            $table->dropColumn('transaction_id');
        });
    }
}
