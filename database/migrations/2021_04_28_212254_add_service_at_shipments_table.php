<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddServiceAtShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipments', function (Blueprint $table) {
            $table->string('code')->after('name');
            $table->string('service')->after('code');
            $table->string('is_active')->after('service')->default('0');
            $table->dropColumn('alias');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipments', function (Blueprint $table) {
            $table->dropColumn('code');
            $table->dropColumn('service');
            $table->dropColumn('is_active');
            $table->string('alias');
        });
    }
}
