<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin;

// php artisan db:seed --class=AdminSeeder
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            Admin::create([
                "id" => 1,
                "is_active" => "1",
                "email" => "admin@gmail.com",
                'email_verified_at' => now(),
                "password" => '$2y$10$miASKW54gvYS3/CGP6Ce6.Nzp6OWGNMknUqwK5iSzpCGjKAOyzRHy', // 123
            ]);
        }catch(\Exception $e){
            echo $e->getMessage();
        }
        //
        
    }
}
