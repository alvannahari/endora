<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Helpers\Constants;
use App\Models\Item;
use Carbon\Carbon;
// php artisan db:seed --class=ItemSeeder
class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{

            $arr = range(0,10);
            foreach($arr as $len){
                $randInt = random_int(1,9);
    
                $data = [
                    'brand_id' => 1,
                    'name' => "product ".$randInt,
                    'description' => "desc",
                    'price' => $randInt*10000,
                    'variant' => "1",
                    'inventory' => "1",
                    'weight' => 1,
                ];
                Item::create($data);
            }
            
        }catch(\Exception $e){
            echo __FILE__.$e->getMessage();
        }
        //
        
    }
}
