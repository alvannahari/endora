<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Helpers\Constants;
use App\Models\User;
use App\Models\Item;
use App\Models\VirtualAccounts;
use Carbon\Carbon;
// php artisan db:seed --class=PaymentSeeder
class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            $users = User::all();
            $items = Item::limit(3)->get();
            // preson($items);
            // exit;
            $users = [$users[0]];

            $orders = [];

            $addd = null;
            foreach($items as $key => $item){
                $order = [
                    [
                        'item_id' => $item->id,
                        'name' => $item->name,
                        'price' => $item->price,
                        'qty' => 1,
                        'variant' => 1,
                        't_weight' => 1,
                        'discount' => 0,
                        'total' => $item->price,
                        'note' => 1,
                    ]
                ];
                $addd = $item;
                
                $orders[] = $order;
            }

            $orders[0][] = [
                'item_id' => $addd->id,
                'name' => $addd->name,
                'price' => $addd->price,
                'qty' => 1,
                'variant' => 1,
                't_weight' => 1,
                'discount' => 0,
                'total' => $addd->price,
                'note' => 1, 
            ];

            // preson($orders);
            // exit;

            foreach($users as $key => $usr){
                $ev = $key%2 == 0;
                $type = $ev ? "bca-va" : "xendit";
                $xid = $ev ? "bca" : "xendit";
                $num = random_int(1,9);
                $total = 0;
                // $total = $num * 1_000;
                $statusp = $ev ? "1" : "0";
                // if($ev) $total * 10;
                $statusp = $ev ? "0" : "1";
                $invoice_number = 111;
                $waybill_number = 123;
                $totalShip = 15_000;

                $inp = $orders[array_rand($orders)];

                foreach($inp as $ord){
                    $total += $ord['total'];
                }

                $va = $usr->virtualAccounts;

                $payment = $usr->payments()->create([
                    'id' => Constants::paymentCode(),
                    'type' => $type,
                    'location_id' => "1",
                    'shipmentable_id' => "1",
                    'shipmentable_type' => "1",
                    'xendit_id' => $xid,
                    'invoice_number' => $invoice_number,
                    'waybill_number' => $waybill_number,
                    'description' => "Bayar memakai ".$type,
                    'total_shipment' => $totalShip,
                    'total' => $total,
                    'status_payment' => 0,
                    'expire_date' => Carbon::now()->addDays(1)
                ]);

                // preout( $inp );
                $payment->vaPayments()->create([
                    "va_id" => $va[0]->id
                ]);
                $payment->order()->createMany($inp);
                break;
            }
            
        }catch(\Exception $e){
            echo __FILE__.$e->getMessage();
        }
        //
        
    }
}
