<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

// php artisan db:seed --class=UserSeeder
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            User::create([
                "id" => 1,
                "username" => "User t1",
                "is_active" => "1",
                "email" => "t1@gmail.com",
                'email_verified_at' => now(),
                User::DEVICE_ID => "1",
                "password" => '$2y$10$miASKW54gvYS3/CGP6Ce6.Nzp6OWGNMknUqwK5iSzpCGjKAOyzRHy', // 123
            ]);
            User::factory()
                ->count(3)
                ->create();
        }catch(\Exception $e){
            echo $e->getMessage();
        }
        //
        
    }
}
