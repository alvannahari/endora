<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\VirtualAccounts;

// php artisan db:seed --class=VirtualAccSeeder
class VirtualAccSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            $users = User::all();

            foreach($users as $usr){
                $usr->generateVA();
            }
            
        }catch(\Exception $e){
            echo $e->getMessage();
        }
        //
        
    }
}
