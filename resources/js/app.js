window._ = require('lodash');
// window.$ = window.jQuery = require('jquery');
window.Pusher = require('pusher-js');

// require('bootstrap-sass');

import Echo from "laravel-echo";
window.Echo = new Echo({
    broadcaster: 'pusher',
    key: 'de45b1bcd6d3622f17a9',
    cluster: 'ap1',
    encrypted: true
});

let notifications = [];
let newNotification = [];

const NOTIFICATION_TYPES = {
    newOrder: 'App\\Notifications\\Admin\\NewOrder',
    invoicePaid: 'App\\Notifications\\Admin\\InvoicePaid'
};

$(document).ready(function() {
    // check if there's a logged in user
    if(Laravel.userId) {
        window.Echo.private(`App.Admin.${Laravel.userId}`)
        .notification((notification) => {
            // console.log(notification);
            $('#countNotifications').html(parseInt($('#countNotifications').html())+1);
            addNotifications(notification, "#notifications");
        });
        $.get('/notifications', function (data) {
            // $('#countNotifications').html(data.length);
            addNotifications(data, "#notifications");
            // console.log(data);
        });
    }
});

function addNotifications(newNotifications, target) {
    newNotification = _.concat(newNotification, newNotifications);
    notifications = _.concat(newNotifications,notifications);
    // show only last 5 notifications
    // notifications.slice(0, 5);
    showNotifications(notifications, target);
    // console.log(notifications);
    newNotification = [];
}

function showNotifications(notifications, target) {
    if(notifications.length) {
        let htmlElements = notifications.map(function (notification) {
            return makeNotification(notification);
        });
        $(target + 'Menu').html(htmlElements.join(''));
        $(target).addClass('has-notifications')
    } else {
        $(target + 'Menu').html('<a class="dropdown-item" href="javascript:void(0)">Tidak Ada Notifikasi Terbaru</a>');
        $(target).removeClass('has-notifications');
    }
}

// Make a single notification string
function makeNotification(notification) {
    let to = routeNotification(notification);
    let notificationText = makeNotificationText(notification);
    
    return '<a class="dropdown-item" href="' + to +'" style="border-bottom: 1px solid #bbb9b93d;">'+ notificationText +'</a>';
}

// get the notification route based on it's type
function routeNotification(notification) {
    // let to = '?read=' + notification.id;
    const paymentId = notification.data.payment.id;
    let to = `payment/${paymentId}`;
    // if(notification.type === NOTIFICATION_TYPES.newMagazine) {
    //     to = `magazine/${magazineId}` + to;
    // } else if(notification.type === NOTIFICATION_TYPES.newMagazineComment) {
    //     to = `magazine/${magazineId}` + to;
    // }
    return '/' + to;
}

// get the notification text based on it's type
function makeNotificationText(notification) {
    let description = '';
    const name = notification.data.username;
    const photo = notification.data.image;
    const created_at = $.timeago(notification.created_at);
    if(notification.type === NOTIFICATION_TYPES.newOrder) {
        description = 'Terdapat Pesanan Baru';
    } else if(notification.type === NOTIFICATION_TYPES.invoicePaid) {
        description = 'Pemesanan Customer Berhasil Dibayar.';
    };
    let text = `<div class="row">
        <div class="col-2">
            <img class="rounded-circle" src="`+photo+`" width="40px" height="40px">
        </div>
        <div class="col-10 pl-0">
            <b>`+ name +`</b>
            <br>
            <span>`+description+`</span>
            <br>
            <span style="float:right;right:0;font-size: 12px;"><i class="mdi mdi-checkbox-blank-circle" style="color: #70c3fd;font-size: 8px;"></i> `+created_at+`</span>
        </div>
    </div>`;
    return text;
}
