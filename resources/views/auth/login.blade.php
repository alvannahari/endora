<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="endoracare.com Web Admin">
  <link rel="shortcut icon" href="img/favicon.png">

  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/assets/images/favicon.png') }}">
    <title>ENDORA CARE</title>
    <!-- Custom CSS -->
    <link href="{{ asset('/dist/css/style.min.css') }}" rel="stylesheet">

    <!-- font icon -->
    <link href="{{ asset('/assets/libs/elegant-icons-style.css') }}" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="{{ asset('/assets/libs/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/libs/style-responsive.css') }}" rel="stylesheet" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-img3-body" style="width:100%;background-image: url('{{ asset('/assets/images/bg-login.png') }}')">

  <div class="container">
    <form class="login-form" method="post" action="{{ url('/login') }}">
      <div class="login-wrap">
        
        <!-- <p class="login-img"><i class="mdi mdi-lock-open-outline" style="font-size:80px"></i></p> -->
        <p class="login-img" style="margin-bottom: 30px"><img src="{{ asset('assets/images/endora_logo.png') }}" alt="LOGO" width="110" height="90"></p>
        @if (session('msg'))
          <center><b class="text-danger">{{ session('msg') }}</b></center>
        @endif
        <div class="input-group">
          @csrf
          <span class="input-group-addon" style="width:50px;"><i class="ti-user" style="position: absolute;margin-top: 10px;"></i></span>
          <input type="text" autocomplete="off" class="form-control" name="email" placeholder="Email" autofocus>
        </div>
        <div class="input-group">
          <span class="input-group-addon" style="width:50px;"><i class="ti-key" style="position: absolute;margin-top: 10px;"></i></span>
          <input type="password" class="form-control" name="password" placeholder="Password">
        </div>
        
        <button class="btn btn-primary btn-lg btn-block" type="submit" name="submit">Login</button>
        {{-- <a class="btn btn-primary btn-block btn-lg" href="{{ url('/dashboard') }}"> <font style="color:white"> Login </font></a> --}}
      </div>
    </form>
  </div>

</body>

</html>
