@extends('layout.main')

@section('title', 'Balance')

@section('content')
<div class="row">
    <div class="col-12 col-md-8">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="table-balance" class="table table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr style="line-height: 2.2">
                                <th>NO.</th>
                                <th>ID</th>
                                <th style="text-align: center">INCOME</th>
                                <th style="text-align: center">OUTCOME</th>
                                <th style="text-align: center">TYPE</th>
                                <th style="text-align: center">DATE</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4">
        <div class="card">
            <div class="card-body">
                <h4 class="font-light text-center">Total Income</h4>
                <h2 class="font-light text-center">Rp. {{ number_format($balance['total']) }} <span class="font-16 text-success font-medium">+{{ $balance['increase'] }}%</span></h2>
                <div class="m-t-30">
                    <div class="row text-center"> 
                        <div class="col-6 border-right">
                            <h4 class="m-b-0">Rp. {{ number_format($balance['xendit']) }}</h4>
                            <span class="font-14 text-muted">Xendit</span>
                        </div>
                        <div class="col-6">
                            <h4 class="m-b-0">Rp. {{ number_format($balance['bca-va']) }}</h4>
                            <span class="font-14 text-muted">BCAVA</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="font-light text-center">Account Xendit</h4>
                {{-- <h2 class="font-light text-center">Rp. 35,658 <span class="font-16 text-success font-medium">+23%</span></h2> --}}
                <div class="m-t-15">
                    <div class="row text-center">
                        <div class="col-6 border-right">
                            <div style="margin-top:-10px">
                                <i class="mdi mdi-cash-multiple" style="font-size: 36px;color: #137eff;"></i>
                            </div>
                            <h4 class="m-b-0" style="margin-top: -3px;color: #6392d2;">Rp. {{ number_format($xendit['balance']) }}</h4>
                            <span class="font-14 text-muted">Balance</span>
                        </div>
                        <div class="col-6">
                            <div style="margin-top:-10px">
                                <i class="mdi mdi-basket-fill" style="font-size: 36px;color: #ff6767;"></i>
                            </div>
                            <h4 class="m-b-0" style="margin-top: -3px;color: #f3875d;">Rp. {{ number_format($xendit['payout']) }}</h4>
                            <span class="font-14 text-muted">Payout</span>
                        </div>
                        <div class="col-12 mt-4" style="justify-content: center;">
                            <button type="button" class="btn btn-primary" style="width: 200px" data-toggle="modal" data-target="#modal-payout"><i class="mdi mdi-currency"></i> Payout Xendit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="card">
            <div class="card-title" style="padding-top: 1rem !important;padding-bottom: 5px !important;"> 
                <h4 class="font-light text-center">Withdraw Xendit</h4>
            </div>
            <div class="card-body pt-1">
                <form action="">
                    <div class="form-group">
                        <label>Account Number Bank</label>
                        <input type="number" class="form-control" name="account" placeholder="ex. 7599010048203762">
                    </div>
                    <div class="form-group">
                        <label>Amount Cash</label>
                        <input type="number" class="form-control" name="amount" placeholder="ex. 3500000">
                    </div>
                    <div class="row mt-4" style="justify-content: center;">
                        <button type="button" id="btn-withdraw" class="btn btn-primary" style="width: 200px" data-toggle="modal" data-target="#modal-withdraw"><i class="mdi mdi-currency"></i> Confirmation</button>
                    </div>
                </form>
            </div>
        </div> --}}
    </div>
</div>

<!-- Modal Payout-->
<div class="modal modal-primary fade" id="modal-payout" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true" style="top:25%;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Payout Account Xendit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form id="form-payout">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label>Email to send a secret password</label>
                        <input type="email" class="form-control" name="email"> 
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label>Amount</label>
                        <input type="number" class="form-control" name="outcome"> 
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label>Confirmation password admin (For Authorization)</label>
                        <input type="password" class="form-control" name="password"> 
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="btn-payout">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Get Payout-->
<div class="modal fade" id="modal-get-payout" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="color: #4c5865;">Information Payout</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-4">
                            <label>ID Xendit</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11" id="text-id-xendit"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4">
                            <label>ID External</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11" id="text-id-external"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4">
                            <label>Email</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11" id="text-email"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4">
                            <label>Amount</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11" id="text-amount"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4">
                            <label>Status</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11" id="text-status"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4">
                            <label>Expire Time</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11" id="text-expire-time"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4">
                            <label>Created At</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11" id="text-created"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4">
                            <label>URL Payout</label>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11" id="text-url-payout"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>

$(document).ready(function () {
    // $.fn.dataTable.ext.classes.sPageButton = 'button primary_button';
    let t = $('#table-balance').DataTable( {
        "scrollY": 650,
        "paging":   false,
        "responsive": true,
        "processing": true,
        // "serverSide": true,
        "ajax": "{!! url()->current() !!}",
        "columns": [
            {
                "data": null,
                "width": "10px",
                "sClass": "text-center",
                "bSortable": false
            },
            { "data": "null", render: function ( data, type, row ) {
                    if (row.payment)
                        return row.payment_id;
                    else
                        return '<a href="#" type="button" id="btn-get-payout">'+row.id+'</a>';
                }
            },
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    if (!row.payment)
                        return '-';
                    else if (row.payment.type == 'bca-va')
                        return '<span class="mr-2">Rp.</span>'+number_format(row.payment.total, 0);
                    else
                        return '<span class="mr-2">Rp.</span>'+number_format(row.income, 0);
                }
            },
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    if (!row.payment)
                        return '<span class="mr-2">Rp.</span>'+number_format(row.outcome, 0);
                    else 
                        return '-';
                }
            },
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    if (row.payment && row.payment.type)
                    return row.payment.type;
                    else
                    return 'xendit';
                }
            },
            { "data": "created_at", "sClass": "text-center"},
        ],
        'drawCallback': function () {
            $('#table-balance tbody tr' ).css('height', '55px');
        },
        'initComplete': function(){
            $("#table-balance_filter").parent().prev().addClass('col-4 mb-1').removeClass('col-sm-12')
            $("#table-balance_filter").parent().addClass('col-8').removeClass('col-sm-12')
            $("#table-balance_filter").parent().prev().html(`
                <form action="{{ route('balance.exportReport') }}" method="POST">
                    @csrf
                    <button class="btn btn-success">
                        <i class="mdi mdi-printer mr-1"></i> Print Report
                    </button>
                </form>
            `);
                // <button id="btn-print" class="btn btn-sm btn-success">
                //     <i class="mdi mdi-printer mr-1"></i> Print Report
                // </button>
        }   
    });
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

});

$(document).on('click', '#btn-print', function(e){
    e.preventDefault();
    alert('Feature is Still Under Repair');
});

function number_format(number) {
    decimals = 0;
    dec_point = '.';
    thousands_point = ',';

    number = parseFloat(number).toFixed(decimals);

    number = number.replace(".", dec_point);

    var splitNum = number.split(dec_point);
    splitNum[0] = splitNum[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_point);
    number = splitNum.join(dec_point);

    return number;
}

$("#form-payout").submit(function(e) {
    e.preventDefault();
    clearError();
    let btn = $('#btn-payout');

    $.ajax({
        type: "post",
        url: "{{ route('balance.payout') }}",
        data: $('#form-payout').serialize(),
        beforeSend : function () {
            btn.html('confirm...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            console.log(response);
            if (!response.error) {
                let newTabPayout = window.open();
                newTabPayout.location = response.data.payout_url;
                newTabPayout.focus();
                // window.open(response.data.payout_url, '_blank').focus();
                // window.setTimeout( location.reload(), 10000 );
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                btn.html('submit');
                btn.prop('disabled', false);
            }
        },
        error: function (response) {
            console.log(response);
            btn.html('submit');
            btn.prop('disabled', false);
        }
    });
});

$(document).on('click', '#btn-get-payout', function(e){
    e.preventDefault();
    let id = $(this).html();
    let url = "{{ route('balance.getPayout', ':id') }}";
    let new_url = url.replace(':id', id);

    alert(id);

    // $.ajax({
    //     type: "get",
    //     url: newurl,
    //     dataType: "json",
    //     success: function (response) {
    //         console.log(response)
    //     },
    //     error : function (response) {
    //         console.log(response);
    //     }
    // });

    // $('#modal-get-payout').modal('show');
});

</script>
@endsection