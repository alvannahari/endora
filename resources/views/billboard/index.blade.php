@extends('layout.main')

@section('title', 'List Billboard')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-10">
                        <!-- <h4 class="card-title">Modul</h4> -->
                        <h6 class="card-subtitle m-t-5 m-b-20">Halaman yang berisi daftar seluruh billboard yang akan ditampilkan pada aplikasi mobile Endora. Data billboard yang diinputkan oleh admin dengan beberapa kategori, offer, dan juga brand.</h6>
                    </div>
                    <div class="col-12 col-md-2">
                        <a href="{{ url('billboard/create') }}" class="btn btn-primary m-b-10" style="float: right;"><i class="mdi mdi-plus"></i> Add New Billboard</a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="table-offer" class="table table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr style="line-height: 2.2">
                                <th>NO.</th>
                                <th style="text-align: center">BIG IMAGE</th>
                                <th>NAME</th>
                                <th style="text-align: center">SMALL IMAGE</th>
                                <th style="text-align: center">IMPORTANT</th>
                                <th style="text-align: center">IS_ACTIVE</th>
                                <th style="text-align: center">CREATED</th>
                                <th></th> 
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-danger fade" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this billboard ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <form action="{{ route('billboard.delete', 1) }}" method="POST" id="form-delete">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-danger" id="btn-delete">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-warning fade" tabindex="-1" role="dialog" id="modal-state">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="text-state">Are you sure to activate this billboard ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <form action="{{ route('billboard.state', 1) }}" method="POST" id="form-state">
                    @csrf
                    <button type="submit" class="btn btn-warning" id="btn-state">Activate</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>

$(document).ready(function () {
    // $.fn.dataTable.ext.classes.sPageButton = 'button primary_button';
    let t = $('#table-offer').DataTable( {
        "pageLength": 50,
        "processing": true,
        // "serverSide": true,
        "ajax": "{!! url()->current() !!}",
        "columns": [
            {
                "data": null,
                "width": "10px",
                "sClass": "text-center",
                "bSortable": false
            },
            { "data": "null","bSortable": false, "sClass": "text-center", render: function ( data, type, row ) {
                    return '<a href="#" type="button" data-toggle="modal"><img src="'+row.cover_big+'" width="130" height="90"><a/>';
                }
            },
            { "data": "name"},
            { "data": "null","bSortable": false, "sClass": "text-center", render: function ( data, type, row ) {
                    return '<a href="#" type="button" data-toggle="modal"><img src="'+row.cover_small+'" width="200" height="40"><a/>';
                }
            },
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    if (row.is_important == '1')
                    return '<label class="label label-info">YES</label>';
                    else 
                    return '<label class="label label-purple">NO</label>';
                }
            },
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    if (row.is_active == '1')
                    return '<label class="label label-megna">Active</label>';
                    else 
                    return '<label class="label label-warning">Disable</label>';
                }
            },
            { "data": "created_at", "sClass": "text-center" },
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    let text = 'Deactivate';
                    if (row.is_active == '0') text = 'Activate';
                    return `
                        <button class="btn btn-sm btn-warning m-l-5" title="delete" onclick="stateBillboard(`+row.id+`,`+row.is_active+`)"><i class="mdi mdi-update"></i> `+text+`</button>
                        <button class="btn btn-sm btn-danger m-l-5" title="delete" onclick="deleteBillboard(`+row.id+`)"><i class="mdi mdi-delete"></i> Delete</button>
                        `;
                },"bSortable": false
            }
        ],
        // "createdRow": function (row, data, index) {
            // $('td', row).eq(1).addClass('truncated');
            // $('td', row).eq(2).css('width', '50px');
            // $('td', row).eq(3).css('width', '40px');
            // $('td', row).eq(6).css('width', '80px');
            // $('td', row).eq(5).css('width', '150px');
        // },
    });
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
});

function deleteBillboard(id) {
    let url = "{{ route('billboard.delete', ':id') }}";
    $('#form-delete').attr('action', url.replace(':id', id));
    $('#modal-delete').modal('show');
}

function stateBillboard(id, state) {
    let url = "{{ route('billboard.state', ':id') }}";
    $('#form-state').attr('action', url.replace(':id', id));
    if (state == 1 ) {
        $('#text-state').html('Are you sure to deactivate this billboard ?') ;
        $('#btn-state').html('Deactivate') ;
    }
    else {
        $('#text-state').html('Are you sure to activate this billboard ?') ;
        $('#btn-state').html('Activate') ;
    }
    $('#modal-state').modal('show');
}

</script>
@endsection