@extends('layout.main')

@section('title', 'Detail Brand')

@section('content')
<div class="row">
    <div class="col-12 col-md-4">
        <div class="card">
            <div class="card-title" style="padding-top: 1rem !important;padding-bottom: 7px !important;"> 
                <div class="row">
                    <div class="col-6">
                        <span style="font-size: 18px;" >Info </span>
                    </div>
                    <div class="col-6">
                        <a href="#" type="button" style="float: right;font-size:26px;margin-top: -5px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" > <i class="mdi mdi-chevron-down"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-update-info" ><i class="mdi mdi-pen mr-2"></i> Update Info</a>
                            <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-update-logo" ><i class="mdi mdi-violin mr-2"></i> Update Logo</a>
                            <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-update-cover"><i class="mdi mdi-image mr-2"></i> Update Cover</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body pt-0" style="text-align: center">
                <div style="border-bottom: 1px solid #f7f7f7;padding-bottom: 25px;background-color: #f7f7f794;">
                    <img src="{{ $brand['image'] }}" alt="" srcset="" style="width: 100%;height: auto;">
                    <img class="rounded-circle m-3" src="{{ $brand['logo'] }}" alt="" srcset="" width="90" height="90" style="margin-top: -45px !important;">
                    <h2 class="mb-0">{{ $brand['name'] }}</h2>
                    <span>{{ $brand['items_count'] }} Product</span>
                </div>
                <div class="pt-3">
                    <p>
                        {{ $brand['description'] }}
                    </p>
                    <div>
                        <span title="created at"><i class="mdi mdi-timer"></i> {{ $brand['created_at'] }}</span>
                        <span class="mx-3">|</span>
                        <span title="updated at"><i class="mdi mdi-update"></i> {{ $brand['updated_at'] }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-8">
        <div class="card">
            <div class="card-title py-3"> 
                <span style="font-size: 18px;" >{{ $brand['items_count'] }} Products </span>
            </div>
            <div class="card-body pt-0" style="overflow: scroll;height: 900px;">
                <div class="row">
                    @forelse ($brand['items'] as $item)
                        <div class="col-6 col-md-3 mb-3">
                            <div style="box-shadow: 1px 1px 4px 0px #d2cdcd">
                                @if (count($item['images']) > 0)
                                    <img src="{{ $item['images'][0]['name'] }}" alt="" style="width: 100%">
                                @else
                                    <img src="{{ asset('storage/item/not_found.png') }}" alt="" style="width: 100%">
                                @endif  
                                <div class="m-2 pb-3">
                                    <a href="{{ url('product').'/'.$item['id'] }}" style="color:#6d6d6d;"><h6>{{ $item['name'] }}</h6> </a>
                                    <span style="color: green">Rp. {{ $item['price'] }}</span>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="col-12 text-center">
                            <h5>No Items or Products for This Brand</h5>
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Update Info -->
<div class="modal modal-primary fade" id="modal-update-info" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Info Brand</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form id="form-brand">
                    @csrf
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" placeholder="input name brand" class="form-control" name="name" value="{{ $brand['name'] }}">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" name="description" cols="20" rows="10" placeholder="description of new brand">{{ $brand['description'] }}</textarea>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label>Validate State</label>
                        <div class="row">
                            <div class="col-6">
                                <input type="radio" name="is_validate" value="0" id="not-validate" {{ $brand['is_validate'] == '0' ? 'checked' : '' }}>
                                <label class="ml-2 w-75" for="not-validate">Not Validated</label>
                            </div>
                            <div class="col-6">
                                <input type="radio" name="is_validate" value="1" id="validate" {{ $brand['is_validate'] == '1' ? 'checked' : '' }}>
                                <label class="ml-2 w-75" for="validate">Validated</label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-update-info">Save</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Update Logo -->
<div class="modal modal-warning fade" id="modal-update-logo" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Logo Brand</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form id="form-update-logo">
                    @csrf
                    <div class="form-group">
                        <label>Logo</label>
                        <input type="file" class="form-control" name="logo">
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-warning" id="btn-update-logo">Save</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Update Cover -->
<div class="modal modal-warning fade" id="modal-update-cover" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Cover Brand</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form id="form-update-cover">
                    @csrf
                    <div class="form-group">
                        <label>Cover</label>
                        <input type="file" class="form-control" name="image">
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-warning" id="btn-update-cover">Save</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#btn-update-info').click(function (e) { 
        e.preventDefault();
        clearError();
        let btn = $(this);

        $.ajax({
            type: "post",
            url: "{{ url('brand').'/'.$brand['id'].'/update' }}",
            data: $('#form-brand').serialize(),
            dataType: "json",
            beforeSend: function() {
                btn.html('Saving...');
                btn.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    for (let key of Object.keys(response.error)) {
                        $('[name="'+key+'"]').addClass('is-invalid');
                        $('[name="'+key+'"]').next().html(response.error[key]);
                    }
                    alert('Something wrong !!!');
                    btn.html('Save');
                    btn.prop('disabled', false);
                }
            }, 
            error: function (){
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        });
    });

    $('#btn-update-logo').click(function (e) { 
        e.preventDefault();
        clearError();
        let btn = $(this);
        let formData = new FormData($('#form-update-logo')[0]);

        $.ajax({
            type: "post",
            url: "{{ url('brand').'/'.$brand['id'].'/updateLogo' }}",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend: function() {
                btn.html('Saving...');
                btn.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    for (let key of Object.keys(response.error)) {
                        $('[name="'+key+'"]').addClass('is-invalid');
                        $('[name="'+key+'"]').next().html(response.error[key]);
                    }
                    alert('Something wrong !!!');
                    btn.html('Save');
                    btn.prop('disabled', false);
                }
            }, 
            error: function (){
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        });
    });

    $('#btn-update-cover').click(function (e) { 
        e.preventDefault();
        clearError();
        let btn = $(this);
        let formData = new FormData($('#form-update-cover')[0]);

        $.ajax({
            type: "post",
            url: "{{ url('brand').'/'.$brand['id'].'/updateCover' }}",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend: function() {
                btn.html('Saving...');
                btn.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    for (let key of Object.keys(response.error)) {
                        $('[name="'+key+'"]').addClass('is-invalid');
                        $('[name="'+key+'"]').next().html(response.error[key]);
                    }
                    alert('Something wrong !!!');
                    btn.html('Save');
                    btn.prop('disabled', false);
                }
            }, 
            error: function (){
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        });
    });
</script>
@endsection