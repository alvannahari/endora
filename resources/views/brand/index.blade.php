@extends('layout.main')

@section('title', 'List Brand')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <button class="btn btn-primary mb-4 mr-2" data-toggle="modal" data-target="#modal-add-brand"><i class="mdi mdi-plus"></i> Add New Brand</button>
                <button class="btn btn-success mb-4 mr-2" data-toggle="modal" data-target="#modal-import"><i class="mdi mdi-file-import"></i> Import File Brand </button>
                <form action="{{ route('brand.export') }}" method="POST" style="display: inline-block">
                    @csrf
                    <button type="submit" class="btn btn-warning mb-4"><i class="mdi mdi-file-export"></i> Export File Brand </button>
                </form>
                {{-- <div class="row">
                    <div class="col-12 col-md-10">
                        <!-- <h4 class="card-title">Modul</h4> -->
                        <h6 class="card-subtitle m-t-5 m-b-20">Halaman yang berisi daftar seluruh produk/item yang akan ditampilkan untuk seluruh pengguna mobile yang terdaftar. Data produk/item yang diinputkan oleh admin dengan beberapa deskripsi yang harus dilengkapi</h6>
                    </div>
                    <div class="col-12 col-md-2">
                        <button class="btn btn-primary m-b-10" style="float: right;" data-toggle="modal" data-target="#modal-add-brand"><i class="mdi mdi-plus"></i> Add New Brand</button>
                    </div>
                </div> --}}
                <div class="table-responsive">
                    <table id="table-brand" class="table table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr style="line-height: 2.2">
                                <th>NO.</th>
                                <th style="text-align: center">LOGO</th>
                                <th>NAME</th>
                                <th style="text-align: center">ITEMS</th>
                                <th style="text-align: center">IS_VALIDATE</th>
                                <th style="text-align: center">CREATED</th>
                                <th></th> 
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Add Brand -->
<div class="modal modal-primary fade" id="modal-add-brand" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New Brand</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form id="form-brand">
                    @csrf
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" placeholder="input name brand" class="form-control" name="name">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" name="description" cols="20" rows="10" placeholder="description of new brand"></textarea>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label>Validate State</label>
                        <div class="row">
                            <div class="col-6">
                                <input type="radio" name="is_validate" value="0" id="not-validate" checked>
                                <label class="ml-2 w-75" for="not-validate">Not Validated</label>
                            </div>
                            <div class="col-6">
                                <input type="radio" name="is_validate" value="1" id="validate">
                                <label class="ml-2 w-75" for="validate">Validated</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Logo <small>( Size:480 x 480 . Max:1mb )</small></label>
                        <input class="form-control" type="file" name="logo">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label>Cover <small>( Size:640 x 320 . Max:1mb )</small></label>
                        <input class="form-control" type="file" name="image">
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-add-brand">Save</button>
            </div>
        </div>
    </div>
</div>

{{-- Modal Delete --}}
<div class="modal modal-danger fade" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this brand ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <form action="{{ route('brand.delete', 1) }}" method="POST" id="form-delete">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-danger" id="btn-delete">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Import Brand-->
<div class="modal modal-success fade" id="modal-import" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                <div class="modal-header">
                        <h5 class="modal-title">Import Data Brand</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                    </div>
            <div class="modal-body">
                <form id="form-import">
                    @csrf
                    <div class="form-group">
                        <label>Choose file excel</label>
                        <input type="file" class="form-control" name="file">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-import">Import</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#exampleModal').on('show.bs.modal', event => {
        var button = $(event.relatedTarget);
        var modal = $(this);
        // Use above variables to manipulate the DOM
        
    });
</script>

<script>
$(document).ready(function () {
    let t = $('#table-brand').DataTable( {
        "pageLength": 25,
        "processing": true,
        // "serverSide": true,
        "ajax": "{!! url()->current() !!}",
        "columns": [
            {
                "data": null,
                "width": "10px",
                "sClass": "text-center",
                "bSortable": false
            },
            { "data": "null","bSortable": false, "sClass": "text-center", render: function ( data, type, row ) {
                    return '<a href="#" type="button" data-toggle="modal"><img class="rounded-circle" src="'+row.logo+'" width="70" height="70"><a/>';
                }
            },
            { "data": "name"},
            { "data": "items_count", "sClass": "text-center"},
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    if (row.is_validate == '1')
                    return '<label class="label label-info">Validate</label>';
                    else 
                    return '<label class="label label-purple">Not Validate</label>';
                }
            },
            { "data": "created_at", "sClass": "text-center" },
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    return `
                        <a href="{{ url('brand') }}/`+row.id+`" class="btn btn-sm btn-success"><i class="mdi mdi-magnify"></i> Detail</a>
                        <button class="btn btn-sm btn-danger m-l-5" title="delete" onclick="deleteBrand(`+row.id+`)"><i class="mdi mdi-delete"></i> Delete</button>
                        `;
                },"bSortable": false
            }
        ],
        "createdRow": function (row, data, index) {
            // $('td', row).eq(1).addClass('truncated');
            // $('td', row).eq(2).css('width', '50px');
            // $('td', row).eq(3).css('width', '40px');
            $('td', row).eq(6).css('width', '170px');
            // $('td', row).eq(5).css('width', '150px');
        },
    });
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
});

$('#btn-add-brand').click(function (e) { 
    e.preventDefault();
    clearError();
    let btn = $(this);
    let formData = new FormData($('#form-brand')[0]);

    $.ajax({
        type: "post",
        url: "{{ url('brand') }}",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        }, 
        error: function (){
            alert('Something wrong !!!');
            btn.html('Save');
            btn.prop('disabled', false);
        }
    });
});

function deleteBrand(id) {
    let url = "{{ route('brand.delete', ':id') }}";
    $('#form-delete').attr('action', url.replace(':id', id));
    $('#modal-delete').modal('show');
}

$('#btn-import').click(function (e) { 
    e.preventDefault();
    clearError();
    let btn = $(this);
    let formData = new FormData($('#form-import')[0]);

    $.ajax({
        type: "post",
        url: "{{ route('brand.import') }}",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            // console.log(response);
            if (!response.error) {
                location.reload();
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('Import');
                btn.prop('disabled', false);
            }
        }, 
        error: function (){
            alert('Something wrong !!!');
            btn.html('Import');
            btn.prop('disabled', false);
        }
    });
});

</script>
@endsection