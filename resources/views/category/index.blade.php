@extends('layout.main')

@section('title', 'List Categories')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-10">
                        <!-- <h4 class="card-title">Modul</h4> -->
                        <h6 class="card-subtitle m-t-5 m-b-20">Halaman yang berisi daftar seluruh Kategori yang Tersedia di Endora-App mobile. Data yang Ditampilkan Berisi data Kategori dan Upper Category Masing-masing Serta Banyak Jumlah Sub Kategori yang Dimiliki</h6>
                    </div>
                    <div class="col-12 col-md-2">
                        <button class="btn btn-primary dropdown-toggle m-b-15" style="float: right" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Add New Category
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-main-category"><i class="mdi mdi-plus"></i> Main Category</a>
                            <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-sub-category-1"><i class="mdi mdi-plus"></i> Sub Category 1</a>
                            <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-sub-category-2"><i class="mdi mdi-plus"></i> Sub Category 2</a>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="table-category" class="table table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr style="line-height: 2.2">
                                <th>NO.</th>
                                <th>IMAGE</th>
                                <th>CATEGORY</th>
                                <th>UPPER CATEGORY</th>
                                <th style="text-align: center">TYPE</th>
                                <th style="text-align: center">COUNT SUB</th>
                                <th style="text-align: center">CREATED</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Main Category -->
<div class="modal modal-primary fade" id="modal-main-category" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New Main Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form id="form-main-category">
                    @csrf
                    <div class="form-group">
                        <label>Name Main Category</label>
                        <input type="text" class="form-control" name="name" placeholder="input the new name category">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group temp-promo">
                        <label>Name Main Category</label>
                        <select name="type" class="form-control">
                            <option>label</option>
                            <option>promo</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                        <input type="file" class="form-control" name="image">
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-add-main-category">Save</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Sub Category 1 -->
<div class="modal modal-primary fade" id="modal-sub-category-1" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New Sub Category 1</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form id="form-sub-category1">
                    @csrf
                    <div class="form-group">
                        <label>Main Category</label>
                        <select class="form-control" name="upper_category">
                            @forelse ($categories as $category)
                                <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                            @empty
                                <option>Please Add the Main Category First</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Sub Category 1</label>
                        <input type="text" class="form-control" name="name" placeholder="input the new name sub-category">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                        <input type="file" class="form-control" name="image">
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-add-sub-category1">Save</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Sub Category 2-->
<div class="modal modal-primary fade" id="modal-sub-category-2" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New Sub Category 2</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form id="form-sub-category2">
                    @csrf
                    <div class="form-group">
                        <label>Main Category</label>
                        <select class="form-control" name="main_category" id="select-main-category">
                            @forelse ($categories as $category)
                                <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                            @empty
                                <option>Please Add the Main Category First</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Sub-Category 1</label>
                        <select class="form-control" name="upper_category" id="select-sub-category1">
                            <option>Please Select Previous Category</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Sub Category 2</label>
                        <input type="text" class="form-control" name="name" placeholder="input the new name sub-category">
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                        <input type="file" class="form-control" name="image">
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-add-sub-category2">Save</button>
            </div>
        </div>
    </div>
</div>

{{-- Modal Delete Category --}}
<div class="modal modal-danger fade" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this category ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" id="btn-delete">Ok</button>
            </div>
        </div>
    </div>
</div>

<script>
let category_id = 0;
let categories = @json($categories);

$(document).ready(function () {
    let t = $('#table-category').DataTable( {
        "pageLength": 50,
        "processing": true,
        // "serverSide": true,
        "ajax": "{!! url()->current() !!}",
        "order": [[ 3, "asc" ]],
        "columns": [
            {
                "data": null,
                "width": "10px",
                "sClass": "text-center",
                "bSortable": false
            },
            { "data": "null","bSortable": false, render: function ( data, type, row ) {
                    return '<img class="rounded-circle" src="'+row.image+'" width="50" height="50">';
                }
            },
            { "data": "name"},
            { "data": "null", render: function ( data, type, row ) {
                    if (row.categories.length > 0) return row.categories[0].name;
                    else return '<div style="width:100%;font-weight:bold;"><span>-</span></div>';
                }
            },
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    if (row.type == 'label')
                    return '<label class="label label-megna">Label</label>';
                    else 
                    return '<label class="label label-warning">Promo</label>';
                }
            },
            { "data": "sub_categories_count", "sClass": "text-center" },
            { "data": "created_at", "sClass": "text-center" },
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    return `
                        <button class="btn btn-sm btn-danger m-l-5" title="delete" onclick="deleteCategory(`+row.id+`)"><i class="mdi mdi-delete"></i> Delete</button>
                        `;
                    // return `
                    //     <a href="{{ url('category') }}/`+row.id+`" class="btn btn-sm btn-success"><i class="mdi mdi-magnify"></i></a>
                    //     <button class="btn btn-sm btn-danger m-l-5" title="delete" onclick="deleteCategory(`+row.id+`)"><i class="mdi mdi-delete"></i> </button>
                    //     `;
                },"bSortable": false
            }
        ]
    });
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
});

$('[name="type"]').change(function (e) { 
    e.preventDefault();
    if ($(this).val() == 'label') {
        $('.promo').fadeOut(300, function(){ 
            $(this).remove();
        });
    }
    if ($(this).val() == 'promo') {
        $('.temp-promo').after(`
            <div class="form-group promo">
                <label>Discount</label>
                <input type="number" class="form-control" name="discount" placeholder="ex. 10%">
                <span class="help-block"></span>
            </div>
            <div class="form-group promo">
                <label>Discount Maximum</label>
                <input type="number" class="form-control" name="maximum" placeholder="ex. 25000">
                <span class="help-block"></span>
            </div>
        `).nextAll().slice(0, 2).hide().fadeIn(700);
    }
});

$('#btn-add-main-category').click(function (e) { 
    e.preventDefault();
    clearError();
    let btn = $(this);
    let formData = new FormData($('#form-main-category')[0]);

    $.ajax({
        type: "post",
        url: "{{ url('category/main') }}",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        }, 
        error: function (){
            alert('Something wrong !!!');
            btn.html('Save');
            btn.prop('disabled', false);
        }
    });
});

$('#btn-add-sub-category1').click(function (e) { 
    e.preventDefault();
    clearError();
    let btn = $(this);
    let formData = new FormData($('#form-sub-category1')[0]);

    $.ajax({
        type: "post",
        url: "{{ url('category/submain') }}",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            // console.log(response.error);
            if (!response.error) {
                location.reload();
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        }, 
        error: function (){
            alert('Something wrong !!!');
            btn.html('Save');
            btn.prop('disabled', false);
        }
    });
});

$('#select-main-category').change(function (e) { 
    e.preventDefault();
    // let val = $(this).val();
    if (this.value == 0) return false;
    let id = this.options[this.selectedIndex].value;
    let category1 = '';
    $.each(categories, function (indexInArray, valueOfElement) { 
        if (valueOfElement.id == id) {
            category1 = categories[indexInArray];
            return false;
        }
    });
    let optionCategory1 = '';
    let elSubCategory1 = $('#select-sub-category1');
    elSubCategory1.removeClass('is-invalid');
    if (category1.sub_categories.length > 0) {
        optionCategory1 = '<option value="0">Please Select Category</option>';
        $.each(category1.sub_categories, function (indexInArray, valueOfElement) { 
            optionCategory1 += '<option value="'+valueOfElement.id+'">'+valueOfElement.name+'</option>'
        });
    } else {
        optionCategory1 = '<option value="0">Sub-Category Not Found!</option>';
        elSubCategory1.addClass('is-invalid');
    }
    elSubCategory1.html(optionCategory1).hide().fadeIn(700);
});

$('#btn-add-sub-category2').click(function (e) { 
    e.preventDefault();
    clearError();
    let val = $('#select-sub-category1').val();
    if (val == '0') {
        alert('First Sub-Category is Empty!!!');
        return false;
    }

    let btn = $(this);
    let formData = new FormData($('#form-sub-category2')[0]);

    $.ajax({
        type: "post",
        url: "{{ url('category/submain') }}",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        }, 
        error: function (){
            alert('Something wrong !!!');
            btn.html('Save');
            btn.prop('disabled', false);
        }
    });
});

function deleteCategory(id) {
    category_id = id;
    $('#modal-delete').modal('show');
}

$('#btn-delete').click(function (e) { 
    e.preventDefault();
    let btn = $(this);
    $.ajax({
        type: "get",
        url: "{{ url('category').'/' }}"+category_id+"/delete",
        data: {
            "_token": "{{ csrf_token() }}",
        },
        dataType: "json",
        beforeSend:function () {
            btn.html('Deleting...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                alert(response.error);
                btn.html('Ok');
                btn.prop('disabled', false);
            }
        }, 
        error : function (reponse) {
            alert(response.error);
            btn.html('Ok');
            btn.prop('disabled', false);
        }
    });
});

</script>
@endsection