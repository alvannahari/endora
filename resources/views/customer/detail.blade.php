@extends('layout.main')

@section('title', 'Detail Customer')

@section('content')
<div class="row">
    <div class="col-12 col-md-4">
        <div class="card">
            <div class="card-title" style="padding-top: 1rem !important;padding-bottom: 7px !important;"> 
                <div class="row">
                    <div class="col-6">
                        <span style="font-size: 18px;" >Photo </span>
                    </div>
                    <div class="col-6">
                        <a href="#" type="button" style="float: right;font-size:26px;margin-top: -5px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" > <i class="mdi mdi-chevron-down"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-update-image" ><i class="mdi mdi-pen mr-2"></i> Update Photo</a>
                            <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-remove-image"><i class="mdi mdi-delete mr-2"></i> Remove Photo</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body pt-0" style="text-align: center">
                <img class="rounded-circle m-3" src="{{ $user['image'] }}" alt="" srcset="" width="250" height="250">
            </div>
        </div>
    </div>
    <div class="col-12 col-md-8">
        <div class="card" style="height: 370px;">
            <div class="card-title" style="padding-top: 1rem !important;padding-bottom: 7px !important;"> 
                <div class="row">
                    <div class="col-4">
                        <span style="font-size: 18px;" >Info </span>
                    </div>
                    <div class="col-8">
                        <a href="#" type="button" style="float: right;font-size:26px;margin-top: -5px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" > <i class="mdi mdi-chevron-down"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-update-customer" ><i class="mdi mdi-pen mr-2"></i> Update User</a>
                            <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-delete-customer"><i class="mdi mdi-delete mr-2"></i> Delete User</a>
                        </div>
                        @if ($user['is_active'])
                        <button class="btn btn-sm btn-warning mr-3" data-toggle="modal" data-target="#modal-state-customer" style="float: right;width:120px"><i class="mdi mdi-block-helper mr-2"></i> Disable</button>
                        @else
                        <button class="btn btn-sm btn-warning mr-3" data-toggle="modal" data-target="#modal-state-customer" style="float: right;width:120px"><i class="mdi mdi-check mr-2"></i> Activate</button>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-3 col-md-2">
                            <label>Username</label>
                        </div>
                        <div class="col-9 col-md-10">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $user['username']}}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3 col-md-2">
                            <label>Email</label>
                        </div>
                        <div class="col-9 col-md-10">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $user['email']}}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3 col-md-2">
                            <label>Order Amount</label>
                        </div>
                        <div class="col-9 col-md-10">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $user['payments_count']}}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3 col-md-2">
                            <label>Created At</label>
                        </div>
                        <div class="col-9 col-md-10">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $user['created_at']}}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3 col-md-2">
                            <label>Updated At</label>
                        </div>
                        <div class="col-9 col-md-10">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $user['updated_at']}}</div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="row">
                    <div class="col-12">
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-title py-3"> 
        <div class="row">
            <div class="col-12">
                <span class="ml-2" style="font-size: 18px;" >Order History</span>
            </div>
        </div>
    </div>
    <div class="card-body pt-0">
        <table class=" table table-hover table-sm" width="100%" >
            <thead>
                <tr>
                    <th style="border-top: none;"><center>NO.</center></th>
                    <th style="border-top: none;">INVOICE NUMBER</th>
                    <th style="border-top: none;">WAYBILL NUMBER</th>
                    <th style="border-top: none;">TOTAL</th>
                    <th style="border-top: none;"><center>PAYMENT</center></th>
                    <th style="border-top: none;"><center>SHIPPING</center></th>
                    <th style="border-top: none;"><center>DATE</center></th>
                    <th style="border-top: none;"></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($user['payments'] as $payment)
                    <tr>
                        <td><center>{{ $loop->iteration }}</center></td>
                        <td>{{ $payment['invoice_number'] }}</td>
                        <td>{{ $payment['waybill_number'] }}</td>
                        <td>{{ $payment['total'] }}</td>
                        <td><center>
                            @if ($payment['status_payment'] == '1')
                                <label class="label label-rounded label-megna">Selesai</label>
                            @else
                                <label class="label label-rounded label-warning">Menunggu</label>
                            @endif
                        </center></td>
                        <td><center>{{ $payment['histories'][0]['status']['name'] ?? 'Menunggu Pembayaran' }}</center></td>
                        <td><center>{{ $payment['created_at'] }}</center></td>
                        <td>
                            <a href="{{ url('payment').'/'.$payment['id'] }}" type="button" class="btn btn-sm btn-primary m-l-5"><i class="mdi mdi-magnify"></i> Detail</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7"><center>the customer has never made an order transaction</center></td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>

<div class="modal modal-primary fade" id="modal-update-image" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Photo Profile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form id="form-update-image">
                    @csrf
                    <div class="form-group">
                        <label>Photo</label>
                        <input type="file" class="form-control" name="image" >
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-update-image">Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-danger fade" tabindex="-1" role="dialog" id="modal-remove-image">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this photo profile ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" id="btn-remove-image">Ok</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-warning fade" tabindex="-1" role="dialog" id="modal-state-customer">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to change state this user ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-warning" id="btn-state-customer">Ok</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-primary fade" tabindex="-1" role="dialog" id="modal-update-customer">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Info User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-update-customer">
                    @csrf
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" name="username" value="{{ $user['username'] }}">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" value="{{ $user['email'] }}">
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn-update-customer">Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-danger fade" tabindex="-1" role="dialog" id="modal-delete-customer">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this user ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" id="btn-delete-customer">Ok</button>
            </div>
        </div>
    </div>
</div>

<script>
$('#btn-update-image').click(function (e) { 
    e.preventDefault();
    clearError();
    var btn = $(this);
    let formData = new FormData($('#form-update-image')[0]);

    $.ajax({
        type: "post",
        url: "{{ url('customer').'/'.$user['id'].'/image' }}",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        }, 
        error: function (){
            alert('Something wrong !!!');
            btn.html('Save');
            btn.prop('disabled', false);
        }
    });
});

$('#btn-remove-image').click(function() {
    let btn = $(this);
    $.ajax({
        type: "get",
        url: "{{ url('customer').'/'.$user['id'].'/image' }}",
        data: {
            "_token": "{{ csrf_token() }}",
        },
        dataType: "json",
        beforeSend:function () {
            btn.html('Removing...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                alert(response.error);
                btn.html('Ok');
                btn.prop('disabled', false);
            }
        }
    });
});

$('#btn-state-customer').click(function (e) { 
    e.preventDefault();
    var btn = $(this);

    $.ajax({
        type: "get",
        url: "{{ url('customer').'/'.$user['id'].'/state' }}",
        beforeSend:function () {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                alert(response.error);
                btn.html('Ok');
                btn.prop('disabled', false);
            }
        }
    });
});

$('#btn-update-customer').click(function (e) { 
    e.preventDefault();
    clearError();
    let btn = $(this);

    $.ajax({
        type: "post",
        url: "{{ url('customer').'/'.$user['id'].'/update' }}",
        data: $('#form-update-customer').serialize(),
        beforeSend:function () {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        }, 
        error: function (){
            alert('Something wrong !!!');
            btn.html('Save');
            btn.prop('disabled', false);
        }
    });
});

$('#btn-delete-customer').click(function() {
    let btn = $(this);
    $.ajax({
        type: "get",
        url: "{{ url('customer').'/'.$user['id'].'/delete' }}",
        data: {
            "_token": "{{ csrf_token() }}",
        },
        dataType: "json",
        beforeSend:function () {
            btn.html('Deleting...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                alert(response.error);
                btn.html('Ok');
                btn.prop('disabled', false);
            }
        }
    });
});
</script>
@endsection