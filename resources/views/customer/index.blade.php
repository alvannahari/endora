@extends('layout.main')

@section('title', 'List Product')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-10">
                        <!-- <h4 class="card-title">Modul</h4> -->
                        <h6 class="card-subtitle m-t-5 m-b-20">Halaman yang berisi daftar seluruh customer Endora-App mobile yang telah terdaftar. Data yang ditampilkan berisi info pribadi customer dan juga data riwayat pemesanan produk </h6>
                    </div>
                    <div class="col-12 col-md-2">
                        <a class="btn btn-primary m-b-10" style="float: right;color:#fff" onclick="test()"><i class="mdi mdi-plus"></i> Add New User</a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="table-customer" class="table table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr style="line-height: 2.2">
                                <th>NO.</th>
                                <th style="text-align: center">PHOTO</th>
                                <th>USERNAME</th>
                                <th>EMAIL</th>
                                <th style="text-align: center">ORDER</th>
                                <th style="text-align: center">STATUS ACTIVE</th>
                                <th style="text-align: center">CREATED</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-danger fade" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this user ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" id="btn-delete">Ok</button>
            </div>
        </div>
    </div>
</div>

<script>
let userId = 0;

$(document).ready(function () {
    // $.fn.dataTable.ext.classes.sPageButton = 'button primary_button';
    let t = $('#table-customer').DataTable( {
        "processing": true,
        // "serverSide": true,
        "ajax": "{!! url()->current() !!}",
        "columns": [
            {
                "data": null,
                "width": "10px",
                "sClass": "text-center",
                "bSortable": false
            },
            { "data": "null","bSortable": false, "sClass": "text-center", render: function ( data, type, row ) {
                    return '<img class="rounded-circle" src="'+row.image+'" width="50" height="50">';
                }
            },
            { "data": "username"},
            { "data": "email"},
            { "data": "payments_count", "sClass": "text-center"},
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    if (row.is_active == '1')
                    return '<label class="label label-megna">Active</label>';
                    else 
                    return '<label class="label label-warning">Disable</label>';
                }
            },
            { "data": "created_at", "sClass": "text-center" },
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    return `
                        <a href="{{ url('customer') }}/`+row.id+`" class="btn btn-sm btn-success" ><i class="mdi mdi-magnify"></i></a>
                        <button class="btn btn-sm btn-danger m-l-5" title="delete" onclick="deleteCustomer(`+row.id+`)"><i class="mdi mdi-delete"></i> </button>
                        `;
                },"bSortable": false
            }
        ],
        // "createdRow": function (row, data, index) {
            // $('td', row).eq(1).addClass('truncated');
            // $('td', row).eq(2).css('width', '50px');
            // $('td', row).eq(3).css('width', '40px');
            // $('td', row).eq(6).css('width', '80px');
            // $('td', row).eq(5).css('width', '150px');
        // },
    });
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
});

function deleteCustomer(id) {
    userId = id;
    $('#modal-delete').modal('show');
}

$('#btn-delete').click(function() {
    let btn = $(this);
    $.ajax({
        type: "get",
        url: "{{ url('customer') }}/"+userId+"/delete",
        data: {
            "_token": "{{ csrf_token() }}",
        },
        dataType: "json",
        beforeSend:function () {
            btn.html('Deleting...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                alert(response.error);
                btn.html('Ok');
                btn.prop('disabled', false);
            }
        }
    });
});

function test() {
    alert('features are still under repair')
}

</script>

@endsection