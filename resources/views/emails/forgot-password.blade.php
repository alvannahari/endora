<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200;400;600;800&display=swap" rel="stylesheet">
    <style>
        * {
            padding: 0;
            margin: 0;
        }
        p {
            font-family: 'Nunito Sans', sans-serif;
        }
        header {
            height: 90px;
            background: #7e47ea;;
            display: flex;
            align-items: center;
            justify-content: center;
            color: #FFFFFFFF;
            font-family: 'Nunito Sans', sans-serif;
            font-weight: bold;
            width: 100%;
            text-align: center;
        }
        .container {
            width: 100%;
            padding: 0px 35px;
            box-sizing: border-box;
            text-align: center;
        }
        .logo {
            margin: 25px 0;
        }
        .content {
            text-align: left;
        }
        .salam,
        .info {
            margin-bottom: 20px;
        }
        .data-diri {
            margin-bottom: 50px;
        }
        .data-diri p {
            margin-bottom: 10px;
        }
        .thanks {
            margin-top: 40px;
        }
        .bold {
            font-weight: bold;
        }
        footer {
            background: #F4F4F4;
            text-align: center;
            color: #5A5A5A;
            padding: 15px;
            margin-top: 90px;
        }
    </style>
</head>
<body>
    <header>
        <div style="text-align: center;width:100%">
            <h1 style="margin-top: 20px;">PT. Endora Care </h1>
        </div>
    </header>
    <div class="container" style="margin-top:15px">
        <div class="content">
            <p class="info">You submitted a service <span class="bold">forgot password</span>. Please be careful to keep your account confidental :</p>
            <div class="data-diri">
                <p>Email	: {{ $user['email'] }}</p>
                <p>Password	: {{ $user['new_password'] }}</p>
            </div>
            <div class="thanks">
                <p>Thanks for choosing endora care - The Endora Care Team</p>
            </div>
        </div>
    </div>
    <footer>
        <p>© 2021 Endora Care.com.</p>
    </footer>
</body>
</html>