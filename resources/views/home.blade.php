@extends('layout/main')

@section('title', 'Dashboard')

@section('content')
<div class="row">
    <div class="col-md-6 col-xs-12">
        <div class="card">
            <div class="card-body" style="align-items: center">
                <span class="card-title pt-3" style="border-bottom: none"> 
                <span style="font-size: 18px;">Order Activity Customer </span>
                </span>
                <canvas class="mt-2" id="chart-user" style="width:100%" height="118">
            </div>
        </div>
    </div>
    <!-- column -->
    <div class="col-md-6 col-xs-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <center>
                            <span class="card-title" style="border-bottom: none"> 
                            <span style="font-size: 18px;">Status Pembayaran</span>
                            </span>
                        </center>
                        <canvas class="mt-2" id="chart-learning" width="250" height="200" style="display: inline-block;height: auto;width: inherit;"></canvas>
                    </div>
                    <div class="col-6" >
                        <center>
                            <span class="card-title" style="border-bottom: none"> 
                            <span style="font-size: 18px;">Status Pengiriman</span>
                            </span>
                        </center>
                        <canvas class="mt-2" id="chart-kognitif" width="250" height="200" style="display: inline-block;height: auto;width: inherit;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6 col-md-3">
        <div class="card">
            <div class="card-body text-center pt-2">
                <div class="card-icon" style="color:#00a000;">
                    <i class="mdi mdi-webhook"></i>
                    <span>1</span>
                </div>
                <h5 style="font-size:22px">{{ $items['today'] }}</h5>
                <h4 style="color: #9e9e9e;">Products Today</h4>
            </div>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="card">
            <div class="card-body text-center pt-2">
                <div class="card-icon" style="color: rgb(255 118 103);">
                    <i class="mdi mdi-webhook"></i>
                    <span>7</span>
                </div>
                <h5 style="font-size:22px">{{ $items['week'] }}</h5>
                <h4 style="color: #9e9e9e;">Products a Week</h4>
            </div>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="card">
            <div class="card-body text-center pt-2">
                <div class="card-icon" style="color: #f3c626;">
                    <i class="mdi mdi-webhook"></i>
                    <span>30</span>
                </div>
                <h5 style="font-size:22px">{{ $items['month'] }}</h5>
                <h4 style="color: #9e9e9e;">Products a Month</h4>
            </div>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="card">
            <div class="card-body text-center pt-2">
                <div class="card-icon" style="color: #389ce2;">
                    <i class="mdi mdi-webhook"></i>
                    <span>365</span>
                </div>
                <h5 style="font-size:22px">{{ $items['year'] }}</h5>
                <h4 style="color: #9e9e9e;">Products a Year</h4>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <!-- column -->
    <div class="col-12">
        <div class="card">
            <span class="card-title pt-3" style="border-bottom: none"> 
                <span style="font-size: 18px;">User Activity History</span>
            </span>
            <div class="col-12 px-4">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="border-top-0" style="width: 5px">NO.</th>
                                <th class="border-top-0" style="width: 50px"></th>
                                <th class="border-top-0">USERNAME</th>
                                <th class="border-top-0">EMAIL</th>
                                <th class="border-top-0"><center>PAYMENT</center></th>
                                <th class="border-top-0"><center>SHIPPING</center></th>
                                <th class="border-top-0" style="width: 170px"><center>ORDER AT</center></th>
                                <th class="border-top-0" style="width: 70px"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($histories as $history)
                            <tr>
                                <td><span class="font-medium"><center>{{ $loop->iteration }}</center></span></td>
                                <td><img class="rounded-circle" src="{{ $history['user']['image'] }}" width="50" height="50"></td>
                                <td>{{ $history['user']['username'] }}</td>
                                <td>{{ $history['user']['email'] }}</td>
                                <td><center>
                                    @if ($history['status_payment'] == '1')
                                        <label class="label label-rounded label-megna">Selesai</label>
                                    @elseif($history['status_payment'] == '0')
                                        <label class="label label-rounded label-warning">Menunggu</label>
                                    @else
                                        <label class="label label-rounded label-purple">Dibatalkan</label>
                                    @endif
                                </center></td>
                                <td><center>
                                    @if (count($history['histories']) > 0)
                                        {{ end($history['histories'])['status']['name'] }}
                                    @elseif ($history['status_payment'] == '1')
                                        Pengiriman Tertunda
                                    @else 
                                        Menunggu Pembayaran
                                    @endif
                                </center></td>
                                <td><center>{{ $history['created_at'] }}</center></td>
                                <td><center><a href="{{ url('payment').'/'.$history['id'] }}" class="btn btn-sm btn-primary"><i class="mdi mdi-magnify"></i> Detail</a></center></td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="4"><center>Order Activity Not Found</center></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('/assets/libs/chart/Chart.min.js') }}"></script>
<script src="{{ asset('/assets/libs/chart/dist/Chart.min.js') }}"></script>
<script src="{{ asset('/assets/libs/chart/dist/chartjs-plugin-datalabels@0.7.0') }}"></script>

<script>
    $(function() {
    var sum = function(a, b) { return a + b; };

    var ctxUser = $("#chart-user")[0].getContext("2d");
    var chartUser = new Chart(ctxUser, {
        type: 'line',
        data: {
            datasets: [{
                data: {!! json_encode($activity['value']) !!},
                backgroundColor: 'rgba(41, 182, 246,0.2)',
                borderColor: 'rgba(41, 182, 246, 1)',
                hoverBackgroundColor : 'white',
                borderWidth: 2,
                label : 'Total Order Amount'
            }],
            labels: {!! json_encode($activity['label']) !!}
        },
        options: {
            animation: {
                animateScale: true,
                animateRotate: true
            },
            responsive: true,
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                            return tooltipItem.yLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    var ctxLearning = $("#chart-learning")[0].getContext("2d");
    var chartLearning = new Chart(ctxLearning, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: {!! json_encode($state_payment) !!},
                backgroundColor: ['#f1c21a', '#1a98f1', '#ea2a16'],
                borderWidth: 1
            }],
            labels: ['Menunggu Pembayaran', 'Sudah Dibayar', 'Dibatalkan'],
        },
        options: {
            animation: {
                animateScale: true,
                animateRotate: true
            },
            responsive: false,
            // maintainAspectRatio: true,
            cutoutPercentage: 1,
            plugins: {
                datalabels: {
                    color: 'black',
                    formatter: function(value, context) {
                        return Math.round(value / context.chart.data.datasets[0].data.reduce(sum) * 100) + '%';
                    }
                }
            },
            legend: {
                display: true, 
                position:'bottom',
                labels: {
                    padding : 12,
                    fontColor: 'black',
                    fontSize : 9,
                    boxWidth: 20,
                    boxHeight: 2,
                },
            }
        }
    });

    var ctxKognitif = $("#chart-kognitif")[0].getContext("2d");
        var chartKognitif = new Chart(ctxKognitif, {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: {!! json_encode($state_shipment) !!},
                    backgroundColor: ['#f1c21a', '#eaa321', '#41e618', '#1a98f1', '#911ff9', '#e61919'],
                    borderWidth: 1
                }],
                labels: ['Dikemas', 'Dikirim', 'Diterima', 'Dikonfirmasi', 'Ditangguhkan'],
            },
            options: {
                plugins: {
                    datalabels: {
                        color: 'black',
                        formatter: function(value, context) {
                            return Math.round(value / context.chart.data.datasets[0].data.reduce(sum) * 100) + '%';
                        }
                    }
                },
                responsive: false,
                cutoutPercentage: 1,
                legend: {
                    display: true, 
                    position:'bottom',
                    labels: {
                        padding : 12,
                        fontColor: 'black',
                        fontSize : 9,
                        boxWidth: 20,
                        boxHeight: 2,
                    },
                }, 
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            },
        });
  });
</script>
@endsection