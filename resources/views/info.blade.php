@extends('layout.main')

@section('title', 'Info Shop')

@section('content')
<div class="row">
    <div class="col-12 col-md-4">
        <div class="card">
            <div class="card-title pt-3 pb-2"> 
                <div class="row">
                    <div class="col-6">
                        <span class="ml-2" style="font-size: 18px;" >Logo Store</span>
                    </div>
                    <div class="col-6">
                        <a href="#" type="button" style="float: right;font-size:26px;margin-top: -5px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" > <i class="mdi mdi-chevron-down"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-update-logo" ><i class="mdi mdi-pen mr-2"></i> Update Logo</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body pt-0 text-center">
                <img class="rounded-circle mx-3 my-4" src="{{ $data['logo'] }}" alt="" width="245" height="245">
            </div>
        </div>
    </div>
    <div class="col-12 col-md-8">
        <div class="card">
            <div class="card-body">
                <form id="form-update-info">
                    @csrf
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" value="{{ $data['name'] }}" name="name">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" class="form-control" value="{{ $data['address'] }}" name="address">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label>Province</label>
                        <input type="text" class="form-control" value="{{ $data['province'] }}" name="province">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label>City</label>
                        <input type="text" class="form-control" value="{{ $data['city'] }}" name="city">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label>Postal Code</label>
                        <input type="text" class="form-control" placeholder="32.736724" value="{{ $data['postal_code'] }}" name="postal_code">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label>Latitude</label>
                        <input type="text" class="form-control" placeholder="-21.842774" value="{{ $data['latitude'] }}" name="latitude">
                        <span class="help-block"></span>
                    </div>  
                    <div class="form-group">
                        <label>Longitude</label>
                        <input type="text" class="form-control" value="{{ $data['longitude'] }}" name="longitude">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="number" class="form-control" value="{{ $data['phone'] }}" name="phone">
                        <span class="help-block"></span>
                    </div>
                    <button class="btn btn-primary px-5 mt-2" id="btn-update-info"><i class="mdi mdi-timer"></i> Update</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal modal-primary fade" id="modal-update-logo" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Logo Store</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form id="form-update-logo">
                    @csrf
                    <div class="form-group">
                        <label>Image Logo</label>
                        <input type="file" class="form-control" name="logo">
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-update-logo">Save</button>
            </div>
        </div>
    </div>
</div>
<script>
$('#btn-update-info').click(function (e) { 
    e.preventDefault();
    clearError();
    let btn = $(this);

    $.ajax({
        type: "post",
        url: "{{ url('info/update')}}",
        data: $('#form-update-info').serialize(),
        beforeSend:function () {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        }, 
        error: function (){
            alert('Something wrong !!!');
            btn.html('Save');
            btn.prop('disabled', false);
        }
    });
});

$('#btn-update-logo').click(function (e) { 
    e.preventDefault();
    clearError();
    let btn = $(this);
    let formData = new FormData($('#form-update-logo')[0]);

    $.ajax({
        type: "post",
        url: "{{ url('info/logo') }}",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        }, 
        error: function (){
            alert('Something wrong !!!');
            btn.html('Save');
            btn.prop('disabled', false);
        }
    });
});
</script>
@endsection