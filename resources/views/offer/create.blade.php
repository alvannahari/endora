@extends('layout.main')

@section('title', 'New Offer')
    
@section('content')
    
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form class="form-horizontal m-t-10 pb-3" id="form-offer">
                    @csrf
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" placeholder="ex. Event Holiday Shop" name="name" style="max-width: 764px;">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label>Type Offer</label>
                        <select name="type" class="form-control" style="max-width: 764px;">
                            <option>Low</option>
                            <option>Medium</option>
                            <option>High</option>
                        </select>
                        <span class="help-block"></span>
                    </div>
                    <div class="row" style="width : 100%">
                        <div class="col-12 col-md-6 mb-3">
                            <label>Status Activate</label>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <input type="radio" name="is_activate" checked id="activate"> 
                                    <label for="activate" class="ml-2" style="margin-top: -2px;position: absolute;"> Activated </label>
                                </div>
                                <div class="col-6">
                                    <input type="radio" name="is_activate" id="no-activate">
                                    <label for="no-activate" class="ml-2" style="margin-top: -2px;position: absolute;"> Not Activated </label>
                                </div>
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="form-group">
                                <label>Start Offer</label>
                                <input type="date" class="form-control" name="start_offer">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="form-group">
                                <label>End Offer</label>
                                <input type="date" class="form-control" name="end_offer">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Image Cover <small>( Size:360 x 180 . Max:2mb )</small></label>
                        <input type="file" class="form-control" name="cover">
                        <span class="help-block"></span>
                    </div>
                    <div class="row" id="field-ctgry">
                        <div class="col-12">
                            <label>Content Category</label>
                            <a class="ml-2" href="#" style="color: #fa5838;float:right;" id="btn-remove-category"><i class="mdi mdi-minus"></i>Remove Category</a>
                            <a class="ml-2" href="#" style="float:right;" data-toggle="modal" data-target="#modal-add-category"><i class="mdi mdi-plus"></i>Add Category</a>
                        </div>
                        <div class="col-12" id="first-category">
                            <input type="hidden">
                            <label>- Silahkan Tambahkan Beberapa Kategori, Boleh Dikosongi !.</label>
                        </div>
                    </div>
                    <div class="row m-t-15" id="field-brand">
                        <div class="col-12">
                            <label>Content Brand</label>
                            <a class="ml-2" href="#" style="color: #fa5838;float:right;" id="btn-remove-brand"><i class="mdi mdi-minus"></i>Remove brand</a>
                            <a class="ml-2" href="#" style="float:right;" data-toggle="modal" data-target="#modal-add-brand"><i class="mdi mdi-plus"></i>Add brand</a>
                        </div>
                        <div class="col-12" id="first-brand">
                            <input type="hidden">
                            <label>- Silahkan Tambahkan Beberapa Brand, Boleh Dikosongi !.</label>
                        </div>
                    </div>
                </form>
                <div class="row pt-2" style="border-top: 1px solid #e9ecef;">
                    <div class="col-12" style="text-align: center"> 
                        <button class="btn btn-danger btn-reset mt-2 mr-1" style="width: 168px;">Reset</button>
                        <button class="btn btn-primary mt-2 ml-1" style="width: 168px;" id="btn-save-offer">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal add category-->
<div class="modal modal-primary fade" id="modal-add-category" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="main-category">Main Category</label>
                    <select id="main-category" class="form-control">
                        <option value="0">Please Select Category</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="sub-category-1">Sub Category 1</label>
                    <select id="sub-category-1" class="form-control">
                        <option value="0">Please Select Previous Category</option>
                    </select>
                    <small><i class="mdi mdi-alert-circle-outline"></i> Sub-category bisa dikosongi dengan memilih opsi teratas</small>
                </div>
                <div class="form-group">
                    <label for="sub-category-2">Sub Category 2</label>
                    <select id="sub-category-2" class="form-control">
                        <option value="0">Please Select Previous Category</option>
                    </select>
                    <small><i class="mdi mdi-alert-circle-outline"></i> Sub-category bisa dikosongi dengan memilih opsi teratas</small>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-add-category">Ok</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal add brand-->
<div class="modal modal-primary fade" id="modal-add-brand" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New Brand</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="brand-name">Brand Name</label>
                    <select id="brand-name" class="form-control">
                        <option value="0">Please Select Brand Name</option>
                        @foreach ($brands as $brand)
                            <option value="{{ $brand['id'] }}">{{ $brand['name'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-add-brand">Ok</button>
            </div>
        </div>
    </div>
</div>

<script>
let categoryArr = 0;
let categories = @json($categories);
let category1 = '';

$('select#main-category').on('change', function() {
    if (this.value == 0) {
        $('select#sub-category-1').html('<option value="0">Please Select Previous Category</option>').hide().fadeIn(700);
        $('select#sub-category-2').html('<option value="0">Please Select Previous Category</option>').hide().fadeIn(700);
        return false;
    }
    let text = this.options[this.selectedIndex].text;
    category1 = '';
    $.each(categories, function (indexInArray, valueOfElement) { 
        if (valueOfElement.name == text) {
            category1 = categories[indexInArray];
            return false;
        }
    });
    let optionCategory1 = '';
    if (category1.sub_categories.length > 0) {
        optionCategory1 = '<option value="0">Please Select Category</option>';
        $.each(category1.sub_categories, function (indexInArray, valueOfElement) { 
            optionCategory1 += '<option value="'+valueOfElement.id+'">'+valueOfElement.name+'</option>'
        });
    } else {
        optionCategory1 = '<option value="0">Sub-Category Not Found!</option>';
    }
    $('select#sub-category-1').html(optionCategory1).hide().fadeIn(700);;
    $('select#sub-category-2').html('<option value="0">Please Select Previous Category</option>').hide().fadeIn(700);;
});

$('select#sub-category-1').on('change', function() {
    if (this.value == 0) {
        $('select#sub-category-2').html('<option value="0">Please Select Previous Category</option>').hide().fadeIn(700);
        return false;
    }
    let text = this.options[this.selectedIndex].text;
    let category2 = '';
    $.each(category1.sub_categories, function (indexInArray, valueOfElement) { 
        if (valueOfElement.name == text) {
            category2 = category1.sub_categories[indexInArray];
            return false;
        }
    });
    let optionCategory2 = '';
    if (category2.sub_categories.length > 0) {
        optionCategory2 = '<option value="0">Please Select Category</option>';
        $.each(category2.sub_categories, function (indexInArray, valueOfElement) { 
            optionCategory2 += '<option value="'+valueOfElement.id+'">'+valueOfElement.name+'</option>'
        });
    } else {
        optionCategory2 = '<option value="0">Sub-Category Not Found!</option>';
    }
    $('select#sub-category-2').html(optionCategory2).hide().fadeIn(700);
});

$('#btn-remove-category').click(function (e) { 
    e.preventDefault();
    if(categoryArr != 0) {
        categoryArr--;
        $(this).parent().nextAll().last().fadeOut(300, function(){ 
            $(this).remove();
            if (categoryArr == 0) $('#first-category').css('display', 'block').hide().fadeIn(300);
        });
    }
});

$('#btn-add-category').click(function (e) { 
    e.preventDefault();
    let mc = $('select#main-category option:selected');
    let c1 = $('select#sub-category-1 option:selected');
    let c2 = $('select#sub-category-2 option:selected');
    let value = 0;
    let component = '';

    if (c2.val() != 0) {
        value = c2.val(); 
        component = `<label><i class="mdi mdi-checkbox-blank-circle mr-2" style="vertical-align: middle;font-size:5px"></i>`+mc.html()+` <i class="mdi mdi-arrow-right-bold mx-1"></i> `+c1.html()+` <i class="mdi mdi-arrow-right-bold mx-1"></i> `+c2.html()+`</label>`;
    }
    else if (c1.val() != 0) {
        value = c1.val();
        component = `<label><i class="mdi mdi-checkbox-blank-circle mr-2" style="vertical-align: middle;font-size:5px"></i>`+mc.html()+` <i class="mdi mdi-arrow-right-bold mx-1"></i> `+c1.html()+` </label>`;
    }
    else if (mc.val() != 0) {
        value = mc.val();
        component = `<label><i class="mdi mdi-checkbox-blank-circle mr-2" style="vertical-align: middle;font-size:5px"></i>`+mc.html()+` </label>`;
    } else {
        alert('Please Select a Category!');
        return false;
    }

    if (categoryArr == 0) $('#first-category').css('display', 'none');

    $('#field-ctgry').append(`
        <div class="col-12">
            <div class="px-2">
                <input type="hidden" name="category[`+categoryArr+`]" value="`+value+`">
                `+component+`
            </div>
        </div>
    `).children(':last').hide().fadeIn(700);
    $('#modal-add-category').modal('hide');
    categoryArr++;
});

let brandArr = 0;
$('#btn-remove-brand').click(function (e) { 
    e.preventDefault();
    if(brandArr != 0) {
        brandArr--;
        $(this).parent().nextAll().last().fadeOut(300, function(){ 
            $(this).remove();
            if (brandArr == 0) $('#first-brand').css('display', 'block').hide().fadeIn(300);
        });
    }
});

$('#btn-add-brand').click(function (e) { 
    e.preventDefault();
    let brand = $('select#brand-name option:selected');

    if (brand.val() == 0) {
        alert('Please Select Brand Name!');
        return false;
    }

    if (brandArr == 0) $('#first-brand').css('display', 'none');

    $('#field-brand').append(`
        <div class="col-12">
            <div class="px-2">
                <input type="hidden" name="brand[`+brandArr+`]" value="`+brand.val()+`">
                <label><i class="mdi mdi-checkbox-blank-circle mr-2" style="vertical-align: middle;font-size:5px"></i>`+brand.html()+` </label>
            </div>
        </div>
    `).children(':last').hide().fadeIn(700);
    $('#modal-add-brand').modal('hide');
    brandArr++;
});

$('#btn-save-offer').click(function (e) { 
    let btn = $(this);
    e.preventDefault();
    clearError()
    
    let formData = new FormData($('#form-offer')[0]);
    $.ajax({    
        type: "post",
        url: "{{ url('offer') }}",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        beforeSend: function(){
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success : function(response) {
            // console.log(response);
            if (!response.error) {
                // location.reload();
                window.location.href = "{{ url('offer') }}";
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        }, 
        error : function(response) {
            console.log(response);
            alert('Something wrong !!!');
            btn.html('Save');
            btn.prop('disabled', false);
        }
    });
});
</script>
@endsection