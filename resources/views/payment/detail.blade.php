@extends('layout.main')

@section('title', 'Detail Payment')

@section('content')
<div class="card">
    {{-- <span class="card-title py-3"> 
        <div class="row">
            <div class="col-12">
                <span class="ml-2" style="font-size: 18px;" >Order History</span>
            </div>
        </div>
    </span> --}}
    <div class="card-body">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <b><h5 class="card-title-1" style="color: #597696;"> Informasi Penerima </h5></b>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label class="mt-1">Nama Penerima</label>
                        </div>
                        <div class="col-9">
                            <input type="text" class="form-control" value="{{ $payment['location']['receiver_name'] }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label class="mt-1">Kategori Alamat</label>
                        </div>
                        <div class="col-9">
                            <input type="text" class="form-control" value="{{ $payment['location']['category'] }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label class="mt-1">No.Telepon</label>
                        </div>
                        <div class="col-9">
                            <input type="text" class="form-control" value="{{ $payment['location']['phone'] }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label class="mt-1">Alamat</label>
                        </div>
                        <div class="col-9">
                            <input type="text" class="form-control" value="{{ $payment['location']['address'] }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label class="mt-1">Kecamatan</label>
                        </div>
                        <div class="col-9">
                            <input type="text" class="form-control" value="{{ $payment['location']['detail']['subdistrict_name'] }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label class="mt-1">Kota/Kab.</label>
                        </div>
                        <div class="col-9">
                            <input type="text" class="form-control" value="{{ $payment['location']['detail']['city'] }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label class="mt-1">Provinsi</label>
                        </div>
                        <div class="col-9">
                            <input type="text" class="form-control" value="{{ $payment['location']['detail']['province'] }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label class="mt-1">Kode Pos</label>
                        </div>
                        <div class="col-9">
                            <input type="text" class="form-control" value="{{ $payment['location']['postal_code'] }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label class="mt-1">Description</label>
                        </div>
                        <div class="col-9">
                            <textarea rows="2" class="form-control" readonly>{{ $payment['location']['description'] }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div id="map" style="width:100%; height: 320px;border-radius:10px"></div>
                </div>
            </div>
            <div class="col-12 col-md-4 mt-4 mt-md-0">
                <div class="form-group">
                    <h5 class="card-title-1" style="color: #597696;"> Informasi Pemesanan</h5>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4">
                            <label class="mt-1">Status Pemesanan</label>
                        </div>
                        <div class="col-7">
                            @if ($payment['status_payment'] == '0')
                                <input type="text" class="form-control" value="Menunggu Pembayaran" readonly>
                            @elseif ($payment['status_payment'] == '97')
                                <input type="text" class="form-control" value="Barang Dalam Komplain" readonly>
                            @elseif ($payment['status_payment'] == '98')
                                <input type="text" class="form-control" value="Customer Mengajukan Pembatalan" readonly>
                            @elseif ($payment['status_payment'] == '99')
                                <input type="text" class="form-control" value="Pesanan Telah Dibatalkan" readonly>
                            @elseif ($payment['status_shipment'] == '1')
                                <input type="text" class="form-control" value="Menunggu Kurir" readonly>
                            @elseif ($payment['status_shipment'] == '2')
                                <input type="text" class="form-control" value="Sedang Dikirim" readonly>
                            @elseif ($payment['status_shipment'] == '3')
                                <input type="text" class="form-control" value="Pesanan Sampain Penerima" readonly>
                            @elseif ($payment['status_shipment'] == '4')
                                <input type="text" class="form-control" value="Barang Dikonfirmasi" readonly>
                            @elseif ($payment['status_payment'] == '1')
                                <input type="text" class="form-control" value="Pesanan Dikemas" readonly>
                            @endif
                            {{-- <button class="btn btn-sm btn-danger mt-2" style="width: 180px">Batalkan Pemesanan</button> --}}
                        </div>
                        <div class="col-1 p-0">
                            <button class="btn btn-danger" style="padding: 0.3rem 0.6rem;" {{ $payment['status_payment'] == '99' || $payment['status_shipment'] == '4'? 'disabled' :'' }} data-toggle="modal" data-target="#modal-cancel"><i class="mdi mdi-close-outline"></i></button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4">
                            <label class="mt-1">Id Pemesanan</label>
                        </div>
                        <div class="col-8">
                            <input type="text" class="form-control" value="{{ $payment['id'] }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4">
                            <label class="mt-1">Nomor Tagihan</label>
                        </div>
                        <div class="col-8">
                            <input type="text" class="form-control" value="{{ $payment['invoice_number'] }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4">
                            <label>Status Pembayaran</label>
                        </div>
                        <div class="col-8">
                            @if ($payment['type'] == 'bca-va')
                                @php 
                                    $status = 'Sudah Dibayar' 
                                @endphp
                                @foreach ($payment['va_payments'] as $pay)
                                    @if ($pay['status'] = '0')
                                        @php 
                                            $status = 'Menunggu Dibayar' 
                                        @endphp
                                    @endif
                                @endforeach
                                <input type="text" class="form-control" value="{{ $status }}" readonly>
                            @else
                                <input type="text" class="form-control" value="{{ $payment['transaction']['status'] == '0' ? 'Menunggu Pembayaran' : 'Sudah Dibayar' }}" readonly>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4">
                            <label>Metode Pembayaran</label>
                        </div>
                        <div class="col-8">
                            <input type="text" class="form-control" value="{{ $payment['type'] != null ? $payment['type'] : 'Xendit' }}" readonly>
                        </div>
                    </div>
                </div>
                @php
                    $total = 0;
                @endphp
                <div style="overflow: scroll;max-height:450px;overflow-x:hidden">
                    @foreach ($payment['order'] as $order)
                    <div class="form-group">
                        <div class="row">
                            <div class="col-3">
                                <img src="{{ $order['item']['images'][0]['name'] ?? asset('assets/images/item/not_found.png') }}" class="w-100" alt="">
                            </div>
                            <div class="col-9">
                                <div class="mt-2">
                                    <label>{{ $order['name'] }}</label>
                                    <label style="float:right" class="mr-1">x{{ $order['qty'] }}</label>
                                </div>
                                <p class="mb-2">Note : {{ $order['note'] }}</p>
                                <p style="float: right;color:green" class="mr-1">Rp. {{ number_format($order['price']) }}</p>
                                @php
                                    $total += $order['price']
                                    @endphp
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="row mt-3">
                    <div class="col-3"></div>
                    <div class="col-9">
                        <div class="w-100">
                            <label>Total Barang</label>
                            <label class="mr-3" style="float: right">Rp. {{ number_format($total) }}</label>
                        </div>
                        <div class="w-100">
                            <label>Biaya Kirim</label>
                            <label class="mr-3" style="float: right">Rp. {{ number_format($payment['total_shipment']) }}</label>
                        </div>
                        <div class="w-100">
                            <b><label>Total pembayaran</label></b>
                            <b><label class="mr-3" style="float: right;color:green">Rp. {{ number_format($payment['total']) }}</label></b>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 mt-4 mt-md-0">
                <div class="form-group">
                    <h5 class="card-title-1" style="color: #597696;"> Informasi Pengiriman </h5>
                </div>
                <form id="form-courier">
                    @csrf
                    <div class="form-group">
                        <div class="row">
                            <div class="col-3">
                                <label class="mt-1">Kurir Pengirim</label>
                            </div>
                            <div class="col-9">
                                <select class="form-control" disabled>
                                @if ($payment['shipmentable_type'] == 'courier')
                                    <option>Courier Endora</option>
                                @else
                                    <option>Shipment Express</option>
                                @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-3">
                                <label class="mt-1">No. Resi</label>
                            </div>
                            <div class="col-9">
                                <input type="text" class="form-control" value="{{ $payment['waybill_number'] ?? '' }} " name="waybill_number">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-3">
                                <label class="mt-1">Nama</label>
                            </div>
                            <div class="col-9">
                                <select name="id" class="form-control">
                                    @if ($payment['shipmentable_type'] == 'courier')
                                        @if ($payment['shipmentable_id'] == 0)
                                            @foreach ($couriers as $courier)
                                                <option value="{{ $courier['id'] }}">{{ $courier['fullname'] }}</option>
                                            @endforeach
                                        @else
                                            @foreach ($couriers as $courier)
                                                <option value="{{ $courier['id'] }}" {{ $payment['shipmentable']['id'] == $courier['id'] ? 'selected' : '' }} >{{ $courier['fullname'] }}</option>
                                            @endforeach
                                        @endif
                                        
                                    @else
                                        @if ($payment['shipmentable_id'] == 0)
                                            @foreach ($shipments as $shipment)
                                                <option value="{{ $shipment['id'] }}" >{{ $shipment['name'].' - '.$shipment['service']  }}</option>
                                            @endforeach
                                        @else
                                            @foreach ($shipments as $shipment)
                                                <option value="{{ $shipment['id'] }}" {{ $payment['shipmentable']['id'] == $shipment['id'] ? 'selected' : '' }} >{{ $shipment['name'].' - '.$shipment['service']  }}</option>
                                            @endforeach
                                        @endif
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-12">
                        @if ($payment['status_payment'] != '99')
                            @if (!empty($payment['histories']))
                                <button class="btn btn-sm btn-primary" style="float: right" id="btn-save-courier" {{ $payment['waybill_number'] == null ? '' : 'disabled' }}><i class="mdi mdi-clipboard-account"></i> Update This Shipment</button>
                            @else
                                <button class="btn btn-sm btn-primary" style="float: right" id="btn-save-courier" {{ $payment['waybill_number'] == null ? '' : 'disabled' }}><i class="mdi mdi-clipboard-account"></i> Select This Courier</button>
                                @endif
                        @else
                            <button class="btn btn-sm btn-primary" style="float: right" disabled><i class="mdi mdi-clipboard-account"></i> Select This Courier</button>
                        @endif
                        {{-- <button class="btn btn-sm btn-success mr-2" style="float: right" id="btn-print-invoice"><i class="mdi mdi-printer"></i> Print Invoice</button> --}}
                    </div>
                </div>

                @if ($payment['status_payment'] == '98' || $payment['status_payment'] == '99')
                    <div class="form-group mt-4">
                        <h5 class="card-title-1" style="color: #597696;"> Alasan Pembatalan </h5>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-3">
                                <label class="mt-1">Pembatalan Dari</label>
                            </div>
                            <div class="col-9">
                                <input type="text" class="form-control" value="{{ $payment['cancel']['cancel_by'] }}" disabled="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-3">
                                <label class="mt-1">Alasan</label>
                            </div>
                            <div class="col-9">
                                <input type="text" class="form-control" value="{{ $payment['cancel']['reason'] }}" disabled="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-3">
                                <label class="mt-1">Pengembalian Dana</label>
                            </div>
                            <div class="col-9">
                                <input type="text" class="form-control" value="{{ $payment['cancel']['refund'] }}" disabled="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-3">
                                <label class="mt-1">Tanggal Pembatalan</label>
                            </div>
                            <div class="col-9">
                                <input type="text" class="form-control" value="{{ $payment['cancel']['created_at'] }}" disabled="">
                            </div>
                        </div>
                    </div>
                @else
                    <div class="form-group mt-4">
                        <h5 class="card-title-1" style="color: #597696;"> Riwayat Pengiriman </h5>
                    </div>
                    @if ($payment['shipmentable_type'] == 'courier')
                        @forelse ($payment['histories'] as $history)
                            <div class="row mb-2">
                                <div class="col-1">
                                    @if($loop->first)
                                        <i class="mdi mdi-ray-start wi-rotate-90" style="font-size: 60px;display: block;margin: -25px;margin-bottom: -44px;margin-top: -7px;overflow: hidden;"></i>
                                    @elseif($loop->last)
                                        <i class="mdi mdi-ray-end wi-rotate-90" style="font-size: 60px;display:block;margin: -25px;margin-top: -46px;overflow: hidden;"></i>
                                    @else
                                        <i class="mdi mdi-ray-vertex wi-rotate-90" style="font-size: 60px;display:block;margin: -25px;"></i>
                                    @endif
                                </div>
                                <div class="col-11 pt-1">
                                    <div>
                                        <span><b>{{ $history['status']['name'] }} </b> <small><a href="javascript:void(0)" type="button" onclick="detailShipping({{ $history['id'] }})">detail</a></small></span>
                                        <span style="float: right">{{ \Carbon\Carbon::parse($history['created_at'])->isoFormat('dddd, D MMM Y') }} </span>
                                    </div>
                                    <div>
                                        @if ($payment['shipmentable_type'] == 'courier')
                                            <small>{{ 'Kurir ['.$payment['shipmentable']['fullname'].'] '.$history['status']['description'] }} </small>
                                        @else
                                            <small>{{ 'Kurir ['.$payment['shipmentable']['name'].'] '.$history['status']['description'] }} </small>
                                        @endif
                                        <small style="float: right">{{ substr($history['created_at'], 11, 8) }} </small>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="row"><div class="col-12"><h6><center>Menunggu pembayaran atau barang masih dikemas</center></h6></div></div>
                        @endforelse
                    @elseif ($payment['histories'])
                        @forelse ($payment['histories']['manifest'] as $history)
                            <div class="row mb-2">
                                <div class="col-1">
                                    @if($loop->first)
                                        <i class="mdi mdi-ray-start wi-rotate-90" style="font-size: 60px;display: block;margin: -25px;margin-bottom: -44px;margin-top: -7px;overflow: hidden;"></i>
                                    @elseif($loop->last)
                                        <i class="mdi mdi-ray-end wi-rotate-90" style="font-size: 60px;display:block;margin: -25px;margin-top: -46px;overflow: hidden;"></i>
                                    @else
                                        <i class="mdi mdi-ray-vertex wi-rotate-90" style="font-size: 60px;display:block;margin: -25px;"></i>
                                    @endif
                                </div>
                                <div class="col-11 pt-1">
                                    <div>
                                        <span><b>{{ $history['manifest_description'] }} </b></span>
                                        <span style="float: right">{{ \Carbon\Carbon::parse($history['manifest_date'])->isoFormat('dddd, D MMM Y') }} </span>
                                    </div>
                                    <div>
                                        <small>{{ $history['city_name'] }} </small>
                                        <small style="float: right">{{ $history['manifest_time'] }} </small>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="row"><div class="col-12"><h6><center>Menunggu pembayaran atau barang masih dikemas</center></h6></div></div>
                        @endforelse
                    @else 
                        <div class="row"><div class="col-12"><h6><center>Pengiriman ini tidak terlacak oleh sistem</center></h6></div></div>
                    @endif
                @endif
            </div>
        </div>
    </div>
</div>

<!-- Modal Cancel Status -->
<div class="modal modal-danger fade" id="modal-cancel" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form action="{{ route('payment.cancel', $payment['id']) }}" method="post" id="form-cancel">
                <div class="modal-body">
                    @csrf
                    @if ($payment['status_payment'] == '98')
                        <div class="form-group">
                            <label>Cancelled By</label>
                            <input type="text" class="form-control" name="cancel_by" value="customer" id="input-cancel-by" readonly>
                        </div>
                        <div class="form-group" id="group-reason-cancel" >
                            <label>Reason Cancel Customer</label>
                            <input type="text" class="form-control" name="reason" value="{{ $payment['cancel']['reason'] }}" readonly>
                        </div>
                    @else
                        <div class="form-group">
                            <label>Cancelled By</label>
                            <input type="text" class="form-control" name="cancel_by" value="admin" id="input-cancel-by" readonly>
                        </div>
                        <div class="form-group">
                            <label>Select the reason cancel payment</label>
                            <select name="reason" class="form-control" id="select-reason-cancel">
                                <option>Stock Telah Habis</option>
                                <option>Pesanan Telah Kadaluarsa</option>
                                <option>Permintaan Pelanggan Pembatalan Transaksi</option>
                                <option>Lainnya</option>
                            </select>
                        </div>
                    @endif
                    <div class="form-group">
                        <label>Refund Amount</label>
                        <input type="number" class="form-control" name="refund" >
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Detail History Shipping -->
<div class="modal modal-primary fade" id="modal-history" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Shipping History</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label>Status</label>
                            <input type="text" class="form-control" value="" id="text-status-history" disabled>
                        </div>
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <input type="text" class="form-control" value="" id="text-description-history" disabled>
                        </div>
                        <div class="form-group">
                            <label>Tanggal</label>
                            <input type="text" class="form-control" value="" id="text-datetime-history" disabled>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <img src="" style="float:right;width: auto;max-height: 230px;height: auto;" id="img-image-history">
                    </div>
                </div>
                <div id="map-history" class="mt-2" style="width:100%; height: 320px;border-radius:10px"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnE-6kZ20FsIhDTEW2UmLtDGK43SZCV1I"></script>
<script>
let history = @json($payment['histories']);
let courier = @json($payment['shipmentable'])

var myMap;
var myLatlng = new google.maps.LatLng(Number("{{ $payment['location']['latitude'] }}"),Number("{{ $payment['location']['longitude'] }}"));

var myMapHistory;
var myLatlngHistory = new google.maps.LatLng(Number("{{ $payment['location']['latitude'] }}"),Number("{{ $payment['location']['longitude'] }}"));
var markerHistory;

function initialize() {
    var mapOptions = {
        zoom: 15,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP  ,
        scrollwheel: false
    }
    myMap = new google.maps.Map(document.getElementById('map'), mapOptions);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: myMap,
        title: "{{ ' ('.$payment['location']['category'].') '.$payment['location']['receiver_name'] }}",
        icon: 'http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png'
    });

    var mapOptionsHistory = {
        zoom: 15,
        center: myLatlngHistory,
        mapTypeId: google.maps.MapTypeId.ROADMAP  ,
        scrollwheel: false
    }
    myMapHistory = new google.maps.Map(document.getElementById('map-history'), mapOptionsHistory);
    markerHistory = new google.maps.Marker({
        position: myLatlngHistory,
        map: myMapHistory,
        title: "{{ ' ('.$payment['location']['category'].') '.$payment['location']['receiver_name'] }}",
        icon: 'http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png'
    });
}
google.maps.event.addDomListener(window, 'load', initialize);

$('#btn-save-courier').click(function (e) { 
    e.preventDefault();
    clearError();
    let btn = $(this);

    $.ajax({
        type: "post",
        url: "{{ url('payment').'/'.$payment['id'].'/select' }}",
        data: $('#form-courier').serialize(),
        beforeSend: function() {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        }, 
        error: function (){
            alert('Something wrong !!!');
            btn.html('Save');
            btn.prop('disabled', false);
        }
    });
});

$('#btn-print-invoice').click(function (e) { 
    e.preventDefault();
    alert('Feature is Under Repair');
});

function detailShipping(id) {
    // alert(id);
    let status, description, datetime, image, newLatitude, newLongitude;
    let titleMap = 'courier mark in the area here';
    $.each(history, function (indexInArray, valueOfElement) { 
        if (valueOfElement.id == id) {
            status = valueOfElement.status.name;
            description = 'Kurir '+courier.fullname+' '+valueOfElement.status.description;
            datetime = valueOfElement.created_at;
            image = valueOfElement.image;
            newLatitude = Number(valueOfElement.latitude);
            newLongitude = Number(valueOfElement.longitude);
            return false;
        }
    });
    console.log(description, datetime);
    let NewLatLng = new google.maps.LatLng(newLatitude, newLongitude);
    markerHistory.setTitle( titleMap );
    markerHistory.setPosition( NewLatLng );
    markerHistory.map.setCenter( NewLatLng );

    $('#text-status-history').val(status);
    $('#text-description-history').val(description);
    $('#text-datetime-history').val(datetime);
    $('#img-image-history').attr('src', image);
    $('#modal-history').modal('show');
}

</script>
@endsection