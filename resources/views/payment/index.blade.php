@extends('layout.main')

@section('title', 'Payment')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="table-payment" class="table table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr style="line-height: 2.2">
                                <th>NO.</th>
                                <th>EMAIL</th>
                                <th style="text-align: center">PAYMENT_ID</th>
                                <th style="text-align: center">WAYBILL</th>
                                {{-- <th style="text-align: center">PAYMENT</th> --}}
                                <th style="text-align: center">STATUS</th>
                                <th style="text-align: center">CREATED</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function () {
    // $.fn.dataTable.ext.classes.sPageButton = 'button primary_button';
    let t = $('#table-payment').DataTable( {
        "pageLength": 100,
        "processing": true,
        // "serverSide": true,
        "ajax": "{!! url()->current() !!}",
        "order": [[ 6, "desc" ]],
        "columns": [
            {
                "data": null,
                "width": "10px",
                "sClass": "text-center",
                "bSortable": false
            },
            { "data": "user.email"},
            { "data": "id", "sClass": "text-center","bSortable": false},
            { "data": "waybill_number", "sClass": "text-center","bSortable": false},
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    if (row.status_payment == '0') return '<label class="label label-rounded label-success">Menunggu Pembayaran</label>';
                    else if (row.status_payment == '97' || row.status_shipment == '5') return '<label class="label label-rounded label-purple">Komplain</label>';
                    else if (row.status_payment == '98') return '<label class="label label-rounded label-warning">Pengajuan Batal</label>';
                    else if (row.status_payment == '99') return '<label class="label label-rounded label-red">Dibatalkan</label>';
                    else if (row.status_shipment == '1') return '<label class="label label-rounded label-megna">Menunggu Kurir</label>';
                    else if (row.status_shipment == '2') return '<label class="label label-rounded label-megna">Sedang Dikirim</label>';
                    else if (row.status_shipment == '3') return '<label class="label label-rounded label-megna">Pesanan Sampai</label>';
                    else if (row.status_shipment == '4') return '<label class="label label-rounded label-megna">Barang Dikonfirmasi</label>';
                    else return '<label class="label label-rounded label-success">Pesanan DiKemas</label>';
                }
            },
            { "data": "created_at", "sClass": "text-center" },
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    return `
                        <a href="{{ url('payment') }}/`+row.id+`" class="btn btn-sm btn-success"><i class="mdi mdi-magnify"></i> Detail</a>
                        `;
                },"bSortable": false
            }
        ],
        "createdRow": function (row, data, index) {
            // $('td', row).eq(1).addClass('truncated');
            // $('td', row).eq(2).css('width', '50px');
            // $('td', row).eq(3).css('width', '40px');
            $('td', row).eq(7).css('width', '160px');
            // $('td', row).eq(5).css('width', '150px');
        },
        'initComplete': function(){
            $("#table-payment_length").parent().parent().append(`
                <div class="col-12 col-md-6" style="text-align: right;">
                    <form class="mr-3" action="{{ route('payment.exportReport') }}" method="post" style="text-align: right;margin-bottom: 5px;display: inline-block;">
                        @csrf
                        <button type="submit" class="btn btn-success" style="width:180px">
                            <i class="mdi mdi-printer mr-1"></i> Print Report
                        </button>
                    </form>
                    <form action="{{ route('payment.synchronize') }}" method="post" style="text-align: right;margin-bottom: 5px;display: inline-block;">
                        @csrf
                        <button type="submit" class="btn btn-primary" style="width:180px">
                            <i class="mdi mdi-sync mr-1"></i> Synchronize
                        </button>
                    </form>
                </div>
            `);
            $("#table-payment_length").parent().next().addClass('col-12 col-md-6').removeClass('col-sm-12 ')
            $("#table-payment_length").parent().hide();
            $("#table-payment_filter").css('text-align', 'left');
            $("#table-payment_filter input").css('width', '300px');
        }   
    });
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
});

</script>

@endsection