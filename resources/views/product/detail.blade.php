@extends('layout.main')

@section('title', 'Detail Product')

@section('content')
<div class="row">
    <div class="col-12 col-md-5">
        <div class="card" style="height: 400px;">
            <div class="card-body">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="padding-bottom: 15px;border-bottom: 1px solid #e4dcdc;text-align: -webkit-center;">
                    <ol class="carousel-indicators">
                        @forelse ($product['images'] as $item)
                            <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"><img src="{{ $item['name'] }}" width="70" height="60" alt=""></li>
                        @empty
                            <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"><img src="{{ asset('storage/item/not_found.png') }}" width="70" height="60" alt=""></li>
                        @endforelse
                    </ol>
                    <div class="row">
                        <div class="col-12 text-center" style="position: absolute;bottom: 32px;z-index: 999;">
                            <a href="#" type="button" class="btn btn-light" style="opacity: .7;width: 186px;box-shadow: none !important;
                            " data-toggle="modal" data-target="#modal-add-image"><i class="mdi mdi-plus"></i> add image</a>
                        </div>
                    </div>
                    <div class="carousel-inner" style="max-width: 440px;">
                        @forelse ($product['images'] as $item)
                            <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                <div class="container text-right" style="position: absolute;top:10px">
                                    <button class="btn btn-danger" style="opacity: .5;box-shadow: none !important;padding: .1rem .3rem;" onclick="removeImage({{ $item['id'] }})"><i class="mdi mdi-close"></i></button>
                                </div>
                                <img class="d-block" height="280px" src="{{ $item['name'] }}" data-holder-rendered="true">
                            </div>
                        @empty
                            <div class="carousel-item active">
                                <img class="d-block" height="280px" src="{{ asset('storage/item/not_found.png') }}" data-holder-rendered="true">
                            </div>
                        @endforelse
                    </div>
                    {{-- <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a> --}}
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-7">
        <div class="card pb-2" style="min-height: 400px;">
            <div class="card-body pb-0">
                <div>
                    <a href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="right:12px;position: absolute;font-size: 26px;top: 10px;z-index: 9999;">
                        <i class="mdi mdi-chevron-down" ></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ url('product').'/'.$product['id'].'/edit' }}" ><i class="mdi mdi-pen mr-2"></i> Update</a>
                        <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-delete-product"><i class="mdi mdi-delete mr-2"></i> Delete</a>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2" style="text-align: right">
                            <label>BRAND</label>
                        </div>
                        <div class="col-10">
                            <div class="container">
                                {{ $product['brand']['name'] }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2" style="text-align: right">
                            <label>NAME</label>
                        </div>
                        <div class="col-10">
                            <div class="container">
                                {{ $product['name'] }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2" style="text-align: right">
                            <label>PRICE</label>
                        </div>
                        <div class="col-10">
                            <div class="container">
                                {{ $product['price'] }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2" style="text-align: right">
                            <label>VARIANT</label>
                        </div>
                        <div class="col-10">
                            <div class="container">
                                {{ $product['variant'] }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2" style="text-align: right">
                            <label>INVENTORY</label>
                        </div>
                        <div class="col-10">
                            <div class="container">
                                {{ $product['inventory'] }} item
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2" style="text-align: right">
                            <label>WEIGHT</label>
                        </div>
                        <div class="col-10">
                            <div class="container">
                                {{ $product['weight'] }} gram
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-0">
                    <div class="row">
                        <div class="col-2" style="text-align: right">
                            <label>CATEGORY</label>
                        </div>
                        <div class="col-10 pr-0">
                            <div class="container">
                                <div class="row">
                                    @foreach ($product['categories'] as $categories)
                                        <p style="width: 100%"><i class="mdi mdi-checkbox-blank-circle mr-2" style="vertical-align: middle;font-size:5px"></i> 
                                            @if (empty($categories['categories'][0]['categories'][0]['name']) && empty($categories['categories'][0]['name']))
                                                {{$categories['name'] }}
                                            @elseif (empty($categories['categories'][0]['categories'][0]['name']))
                                                {{ $categories['categories'][0]['name'] ?? null }} <i class="mdi mdi-arrow-right-bold mx-1"></i> {{$categories['name'] }}
                                            @else
                                                {{ $categories['categories'][0]['categories'][0]['name'] }} <i class="mdi mdi-arrow-right-bold mx-1"></i> {{ $categories['categories'][0]['name'] }} <i class="mdi mdi-arrow-right-bold mx-1"></i> {{$categories['name'] }}
                                            @endif
                                        </p>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <span class="card-title py-3"> 
        <div class="row">
            <div class="col-12">
                <span class="ml-2" style="font-size: 18px;" >Description Product</span>
            </div>
        </div>
    </span>
    <div class="card-body pt-0" >
        <div class="form-group">
            <div class="px-2" style="white-space: pre-line;">
                <p>{{ $product['description'] }} </p>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-danger fade" tabindex="-1" role="dialog" id="modal-delete-product">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this product ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" id="btn-delete-product">Ok</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-primary fade" tabindex="-1" role="dialog" id="modal-add-image">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-new-image">
                    @csrf
                    <input type="hidden" value="{{ count($product['images'])+1 }}" name="order">
                    <div class="form-group">
                        <label>Image <small>( Size:180 x 180 . Max:1mb )</small></label>
                        <input type="file" class="form-control" name="name" >
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn-add-image">Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-danger fade" tabindex="-1" role="dialog" id="modal-delete-image">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this image item ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" id="btn-delete-image">Ok</button>
            </div>
        </div>
    </div>
</div>

<script>
var imageItemId = 1;

$('#btn-delete-product').click(function() {
    let btn = $(this);
    $.ajax({
        type: "get",
        url: "{{ url('product').'/'.$product['id'].'/delete' }}",
        data: {
            "_token": "{{ csrf_token() }}",
        },
        dataType: "json",
        beforeSend:function () {
            btn.html('Deleting...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                // location.reload();
                history.back();
            } else {
                alert(response.error);
                btn.html('Ok');
                btn.prop('disabled', false);
            }
        }
    });
});

$('#btn-add-image').click(function (e) { 
    e.preventDefault();
    clearError();
    var btn = $(this);
    var formData = new FormData($('#form-new-image')[0]);

    $.ajax({
        type: "post",
        url: "{{ url('item').'/'.$product['id'].'/image' }}",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        }, 
        error: function (){
            alert('Something wrong !!!');
            btn.html('Save');
            btn.prop('disabled', false);
        }
    });
    
});

function removeImage(id) {
    imageItemId = id;
    $('#modal-delete-image').modal('show');
}

$('#btn-delete-image').click(function (e) { 
    e.preventDefault();
    var btn = $(this);

    $.ajax({
        type: "get",
        url: "{{ url('item') }}"+'/'+imageItemId+'/delete',
        data: {
            "_token": "{{ csrf_token() }}",
        },
        dataType: "json",
        beforeSend:function () {
            btn.html('Deleting...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                alert(response.error);
                btn.html('Ok');
                btn.prop('disabled', false);
            }
        }
    });
});
</script>
@endsection