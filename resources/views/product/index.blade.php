@extends('layout.main')

@section('title', 'List Product')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <a href="{{ route('product.add') }}" class="btn btn-primary mb-4 mr-2"><i class="mdi mdi-plus"></i> Add New Product</a>
                {{-- <button class="btn btn-danger mb-4 mr-2"><i class="mdi mdi-delete-variant"></i> View Trash Item</button> --}}
                <button class="btn btn-success mb-4 mr-2" data-toggle="modal" data-target="#modal-import"><i class="mdi mdi-file-import"></i> Import File Product </button>
                <form action="{{ route('product.export') }}" method="POST" style="display: inline-block">
                    @csrf
                    <button type="submit" class="btn btn-warning mb-4"><i class="mdi mdi-file-export"></i> Export File Product </button>
                </form>
                {{-- <div class="row">
                    <div class="col-12 col-md-10">
                        <!-- <h4 class="card-title">Modul</h4> -->
                        <h6 class="card-subtitle m-t-5 m-b-20">Halaman yang berisi daftar seluruh produk/item yang akan ditampilkan untuk seluruh pengguna mobile yang terdaftar. Data produk/item yang diinputkan oleh admin dengan beberapa deskripsi yang harus dilengkapi</h6>
                    </div>
                    <div class="col-12 col-md-2">
                    </div>
                </div> --}}
                <div class="table-responsive">
                    <table id="table-product" class="table table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr style="line-height: 2.2">
                                <th>NO.</th>
                                <th style="text-align: center">IMAGE</th>
                                <th>ITEM</th>
                                <th style="text-align: right !important">PRICE (Rp)</th>
                                <th style="text-align: center">INVENTORY</th>
                                <th style="text-align: center">BRAND</th>
                                <th style="text-align: center">CREATED</th>
                                <th></th> 
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-danger fade" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this product ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <form action="{{ route('product.delete', 1) }}" method="POST" id="form-delete">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-danger" id="btn-delete">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Import Excel -->
<div class="modal modal-success fade" id="modal-import" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">File Import Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form id="form-import">
                    @csrf
                    <div class="form-group">
                        <label>Choose file excel to import</label>
                        <input type="file" class="form-control" name="file">
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="btn-import">Import</button>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function () {
    // $.fn.dataTable.ext.classes.sPageButton = 'button primary_button';
    let t = $('#table-product').DataTable( {
        "pageLength": 50,
        "processing": true,
        // "serverSide": true,
        "ajax": "{!! url()->current() !!}",
        "columns": [
            {
                "data": null,
                "width": "10px",
                "sClass": "text-center",
                "bSortable": false
            },
            { "data": "null","bSortable": false, "sClass": "text-center", render: function ( data, type, row ) {
                    if (row.images.length > 0) {
                        return '<img src="'+row.images[0].name+'" width="100" height="70">';
                    } else {
                        return `<img src="{{ asset('storage/item/not_found.png') }}" width="100" height="70">`;
                    }
                }
            },
            { "data": "name"},
            { "data": "null", "sClass": "text-right", render: function ( data, type, row ) {
                    let	reverse = row.price.toString().split('').reverse().join('');
                    let ribuan 	= reverse.match(/\d{1,3}/g);
                    ribuan	= ribuan.join(',').split('').reverse().join('');
                    return ribuan;
                }
            },
            { "data": "inventory", "sClass": "text-center"},
            { "data": "brand.name", "sClass": "text-center"},
            { "data": "created_at", "sClass": "text-center" },
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    return `
                        <a href="{{ url('product') }}/`+row.id+`" class="btn btn-sm btn-success"><i class="mdi mdi-magnify"></i></a>
                        <button class="btn btn-sm btn-danger m-l-5" title="delete" onclick="deleteItem(`+row.id+`)"><i class="mdi mdi-delete"></i> </button>
                        `;
                },"bSortable": false
            }
        ],
        "createdRow": function (row, data, index) {
            // $('td', row).eq(1).addClass('truncated');
            // $('td', row).eq(2).css('width', '50px');
            // $('td', row).eq(3).css('width', '40px');
            $('td', row).eq(6).css('width', '80px');
            // $('td', row).eq(5).css('width', '150px');
        },
    });
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
});

function deleteItem(id) {
    let url = "{{ route('product.delete', ':id') }}";
    $('#form-delete').attr('action', url.replace(':id', id));
    $('#modal-delete').modal('show');
}

$('#btn-import').click(function (e) { 
    e.preventDefault();
    let btn = $(this);
    var formData = new FormData($('#form-import')[0]);

    $.ajax({
        type: "post",
        url: "{{ route('product.import') }}",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {
            btn.html('Import...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            // console.log(response);
            if (!response.error) {
                location.reload();
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('Import');
                btn.prop('disabled', false);
            }
        }, 
        error: function (){
            alert('Something wrong !!!');
            btn.html('Import');
            btn.prop('disabled', false);
        }
    });
});

</script>

@endsection