@extends('layout.main')

@section('title', 'Update Product')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form class="form-horizontal m-t-10" id="form-product">
                    <div class="row">
                        <div class="col-12">
                            @csrf
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" form="form-product" placeholder="ex. Opia Thermal Jug 800 ml" name="name" value="{{ $product['name'] }}">
                                <span class="help-block"></span>
                            </div>
                            <div class="row" style="width : 100%">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Price</label>
                                        <input type="number" class="form-control" form="form-product" placeholder="ex. 150000" name="price" value="{{ $product['price'] }}">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Inventory</label>
                                        <input type="number" class="form-control" form="form-product" placeholder="ex. 30" name="inventory" value="{{ $product['inventory'] }}">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Weight (gram)</label>
                                        <input type="number" class="form-control" form="form-product" placeholder="ex. 2,1" name="weight" value="{{ $product['weight'] }}">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="width : 100%">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Brand</label>
                                        <select name="brand_id" class="form-control">
                                            @foreach ($brands as $brand)
                                            <option value="{{ $brand['id'] }}" {{ $brand['id'] == $product['brand']['id'] ? 'selected' : '' }}>{{ $brand['name'] }}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="form-group">
                                        <label>Variant</label>
                                        <input type="text" class="form-control" form="form-product" placeholder="ex. high,medium,low" name="variant" value="{{ $product['variant'] }}">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="field-ctgry">
                                <div class="col-12">
                                    <label>Category</label>
                                    <a class="ml-2" href="#" style="color: #fa5838;float:right;" id="btn-remove-category"><i class="mdi mdi-minus"></i>Remove Category</a>
                                    <a class="ml-2" href="#" style="float:right;" data-toggle="modal" data-target="#modal-add-category"><i class="mdi mdi-plus"></i>Add Category</a>
                                </div>
                                @foreach ($product['categories'] as $categories)
                                <div class="col-12">
                                    <div class="px-2">
                                        <input type="hidden" name="category[{{ $loop->index }}]" value="{{ $categories['id'] }}">
                                        <label><i class="mdi mdi-checkbox-blank-circle mr-2" style="vertical-align: middle;font-size:5px"></i> 
                                            @if (empty($categories['categories'][0]['categories'][0]['name']) && empty($categories['categories'][0]['name']))
                                                {{$categories['name'] }}
                                            @elseif (empty($categories['categories'][0]['categories'][0]['name']))
                                                {{ $categories['categories'][0]['name'] ?? null }} <i class="mdi mdi-arrow-right-bold mx-1"></i> {{$categories['name'] }}
                                            @else
                                                {{ $categories['categories'][0]['categories'][0]['name'] }} <i class="mdi mdi-arrow-right-bold mx-1"></i> {{ $categories['categories'][0]['name'] }} <i class="mdi mdi-arrow-right-bold mx-1"></i> {{$categories['name'] }}
                                            @endif
                                        </label>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <div class="form-group mt-2">
                                <label>Description</label>
                                <textarea class="form-control" form="form-product" name="description" rows="10" placeholder="Description of detail product">{{ $product['description'] }}</textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row mt-2">
                    <div class="col-12" style="text-align: center"> 
                        <button class="btn btn-danger btn-reset mt-2 mr-1" style="width: 168px;">Reset</button>
                        <a href="#" class="btn btn-primary mt-2 ml-1" style="width: 168px;" id="btn-save-product">Save</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal add category-->
<div class="modal modal-primary fade" id="modal-add-category" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="main-category">Main Category</label>
                    <select id="main-category" class="form-control">
                        <option value="0">Silahkan Pilih Kategori</option>
                        @foreach ($items as $category)
                            <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="sub-category-1">Sub Category 1</label>
                    <select id="sub-category-1" class="form-control">
                        <option>Silahkan Pilih Kategori Sebelumnya</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="sub-category-2">Sub Category 2</label>
                    <select id="sub-category-2" class="form-control">
                        <option>Silahkan Pilih Kategori Sebelumnya</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-save-category">Ok</button>
            </div>
        </div>
    </div>
</div>

<script>
let categoryArr = {{ count($product['categories']) }};
let categories = @json($items);
let category1 = '';

$('select#main-category').on('change', function() {
    if (this.value == 0) {
        $('select#sub-category-1').html('<option value="0">Please Select Previous Category</option>').hide().fadeIn(700);
        return false;
    }
    let text = this.options[this.selectedIndex].text;
    category1 = '';
    $.each(categories, function (indexInArray, valueOfElement) { 
        if (valueOfElement.name == text) {
            category1 = categories[indexInArray];
            return false;
        }
    });
    let optionCategory1 = '';
    if (category1.sub_categories.length > 0) {
        optionCategory1 = '<option value="0">Please Select Category</option>';
        $.each(category1.sub_categories, function (indexInArray, valueOfElement) { 
            optionCategory1 += '<option value="'+valueOfElement.id+'">'+valueOfElement.name+'</option>'
        });
    } else {
        optionCategory1 = '<option value="0">Sub-Category Not Found!</option>';
    }
    $('select#sub-category-1').html(optionCategory1).hide().fadeIn(700);;
    $('select#sub-category-2').html('<option value="0">Please Select Previous Category</option>').hide().fadeIn(700);;
});

$('select#sub-category-1').on('change', function() {
    if (this.value == 0) {
        $('select#sub-category-2').html('<option value="0">Please Select Previous Category</option>').hide().fadeIn(700);
        return false;
    }
    let text = this.options[this.selectedIndex].text;
    let category2 = '';
    $.each(category1.sub_categories, function (indexInArray, valueOfElement) { 
        if (valueOfElement.name == text) {
            category2 = category1.sub_categories[indexInArray];
            return false;
        }
    });
    let optionCategory2 = '';
    if (category2.sub_categories.length > 0) {
        optionCategory2 = '<option value="0">Please Select Category</option>';
        $.each(category2.sub_categories, function (indexInArray, valueOfElement) { 
            optionCategory2 += '<option value="'+valueOfElement.id+'">'+valueOfElement.name+'</option>'
        });
    } else {
        optionCategory2 = '<option value="0">Sub-Category Not Found!</option>';
    }
    $('select#sub-category-2').html(optionCategory2).hide().fadeIn(700);;
});

$('#btn-remove-category').click(function (e) { 
    e.preventDefault();
    if(categoryArr != 0) {
        categoryArr--;
        $(this).parent().nextAll().last().fadeOut(300, function(){ 
            $(this).remove();
            if (categoryArr == 0) $('#first-category').css('display', 'block').hide().fadeIn(300);
        });
    }
});

$('#btn-save-category').click(function (e) { 
    e.preventDefault();
    let mc = $('select#main-category option:selected');
    let c1 = $('select#sub-category-1 option:selected');
    let c2 = $('select#sub-category-2 option:selected');
    let value = 0;
    let component = '';

    if (c2.val() != 0) {
        value = c2.val(); 
        component = `<label><i class="mdi mdi-checkbox-blank-circle mr-2" style="vertical-align: middle;font-size:5px"></i>`+mc.html()+` <i class="mdi mdi-arrow-right-bold mx-1"></i> `+c1.html()+` <i class="mdi mdi-arrow-right-bold mx-1"></i> `+c2.html()+`</label>`;
    }
    else if (c1.val() != 0) {
        value = c1.val();
        component = `<label><i class="mdi mdi-checkbox-blank-circle mr-2" style="vertical-align: middle;font-size:5px"></i>`+mc.html()+` <i class="mdi mdi-arrow-right-bold mx-1"></i> `+c1.html()+` </label>`;
    }
    else if (mc.val() != 0) {
        value = mc.val();
        component = `<label><i class="mdi mdi-checkbox-blank-circle mr-2" style="vertical-align: middle;font-size:5px"></i>`+mc.html()+` </label>`;
    } else {
        alert('Please Select a Category!');
        return false;
    }

    if (categoryArr == 0) $('#first-category').css('display', 'none');

    $('#field-ctgry').append(`
        <div class="col-12">
            <div class="px-2">
                <input type="hidden" name="category[`+categoryArr+`]" value="`+value+`">
                `+component+`
            </div>
        </div>
    `).children(':last').hide().fadeIn(700);
    $('#modal-add-category').modal('hide');
    categoryArr++;
});

$('#btn-save-product').click(function (e) { 
    let btn = $(this);
    e.preventDefault();
    clearError()
    
    let formData = new FormData($('#form-product')[0]);
    $.ajax({    
        type: "post",
        url: "{{ url('product').'/'.$product['id'].'/update' }}",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        beforeSend: function(){
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success : function(response) {
            console.log(response);
            if (!response.error) {
                history.back();
            } else {
                for (let key of Object.keys(response.error)) {
                    if(key == 'category') {
                        $('[name="category[0]"]').addClass('is-invalid');
                        $('[name="category[0]"]').next().html(response.error[key]);
                    } else {
                        let slice_index = key.slice(-1);
                        if(!isNaN(slice_index)){
                            $('[name="category['+slice_index+']"]').addClass('is-invalid');
                            $('[name="category['+slice_index+']"]').next().html(response.error[key]);
                        } else {
                            $('[name="'+key+'"]').addClass('is-invalid');
                            $('[name="'+key+'"]').next().html(response.error[key]);
                        }
                    }
                }
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        }, 
        error : function() {
            alert('Something wrong !!!');
            btn.html('Save');
            btn.prop('disabled', false);
        }
    });
});
</script>
@endsection