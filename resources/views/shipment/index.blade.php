@extends('layout.main')

@section('title', 'Shipment Express')

@section('content')
<div class="row">
    <div class="col-12 col-md-8">
        <div class="card">
            <div class="card-body pt-0">
                <form action="{{ route('shipment.update') }}" id="form-shipment" method="POST">
                    @csrf
                    <div class="table-responsive">
                        <table id="table-shipment" class="table table-sm table-borderless" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>NAME</th>
                                    <th style="text-align: center">CODE</th>
                                    <th style="text-align: center">SERVICE</th>
                                    {{-- <th style="text-align: center">PRICE (KM)</th> --}}
                                    <th style="text-align: center">LAST UPDATE</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <button type="submit" id="btn-shipment" class="btn btn-primary mt-2" style="width: 200px"> Save</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function () {
    // $.fn.dataTable.ext.classes.sPageButton = 'button primary_button';
    let t = $('#table-shipment').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false,
        "responsive": true,
        "processing": true,
        // "serverSide": true,
        "ajax": "{!! url()->current() !!}",
        "columns": [
            { "data": "null", render: function ( data, type, row ) {
                    if (row.is_active == '1')
                        return ' <input type="checkbox" name="shipment['+row.id+']" value="'+row.id+'" checked>';
                    else
                        return ' <input type="checkbox" name="shipment['+row.id+']" value="'+row.id+'">';
                }
            },
            { "data": "name"},
            { "data": "code", "sClass": "text-center"},
            { "data": "service", "sClass": "text-center"},
            { "data": "updated_at", "sClass": "text-center"},
        ],
        'drawCallback': function () {
            $('#table-balance tbody tr' ).css('height', '55px');
        },
        "createdRow": function (row, data, index) {
            $('td', row).eq(0).css('width', '10px');
        },
    });
});
</script>
@endsection