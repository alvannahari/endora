<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\BCA\BCAController;
use App\Http\Controllers\Api\BCA\BCAOauthController;
// use App\Http\Controllers\DummyController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/', function () {
    return response([
        'App'       => 'Api_Endora',
        'Version'   => '1.0'
    ], 200);
});

Route::get('error', function () {
    return response([
        'status'    => false,
        'message'   => 'Request Unauthorized',
        'data'      => null
    ], 401);
})->name('error');

// Route::get('test', [ DummyController::class ,'index' ]);
// Route::post('test', [ DummyController::class ,'index' ]);
// Route::get('test/{type?}/{sub?}', [ DummyController::class ,'index' ]);
// Route::post('test/{type?}/{sub?}', [ DummyController::class ,'index' ]);

Route::group(['prefix' => 'bca', 
    'namespace' => 'Api\BCA',
    'middleware' => 'client',
], function () {
    Route::post('oauth/token', [ BCAOauthController::class ,'issueTokenByBasic' ])
        ->name('bca.issueTokenByBasic')
        ->withoutMiddleware("client");
    Route::post('va/bills', [ BCAController::class ,'billInquiry' ])->name('payment.bca.bill');
    Route::post('va/payments', [ BCAController::class ,'flagCallback' ])->name('payment.bca.flags');
});

Route::group(['prefix' => 'customer', 'namespace' => 'Api\Customer'], function () {
    Route::post('login', 'V1\Login');
    Route::post('register', 'V1\Register');
    Route::post('forgot_password', 'V1\ForgotPassword');
    
    Route::group(['middleware' => ['auth:api-customer','scope:customer'] ], function () {
        Route::get('home', 'V1\Home');
        Route::get('preview', 'V1\Preview');
        Route::get('notif_list', 'V1\NotifList');

        Route::group(['prefix' => 'rajaongkir'], function () {
            Route::get('get_couriers', 'V1\RajaOngkir\GetAllCouriersEndora');
            Route::get('get_currency', 'V1\RajaOngkir\GetCurrency');
            Route::get('get_provinces', 'V1\RajaOngkir\GetProvinces');
            Route::get('get_cities', 'V1\RajaOngkir\GetCities');
            Route::get('get_cities_by_province', 'V1\RajaOngkir\GetCitiesByProvinceId');
            Route::get('get_subdistrict_by_city', 'V1\RajaOngkir\GetSubdistrictsByCity');
            Route::post('check_ongkir', 'V1\RajaOngkir\CheckOngkir');
            Route::post('check_waybill', 'V1\RajaOngkir\CheckWaybill');
            Route::get('get_supported_waybills', 'V1\RajaOngkir\GetSupportedWaybills');
            Route::get('get_detail_subdistrict', 'V1\RajaOngkir\GetDetailSubdistrict');
        });
        Route::group(['prefix' => 'favourite'], function () {
            Route::get('get_list', 'V1\Favourite\GetList');
            Route::post('state_favourite', 'V1\Favourite\StateFavourite');
        });
        Route::group(['prefix' => 'cart'], function () {
            Route::get('get_list', 'V1\Cart\GetList');
            Route::post('add_item', 'V1\Cart\AddItem');
            Route::get('delete_item', 'V1\Cart\DeleteItem');
            Route::post('update_qty', 'V1\Cart\UpdateQty');
        });
        Route::group(['prefix' => 'location'], function () {
            Route::get('get_list', 'V1\Location\GetList');
            Route::get('get_main', 'V1\Location\GetMain');
            Route::post('add_location', 'V1\Location\AddLocation');
            Route::post('update_location', 'V1\Location\UpdateLocation');
            Route::get('delete_location', 'V1\Location\DeleteLocation');
            Route::post('is_main', 'V1\Location\IsMain');
        });
        Route::group(['prefix' => 'billboard'], function () {
            Route::get('get_list', 'V1\Billboard\GetList');
            Route::get('detail', 'V1\Billboard\Detail');
        });
        Route::group(['prefix' => 'offer'], function () {
            Route::get('get_list', 'V1\Offer\GetList');
            Route::get('detail', 'V1\Offer\Detail');
        });
        Route::group(['prefix' => 'category'], function () {
            Route::get('get_list', 'V1\Category\GetList');
            Route::get('detail', 'V1\Category\Detail');
        });
        Route::group(['prefix' => 'brand'], function () {
            Route::get('get_list', 'V1\Brand\GetList');
            Route::get('detail', 'V1\Brand\Detail');
        });
        Route::group(['prefix' => 'item'], function () {
            Route::get('search', 'V1\Item\Search');
            Route::get('detail', 'V1\Item\Detail');
            Route::get('get_list_promo', 'V1\Item\GetListPromo');
        });
        Route::group(['prefix' => 'payment'], function () {
            Route::get('get_list', 'V1\Payment\GetList');
            Route::get('detail/{notifId?}', 'V1\Payment\Detail');
            Route::post('checkout', 'V1\Payment\Checkout');
            Route::post('canceled', 'V1\Payment\Canceled');
            Route::post('review', 'V1\Payment\Reviews');
            Route::post('confirmation_item', 'V1\Payment\Confirmation');
            Route::post('set_submission', 'V1\Payment\SetSubmission');
            Route::group(['excluded_middleware' => ['auth:api-customer','scope:customer']], function () {
                Route::post('paid_callback', 'V1\Payment\PaidCallback');
            }); 
        });
        Route::group(['prefix' => 'user'], function () {
            Route::post('change_password', 'V1\User\ChangePassword');
            Route::post('change_photo', 'V1\User\ChangePhoto');
            Route::post('change_profile', 'V1\User\ChangeProfile');
            Route::get('delete_account', 'V1\User\DeleteAccount');
            Route::post('forgot_password', 'V1\User\ForgotPassword');
            Route::get('remove_photo', 'V1\User\RemovePhoto');
            Route::get('clear_search', 'V1\User\ClearSearch');
            Route::get('get_search', 'V1\User\GetHistorySearch');
        });
        Route::group(['prefix' => 'xendit'], function () {
            Route::post('invoice', 'V1\Xendit\Invoice\Create');

            // Route::group(['excluded_middleware' => ['auth:api-customer','scope:customer']], function () {
            //     Route::post('callback_invoice', 'V1\Xendit\VA\CallbackCreateVA');
            // }); 
            // Route::get('get_list_va', 'V1\Xendit\VA\GetListVA');
            // Route::post('invoice', 'V1\Xendit\VA\CreateVA');

            // Route::group(['excluded_middleware' => ['auth:api-customer','scope:customer']], function () {
            //     Route::post('callback_invoice', 'V1\Xendit\VA\CallbackCreateVA');
            // }); 
        });
        Route::group(['prefix' => 'testNotif'], function () {
            Route::post('testPayment', 'V1\NotificationTestController@testPayment');
            Route::post('testShipment', 'V1\NotificationTestController@testShipment');
        });
    });
});

Route::group(['prefix' => 'courier', 'namespace' => 'Api\Courier'], function () {
    Route::post('login', 'V1\Login');
    Route::post('register', 'V1\Register');
    
    Route::group(['middleware' => ['auth:api-courier','scope:courier'] ], function () {
        Route::get('history_shipping', 'V1\HistoryShipping');
        Route::get('detail_shipping/{notifId?}', 'V1\DetailShipping');
        Route::post('status_shipping', 'V1\StatusShipping');
        Route::get('get_profile', 'V1\GetProfile');
        Route::post('verify_user', 'V1\VerifyUser');
        Route::post('update_profile', 'V1\UpdateProfile');
        Route::get('notif_list', 'V1\NotifList');
        Route::post('change_password', 'V1\ChangePassword');
        Route::post('forgot_password', 'V1\ForgotPassword');
        Route::post('change_photo', 'V1\ChangePhoto');
        Route::get('remove_photo', 'V1\RemovePhoto');
    });
});
