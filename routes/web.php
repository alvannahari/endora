<?php

use Illuminate\Support\Facades\Route;

// use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('getAllUsers', [UserController::class, 'getAllUsers']);

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');

// Route::get('test', 'HomeController@test');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('notifications', 'HomeController@notifications');

    Route::get('product', 'ProductController@index')->name('product');
    Route::delete('product/{item}/delete', 'ProductController@destroy')->name('product.delete');
    Route::get('product/create', 'ProductController@create')->name('product.add');
    Route::post('product', 'ProductController@store');
    Route::post('product/import', 'ProductController@import')->name('product.import');
    Route::post('product/export', 'ProductController@export')->name('product.export');
    Route::get('product/{item}', 'ProductController@show');
    Route::post('item/{item}/image', 'ProductController@storeImage');
    Route::get('item/{imageItem}/delete', 'ProductController@destroyImage');
    Route::get('product/{item}/edit', 'ProductController@edit');
    Route::post('product/{item}/update', 'ProductController@update');

    Route::get('brand', 'BrandController@index')->name('brand');
    Route::post('brand', 'BrandController@store');
    Route::post('brand/import', 'BrandController@import')->name('brand.import');
    Route::post('brand/export', 'BrandController@export')->name('brand.export');
    Route::delete('brand/{brand}/delete', 'BrandController@destroy')->name('brand.delete');
    Route::get('brand/{brand}', 'BrandController@show');
    Route::post('brand/{brand}/update', 'BrandController@update');
    Route::post('brand/{brand}/updateLogo', 'BrandController@updateLogo');
    Route::post('brand/{brand}/updateCover', 'BrandController@updateCover');

    Route::get('billboard', 'BillboardController@index')->name('billboard');
    Route::delete('billboard/{billboard}/delete', 'BillboardController@destroy')->name('billboard.delete');
    Route::post('billboard/{billboard}/state', 'BillboardController@state')->name('billboard.state');
    Route::get('billboard/create', 'BillboardController@create');
    Route::post('billboard', 'BillboardController@store');

    Route::get('category', 'CategoryController@index')->name('category');
    Route::post('category/main', 'CategoryController@storeCategory');
    Route::post('category/submain', 'CategoryController@storeSubCategory');
    Route::get('category/{category}/delete', 'CategoryController@destroy');

    Route::get('offer', 'OfferController@index')->name('offer');
    Route::delete('offer/{offer}/delete', 'OfferController@destroy')->name('offer.delete');
    Route::post('offer/{offer}/state', 'OfferController@state')->name('offer.state');
    Route::get('offer/create', 'OfferController@create');
    Route::post('offer', 'OfferController@store');

    Route::get('payment', 'PaymentController@index')->name('payment');
    Route::post('payment/synchronize', 'PaymentController@synchronizePaymentShipment')->name('payment.synchronize');
    Route::post('payment/report', 'PaymentController@exportReport')->name('payment.exportReport');
    Route::post('payment/{payment}', 'PaymentController@cancel')->name('payment.cancel');
    Route::get('payment/{payment}', 'PaymentController@show');
    Route::post('payment/{payment}/update', 'PaymentController@update');
    Route::post('payment/{payment}/select', 'PaymentController@selectShipping');
    Route::post('payment/{payment}/invoice', 'PaymentController@printInvoice')->name('payment.printInvoice');

    Route::get('balance', 'BalanceController@index')->name('balance');
    Route::post('balance/payout', 'BalanceController@payout')->name('balance.payout');
    Route::get('balance/getPayout/{id}', 'BalanceController@getPayout')->name('balance.getPayout');
    Route::post('balance/exportReport', 'BalanceController@exportReport')->name('balance.exportReport');

    Route::get('history', 'HistoryController@index')->name('history');

    Route::get('customer', 'CustomerController@index')->name('customer');
    Route::post('customer', 'CustomerController@store');
    Route::get('customer/{user}/delete', 'CustomerController@destroy');
    Route::get('customer/{user}', 'CustomerController@show');
    Route::post('customer/{user}/image', 'CustomerController@updateImage');
    Route::get('customer/{user}/image', 'CustomerController@destroyImage');
    Route::get('customer/{user}/state', 'CustomerController@changeState');
    Route::post('customer/{user}/update', 'CustomerController@update');

    Route::get('courier', 'CourierController@index')->name('courier');
    Route::post('courier', 'CourierController@store');
    Route::get('courier/{courier}/delete', 'CourierController@destroy');
    Route::get('courier/{courier}', 'CourierController@show');
    Route::post('courier/{courier}/image', 'CourierController@updateImage');
    Route::get('courier/{courier}/image', 'CourierController@destroyImage');
    Route::get('courier/{courier}/state', 'CourierController@changeState');
    Route::post('courier/{courier}/update', 'CourierController@update');
    
    Route::get('info', 'InfoController@index')->name('info');
    Route::post('info/update', 'InfoController@update');
    Route::post('info/logo', 'InfoController@updateLogo');
    
    Route::get('shipment', 'ShipmentController@index')->name('shipment');
    Route::post('shipment/update', 'ShipmentController@update')->name('shipment.update');

    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
});


